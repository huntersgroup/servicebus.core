using RabbitMQ.Client;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// Represents a suit of extension methods for extending <see cref="IChannel"/> instance configuration using custom settings.
/// </summary>
public static class ChannelExtensions
{
    private static readonly BinderDescriptor DefaultBinder = new();

    /// <summary>
    /// Builds an exchange using the given descriptor.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <returns></returns>
    public static IChannel BuildExchange(this IChannel channel, ExchangeDescriptor exchange)
    {
        channel.ExchangeDeclareAsync(exchange.FullName, exchange.Type.GetName(), exchange.Durable, exchange.AutoDelete, exchange.Arguments).GetAwaiter().GetResult();

        return channel;
    }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="channel"></param>
    ///// <returns></returns>
    //public static IChannel ConfirmSelect(this IChannel channel)
    //{
    //    //todo: to understand how to implement it.
    //    //channel.ConfirmSelectAsync().GetAwaiter().GetResult();
        
    //    return channel;
    //}

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="channel"></param>
    ///// <param name="timeout"></param>
    ///// <returns></returns>
    //public static IChannel WaitForConfirms(this IChannel channel, TimeSpan timeout)
    //{
    //    //todo: to understand how to implement it.
    //    // timeout is not provided !!!
    //    //channel.WaitForConfirmsAsync();

    //    return channel;
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="autoAck"></param>
    /// <returns></returns>
    public static BasicGetResult BasicGet(this IChannel channel, string queue, bool autoAck)
    {
        return channel.BasicGetAsync(queue, autoAck).GetAwaiter().GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <returns></returns>
    public static QueueDeclareOk QueueDeclare(this IChannel channel)
    {
        return channel.QueueDeclareAsync().GetAwaiter().GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="exchange"></param>
    /// <param name="routingKey"></param>
    /// <param name="arguments"></param>
    public static void QueueBind(this IChannel channel, string queue, string exchange, string routingKey, IDictionary<string, object> arguments = null)
    {
        channel.QueueBindAsync(queue, exchange, routingKey).GetAwaiter().GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <returns></returns>
    public static IBasicProperties CreateBasicProperties(this IChannel channel)
    {
        return new BasicProperties
        {
            Headers = new Dictionary<string, object>()
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <returns></returns>
    public static IChannel BuildExchangeBound(this IChannel channel, ExchangeBoundDescriptor exchange)
    {
        if (exchange == null)
        {
            return channel;
        }

        channel.BuildExchange(exchange);

        // binding with queues.
        foreach (var queue in exchange.Queues)
        {
            var binder = queue.Binder ?? DefaultBinder;

            channel.BuildQueue(queue)
                .BuildBinder(exchange, binder, queue);
        }

        // binding with exchanges.
        foreach (var destinationEx in exchange.Exchanges)
        {
            var binder = destinationEx.Binder ?? DefaultBinder;

            channel.BuildExchangeBound(destinationEx)
                .BuildBinder(exchange, binder, destinationEx);
        }

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static ExchangeDescriptor BuildDlxExchange(this IChannel channel, RetryPolicyDescriptor retryPolicy, QueueDescriptor queue)
    {
        if (retryPolicy is not { Enabled: true })
        {
            return null;
        }

        var dlxExchange = retryPolicy.BuildDlxExchange(queue);
        channel.BuildExchange(dlxExchange);

        var retryExchange = retryPolicy.BuildRetryExchange(queue);
        channel.BuildExchange(retryExchange);

        var retryQueue = retryPolicy.BuildRetryQueue(queue, retryExchange);
        channel.BuildQueue(retryQueue);

        channel.BuildBinder(dlxExchange, new BinderDescriptor(), retryQueue);
        channel.BuildBinder(retryExchange, new BinderDescriptor(), queue);

        return dlxExchange;
    }

    /// <summary>
    /// Builds a queue using the given descriptor.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static IChannel BuildQueue(this IChannel channel, QueueDescriptor queue)
    {
        channel.QueueDeclareAsync(queue.FullName, queue.Durable, queue.Exclusive, queue.AutoDelete, queue.Arguments);

        return channel;
    }

    /// <summary>
    /// Makes a binding between exchange a queue
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="exchange"></param>
    /// <param name="binder"></param>
    /// <returns></returns>
    public static IChannel BuildBinder(this IChannel channel, ExchangeDescriptor exchange, BinderDescriptor binder, QueueDescriptor queue)
    {
        binder ??= DefaultBinder;

        channel.QueueBindAsync(queue.FullName, exchange.FullName, binder.RoutingKey, binder.Arguments);

        return channel;
    }

    /// <summary>
    /// Makes a binding between exchanges
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="source"></param>
    /// <param name="binder"></param>
    /// <param name="destination"></param>
    /// <returns></returns>
    public static IChannel BuildBinder(this IChannel channel, ExchangeDescriptor source, BinderDescriptor binder, ExchangeDescriptor destination)
    {
        channel.ExchangeBindAsync(destination.FullName, source.FullName, binder.RoutingKey, binder.Arguments);

        return channel;
    }

    /// <summary>
    /// Builds a custom consumer binding between queue and consumer.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="consumer"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public static IChannel BindConsumer(this IChannel channel, QueueDescriptor queue, IAsyncBasicConsumer consumer, IDictionary<string, object> args = null)
    {
        // todo: verify
        channel.BasicConsumeAsync(queue: queue.FullName, autoAck: false, consumer: consumer, arguments: args, consumerTag: null!);

        return channel;
    }

    /// <summary>
    /// Publish a message into the given exchange, using the given parameters.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route">The current route used to forward message.</param>
    /// <param name="properties">Custom message properties for message.</param>
    /// <param name="body">The original message encoded.</param>
    /// <returns></returns>
    public static IChannel PublishMessage(this IChannel channel, ExchangeDescriptor exchange, string route, IReadOnlyBasicProperties properties, byte[] body)
    {
        var prop = properties as BasicProperties ?? new BasicProperties(properties);
        channel.BasicPublishAsync(exchange.FullName, route, true, prop, body).AsTask().GetAwaiter().GetResult();

        return channel;
    }

    /// <summary>
    /// Publish a message into the given exchange, using the given parameters.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route">The current route used to forward message.</param>
    /// <param name="properties">Custom message properties for message.</param>
    /// <param name="body">The original message encoded.</param>
    /// <returns></returns>
    public static async ValueTask PublishMessageAsync(this IChannel channel, ExchangeDescriptor exchange, string route, IBasicProperties properties, byte[] body)
    {
        var prop = properties as BasicProperties ?? new BasicProperties(properties);
        // todo: verify parameter 'mandatory' that's false in the original version
        await channel.BasicPublishAsync(exchange: exchange.FullName, routingKey:route, false, basicProperties: prop, body:body);
    }

    /// <summary>
    /// Publish a message into the given exchange, using the given parameters.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route">The current route used to forward message.</param>
    /// <param name="properties">Custom message properties for message.</param>
    /// <param name="body">The original message encoded.</param>
    /// <returns></returns>
    public static IChannel PublishMessage(this IChannel channel, ExchangeDescriptor exchange, string route, IReadOnlyBasicProperties properties, ReadOnlyMemory<byte> body)
    {
        var prop = properties as BasicProperties ?? new BasicProperties(properties);
        // todo: verify parameter 'mandatory' that's false in the original version
        channel.BasicPublishAsync(exchange: exchange.FullName, routingKey: route, false, prop, body).AsTask().GetAwaiter().GetResult();

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="autoAck"></param>
    /// <param name="consumer"></param>
    public static void BasicConsume(this IChannel channel, string queue, bool autoAck, IAsyncBasicConsumer consumer)
    {
        channel.BasicConsumeAsync(queue: queue, autoAck: autoAck, consumer).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Cancel all consumer tags registered by the given channel
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="consumer"></param>
    public static void Cancel(this IChannel channel, AsyncDefaultBasicConsumer consumer)
    {
        if (channel == null || consumer == null)
        {
            return;
        }

        foreach (var consumerTag in consumer.ConsumerTags)
        {
            try
            {
                channel.BasicCancel(consumerTag);
            }
            catch (Exception)
            {
                // nothing to do here!!
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="prefetchSize"></param>
    /// <param name="prefetchCount"></param>
    /// <param name="global"></param>
    /// <returns></returns>
    public static IChannel BasicQos(this IChannel channel, uint prefetchSize, ushort prefetchCount, bool global)
    {
        channel.BasicQosAsync(prefetchSize, prefetchCount, global).GetAwaiter().GetResult();

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="deliveryTag"></param>
    /// <param name="requeue"></param>
    /// <returns></returns>
    public static IChannel BasicReject(this IChannel channel, ulong deliveryTag, bool requeue)
    {
        channel.BasicRejectAsync(deliveryTag, requeue).AsTask().GetAwaiter().GetResult();
        
        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="consumerTag"></param>
    public static void BasicCancel(this IChannel channel, string consumerTag)
    {
        channel.BasicCancelAsync(consumerTag).GetAwaiter().GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="deliveryTag"></param>
    /// <param name="multiple"></param>
    /// <returns></returns>
    public static IChannel BasicAck(this IChannel channel, ulong deliveryTag, bool multiple)
    {
        channel.BasicAckAsync(deliveryTag, multiple).AsTask().GetAwaiter().GetResult();

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="deliveryTag"></param>
    /// <param name="multiple"></param>
    /// <param name="requeue"></param>
    /// <returns></returns>
    public static IChannel BasicNack(this IChannel channel, ulong deliveryTag, bool multiple, bool requeue)
    {
        channel.BasicNackAsync(deliveryTag, multiple, requeue).AsTask().GetAwaiter().GetResult();

        return channel;
    }
}
