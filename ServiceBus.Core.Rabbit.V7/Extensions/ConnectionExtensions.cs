using RabbitMQ.Client;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class ConnectionExtensions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionFactory"></param>
    /// <returns></returns>
    public static IConnection CreateConnection(this IConnectionFactory connectionFactory)
    {
        return connectionFactory.CreateConnectionAsync().GetAwaiter().GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionFactory"></param>
    /// <param name="clientProvidedName"></param>
    /// <returns></returns>
    public static IConnection CreateConnection(this IConnectionFactory connectionFactory, string clientProvidedName)
    {
        return connectionFactory.CreateConnectionAsync(clientProvidedName).GetAwaiter().GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <returns></returns>
    public static IChannel CreateChannel(this IConnection connection)
    {
        return connection.CreateChannelAsync().GetAwaiter().GetResult();
    }
}
