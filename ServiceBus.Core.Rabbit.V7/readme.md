**ServiceBus.Core.Rabbit** is the implementation of **ServiceBus.Core** framework, which uses RabbitMQ as underlying message platform.

for details, see the [documentation](https://servicebuscore.readthedocs.io/en/latest/).
