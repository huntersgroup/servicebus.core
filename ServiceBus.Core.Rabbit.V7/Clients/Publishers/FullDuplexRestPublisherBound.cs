using System.Collections.Concurrent;
using System.Globalization;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;
using ServiceBus.Core.Threading;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// 
/// </summary>
public class FullDuplexRestPublisherBound : AsyncRestPublisher, IDisposable, IAsyncDisposable
{
    private readonly string replyQueueName;
    private readonly Action<RestResponse> afterTranslateAction = _ => { };
    private readonly ConcurrentDictionary<string, ITranslatorTaskCompletionSource> callBackResponses;
    private readonly AsyncEventingBasicConsumer consumer;

    public FullDuplexRestPublisherBound(
        RestPublisherBoundDescriptor descriptor,
        IChannel channel,
        IDataFormatter dataFormatter,
        IHttpRestMessageSerializer messageSerializer)
        : base(descriptor, channel, dataFormatter, messageSerializer)
    {
        this.replyQueueName = this.Channel.QueueDeclare().QueueName;
        this.Channel.QueueBind(queue: this.replyQueueName, exchange: this.Exchange.FullName, this.replyQueueName);

        this.callBackResponses = new ConcurrentDictionary<string, ITranslatorTaskCompletionSource>();

        if (descriptor.IncludeContent)
        {
            this.afterTranslateAction = response =>
            {
                response.Content = this.MessageSerializer.Encoder.GetString(response.RawBytes);
            };
        }

        this.consumer = new AsyncEventingBasicConsumer(this.Channel);

        this.consumer.ReceivedAsync += this.OnReceived;

        this.Channel.BasicConsume(this.replyQueueName, autoAck: true, this.consumer);
    }

    ///<inheritdoc />
    public override async Task<RestResponse> SendAsync(RestRequest request)
    {
        var tsc = new TranslatorTaskCompletionSource<HttpRestResponse, RestResponse>(serviceRequest =>
        {
            var resp = serviceRequest.Translate();
            this.afterTranslateAction(resp);

            return resp;
        });

        await this.SendRequestAsync(request, tsc);

        return await tsc.Task;
    }

    ///<inheritdoc />
    public override async Task<RestResponse<TData>> SendAsync<TData>(RestRequest request)
    {
        var tsc = new TranslatorTaskCompletionSource<HttpRestResponse, RestResponse<TData>>(serviceRequest =>
        {
            var resp = serviceRequest.Translate<TData>(this.DataFormatter);
            this.afterTranslateAction(resp);

            return resp;
        });

        await this.SendRequestAsync(request, tsc);

        return await tsc.Task;
    }

    ///<inheritdoc />
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc />
    public async ValueTask DisposeAsync()
    {
        if (this.consumer.IsRunning)
        {
            this.Channel.Cancel(this.consumer);
        }

        this.consumer.ReceivedAsync -= this.OnReceived;

        await ValueTask.CompletedTask;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    /// <param name="taskCompletionSource"></param>
    /// <returns></returns>
    private async ValueTask SendRequestAsync(RestRequest request, ITranslatorTaskCompletionSource taskCompletionSource)
    {
        request.AddOrOverrideHeader(HttpHeaders.ContentType, this.Descriptor.ContextType);

        var timeout = request.Timeout ?? this.Descriptor.Timeout ?? TimeSpan.FromSeconds(15);

        var properties = this.Channel.CreateBasicProperties();
        properties.CorrelationId = request.Id;
        properties.Timestamp = new AmqpTimestamp(DateTime.UtcNow.AsUnixDateTime());
        properties.Expiration = timeout.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
        properties.ReplyTo = this.replyQueueName;

        // workaround due to missing initialization on subscriber message event.
        // adding at least one entry, it works fine as before.
        properties.AddHeader(nameof(properties.CorrelationId), properties.CorrelationId);

        this.callBackResponses.TryAdd(request.Id, taskCompletionSource);

        var httpRequest = request.Translate(this.DataFormatter);
        var payload = this.MessageSerializer.SerializeRequest(httpRequest);

        var cts = new CancellationTokenSource(timeout);

        cts.Token.Register(() =>
        {
            if (!this.callBackResponses.TryRemove(request.Id, out var tcs))
            {
                return;
            }

            tcs.SetException(new TimeoutException($"The request with the given id ({request.Id}) wasn't completed successfully due to timeout problem."));
        });

        await this.Channel.PublishMessageAsync(this.Exchange, string.Empty, properties, payload);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="ea"></param>
    /// <returns></returns>
    private async Task OnReceived(object sender, BasicDeliverEventArgs ea)
    {
        if (!this.callBackResponses.TryRemove(ea.BasicProperties.CorrelationId, out var tcs))
        {
            return;
        }

        var response = this.MessageSerializer.DeserializeResponse(ea.Body);
        tcs.TrySetResult(response);

        await Task.CompletedTask;
    }
}
