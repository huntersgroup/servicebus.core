using RabbitMQ.Client;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// 
/// </summary>
public abstract class AsyncRestPublisher : BoundClient, IAsyncRestPublisher
{
    protected AsyncRestPublisher(RestPublisherBoundDescriptor descriptor, IChannel channel, IDataFormatter dataFormatter, IHttpRestMessageSerializer messageSerializer)
        : base(descriptor, channel)
    {
        descriptor.VerifyStatus();

        this.Descriptor = descriptor;
        this.DataFormatter = dataFormatter;
        this.MessageSerializer = messageSerializer;

        this.Exchange = new ExchangeDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueDescriptor = new QueueDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueBinderDescriptor = new BinderDescriptor();

        // prepares working queue, exchange and working binder for queue flow.
        this.Channel.BuildExchange(this.Exchange)
            .BuildQueue(workingQueueDescriptor)
            .BuildBinder(this.Exchange, workingQueueBinderDescriptor, workingQueueDescriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    public RestPublisherBoundDescriptor Descriptor { get; }

    /// <summary>
    /// 
    /// </summary>
    public IDataFormatter DataFormatter { get; }

    /// <summary>
    /// 
    /// </summary>
    public IHttpRestMessageSerializer MessageSerializer { get; }

    /// <summary>
    /// 
    /// </summary>
    public ExchangeDescriptor Exchange { get; }

    ///<inheritdoc />
    public abstract Task<RestResponse> SendAsync(RestRequest request);

    ///<inheritdoc />
    public abstract Task<RestResponse<TData>> SendAsync<TData>(RestRequest request);
}
