using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;


namespace ServiceBus.Core.Clients.Consumers;

public class ConsumerBound<TData> : BoundClient, IAsyncConsumer<TData>
{
    private readonly SubscriberBoundDescriptor descriptor;
    private readonly Func<BasicDeliverEventArgs, Message<TData>> messageDecoderFunc;
    private ExchangeDescriptor dlxExchange;
    private Func<object, BasicDeliverEventArgs, Task> messageAction;

    public ConsumerBound(SubscriberBoundDescriptor descriptor, IDataDecoder decoder, IChannel channel)
        : base(descriptor, channel)
    {
        descriptor.ThrowIfNull(nameof(descriptor));
        decoder.ThrowIfNull(nameof(decoder));

        this.descriptor = descriptor;

        this.descriptor.VerifyStatus();

        this.SetUp();

        this.messageDecoderFunc = descriptor.MessageFormat switch
        {
            MessageFormat.Header => ea => decoder.Decode<Message<TData>>(ea.Body),
            MessageFormat.Headless => ea =>
            {
                var body = decoder.Decode<TData>(ea.Body);

                var properties = ea.BasicProperties;

                return new Message<TData>
                {
                    Route = ea.RoutingKey,
                    Persistent = properties?.Persistent,
                    Created = properties?.Timestamp.UnixTime.FromUnixDateTime(),
                    Data = body
                };
            }
            ,
            _ => throw new InvalidDescriptorException(
                $"The current message format indicated in the current subscriber descriptor is not supported, value: {descriptor.MessageFormat}")
        };

        this.Received = new DelegateHandler<MessageDeliverEventArgs<TData>>();
        this.OnException = new DelegateHandler<MessageExceptionEventArgs>();
        this.OnMaxRetryFailed = new DelegateHandler<RetryExceptionEventArgs<TData>>();
    }

    ///<inheritdoc/>
    public IDelegateHandler<MessageDeliverEventArgs<TData>> Received { get; }

    ///<inheritdoc/>
    public IDelegateHandler<MessageExceptionEventArgs> OnException { get; }

    ///<inheritdoc/>
    public IDelegateHandler<RetryExceptionEventArgs<TData>> OnMaxRetryFailed { get; }

    ///<inheritdoc/>
    public async Task Execute()
    {
        if (this.Received.Count() == 0)
        {
            throw new SetupBrokerClientException($"The {nameof(this.Received)} delegate handler must have at least one delegate before calling this method.");
        }

        var queueName = this.descriptor.Queue.FullName;

        for (var i = 0; i < this.descriptor.PrefetchCount; i++)
        {
            var queueMessage = this.Channel.BasicGet(queueName, false);

            if (queueMessage == null)
            {
                break;
            }

            var ea = new BasicDeliverEventArgs(string.Empty, queueMessage.DeliveryTag, queueMessage.Redelivered,
                queueMessage.Exchange, queueMessage.RoutingKey,
                queueMessage.BasicProperties, queueMessage.Body);

            try
            {
                await this.messageAction(this.Channel, ea);
            }
            catch (Exception ex)
            {
                this.CatchException(this, ex, ea);
            }
        }
    }

    private void SetUp()
    {
        try
        {
            // prefetch in this case must be set to one, there's no sense to download many messages and elaborating many ones at the same time.
            this.Channel.BasicQos(0, 1, false);

            this.Channel
                .BuildQueue(this.descriptor.Queue);

            this.dlxExchange = this.Channel.BuildDlxExchange(this.descriptor.RetryPolicy, this.descriptor.Queue);

            this.messageAction = this.dlxExchange == null ? this.OnReceived : this.OnReceivedWithRetry;
        }
        catch (Exception e)
        {
            throw new SetupBrokerClientException("An exception was thrown on setup subscriber, see inner exception for details", e)
            {
                ClientType = typeof(ConsumerBound<TData>)
            };
        }
    }

    private void CatchException(object sender, Exception ex, BasicDeliverEventArgs ea)
    {
        this.Channel.BasicReject(ea.DeliveryTag, false);

        this.OnException.InvokeAsync(sender, new MessageExceptionEventArgs
        {
            PayloadType = typeof(TData),
            Exception = ex
        });
    }

    private async Task OnReceived(object sender, BasicDeliverEventArgs ea)
    {
        var eventArg = this.GetMessageDeliverEventArgs(ea);

        await this.Received.InvokeAsync(sender, eventArg);

        if (eventArg.Acknowledged)
        {
            this.Channel.BasicAck(ea.DeliveryTag, false);
        }
        else
        {
            this.Channel.BasicNack(ea.DeliveryTag, false, eventArg.Requeue);
        }
    }

    private async Task OnReceivedWithRetry(object sender, BasicDeliverEventArgs ea)
    {
        var eventArg = this.GetMessageDeliverEventArgs(ea);

        await this.Received.InvokeAsync(sender, eventArg);

        if (eventArg.Acknowledged)
        {
            this.Channel.BasicAck(ea.DeliveryTag, false);
        }
        else
        {
            this.Channel.BasicNack(ea.DeliveryTag, false, false);

            if (eventArg.Requeue)
            {
                var retryCounter = ea.BasicProperties.IncrementRetryCounter();

                if (retryCounter < this.descriptor.RetryPolicy.MaxRetry)
                {
                    this.Channel.PublishMessage(this.dlxExchange, string.Empty, ea.BasicProperties, ea.Body);
                }
                else
                {
                    await this.OnMaxRetryFailed.InvokeAsync(sender, new RetryExceptionEventArgs<TData>
                    {
                        Message = eventArg.Message,
                        RetryCounter = retryCounter,
                        Exception = new Exception("An exception was occurred due to reach the max retry counter for requeue message")
                    });
                }
            }
        }
    }

    private MessageDeliverEventArgs<TData> GetMessageDeliverEventArgs(BasicDeliverEventArgs ea)
    {
        var message = this.messageDecoderFunc(ea);
        return new MessageDeliverEventArgs<TData>(ea.Body)
        {
            Message = message
        };
    }
}
