using RabbitMQ.Client;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public class ChannelHandler : IDisposable, IAsyncDisposable
{
    private readonly Lazy<IConnection> connection;
    private readonly Lazy<IChannel> channel;
    private string appTarget;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionFactory"></param>
    /// <param name="providerName"></param>
    public ChannelHandler(IConnectionFactory connectionFactory, string providerName)
    {
        this.appTarget = providerName;
        this.connection = new Lazy<IConnection>(connectionFactory.CreateConnectionAsync(this.appTarget).GetAwaiter().GetResult);
        this.channel = new Lazy<IChannel>(this.connection.Value.CreateChannelAsync().GetAwaiter().GetResult);
    }

    /// <summary>
    /// 
    /// </summary>
    public IChannel Channel { get { return this.channel.Value; } }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="providerName"></param>
    /// <returns></returns>
    public bool UpdateTarget(string providerName)
    {
        if (this.connection.IsValueCreated && string.IsNullOrWhiteSpace(providerName))
        {
            return false;
        }

        this.appTarget = providerName;

        return true;
    }

    ///<inheritdoc />
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc />
    public ValueTask DisposeAsync()
    {
        if (this.channel.IsValueCreated)
        {
            this.channel.Value.Dispose();
        }

        if (this.connection.IsValueCreated)
        {
            this.connection.Value.Dispose();
        }

        return ValueTask.CompletedTask;
    }
}
