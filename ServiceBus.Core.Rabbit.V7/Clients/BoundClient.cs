using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public class BoundClient : IAsyncBrokerClient
{
    public BoundClient(IBrokerDescriptor descriptor, IChannel channel)
    {
        descriptor.ThrowIfNull(nameof(descriptor));
        channel.ThrowIfNull(nameof(channel));

        if (channel.IsClosed)
        {
            throw new SetupBrokerClientException("The current channel cannot be used because it's closed.");
        }

        this.Channel = channel;
        this.OnChannelEventFired = new DelegateHandler<ChannelEventArgs>();

        if (!descriptor.EnableChannelEvents)
        {
            return;
        }

        this.Channel.FlowControlAsync += this.OnChannelFlowControl;
        this.Channel.CallbackExceptionAsync += this.OnChannelCallbackException;
        this.Channel.ChannelShutdownAsync += this.OnChannelModelShutdown;
    }

    ///<inheritdoc />
    public IDelegateHandler<ChannelEventArgs> OnChannelEventFired { get; protected set; }

    /// <summary>
    /// Gets the underlying rabbit channel.
    /// </summary>
    protected IChannel Channel { get; }
    

    private async Task OnChannelCallbackException(object sender, CallbackExceptionEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.CallbackException,
            Properties =
            {
                {nameof(e.Detail), e.Detail},
                {nameof(e.Exception), e.Exception}
            }
        };

        await this.OnChannelEventFired.InvokeAsync(sender, args);
    }

    private async Task OnChannelFlowControl(object sender, FlowControlEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.FlowControl,
            Properties =
            {
                {nameof(e.Active), e.Active}
            }
        };

        await this.OnChannelEventFired.InvokeAsync(sender, args);
    }

    private async Task OnChannelModelShutdown(object sender, ShutdownEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.ModelShutdown,
            Properties =
            {
                {nameof(e.ClassId), e.ClassId},
                {nameof(e.MethodId), e.MethodId},
                {nameof(e.ReplyCode), e.ReplyCode},
                {nameof(e.ReplyText), e.ReplyText}
            }
        };

        await this.OnChannelEventFired.InvokeAsync(sender, args);
    }
}
