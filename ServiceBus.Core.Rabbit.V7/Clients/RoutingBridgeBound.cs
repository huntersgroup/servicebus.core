using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients;

public class RoutingBridgeBound<TData> : SubscriberBound<TData>, IRoutingBridgeBound<TData>
{
    private readonly PublisherBound publisher;
    private readonly IChannelFactory channelFactory;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="formatter"></param>
    /// <param name="channelFactory"></param>
    public RoutingBridgeBound(RoutingBridgeDescriptor descriptor, IDataFormatter formatter, IChannelFactory channelFactory)
        : base(descriptor.Source, formatter, channelFactory.Build($"bridge_sub: {Guid.NewGuid():N}"))
    {
        this.channelFactory = channelFactory;
        this.publisher = new PublisherBound(descriptor.Target, formatter, channelFactory.Build($"bridge_pub: {Guid.NewGuid():N}"));

        var nextExecutor = new DelegateHandler<MessageDeliverEventArgs<TData>>();
        nextExecutor.Add(this.HandlerOnAfterEventRaised);

        this.Received = new NextDelegateHandler<MessageDeliverEventArgs<TData>>(nextExecutor, null);
        this.BeforeForwarding = new DelegateHandler<MessageDeliverEventArgs<TData>>();
        this.OnForwardingException = new DelegateHandler<ForwardExceptionEventArg<TData>>();
        this.OnChannelEventFired = new NextDelegateHandler<ChannelEventArgs>(null, new[] { this.publisher.OnChannelEventFired });
    }

    ///<inheritdoc/>
    public IDelegateHandler<MessageDeliverEventArgs<TData>> BeforeForwarding { get; }

    ///<inheritdoc/>
    public IDelegateHandler<ForwardExceptionEventArg<TData>> OnForwardingException { get; }

    ///<inheritdoc/>
    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        this.Received.Clear();
        this.BeforeForwarding.Clear();
        this.OnForwardingException.Clear();
        this.OnChannelEventFired.Clear();
        this.channelFactory.Dispose();
    }

    /// <summary>
    /// This is the last method executed after executing all message event handlers were executed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async Task HandlerOnAfterEventRaised(object sender, MessageDeliverEventArgs<TData> e)
    {
        await this.BeforeForwarding.InvokeAsync(sender, e);

        try
        {
            if (!e.Acknowledged || !this.IsRunning)
            {
                return;
            }

            await this.publisher.SendAsync(e.Message);
        }
        catch (Exception ex)
        {
            // due to the error, it's needed to
            e.Acknowledged = false;

            var forwardEx = new ForwardExceptionEventArg<TData>(e)
            {
                Exception = ex
            };

            await this.OnForwardingException.InvokeAsync(sender, forwardEx);
        }
    }
}
