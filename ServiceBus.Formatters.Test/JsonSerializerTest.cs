using ServiceBus.Core.Formatters.Json;
using ServiceBus.Formatters.Test.Model;

namespace ServiceBus.Formatters.Test;

public abstract class JsonSerializerTest
{
    protected abstract IJsonSerializer Serializer { get; }

    [Fact]
    public void Serialize_Test()
    {
        var instance = new Person { Name = "Tom", AgeDec = 12.0m, AgeI = 1, AgeD = 1.7E+3, AgeF = 3.551f };
        var expected = "{\"name\":\"Tom\",\"ageDec\":12.0,\"ageD\":1700.0,\"ageF\":3.551,\"ageI\":1}";

        var actual = this.Serializer.Serialize(instance);
        Assert.NotNull(actual);
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void Deserialize_Test()
    {

    }

    [Theory]
    [InlineData(10, "10.0")]
    [InlineData(0, "0.0")]
    [InlineData(110.45999999, "110.45999999")]
    [InlineData(110.00000000, "110.0")]
    [InlineData(999.0000001, "999.0000001")]
    public void ApplyDecimalWithFractionalPortionTest(decimal val, string expected)
    {
        var actual = this.Serializer.Serialize(val);

        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(10d, "10.0")]
    [InlineData(0d, "0.0")]
    [InlineData(110.45999999d, "110.45999999")]
    [InlineData(150.00000000d, "150.0")]
    public void ApplyDoubleWithFractionalPortionTest(double val, string expected)
    {
        var actual = this.Serializer.Serialize(val);

        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(10f, "10.0")]
    [InlineData(0f, "0.0")]
    [InlineData(110.4541f, "110.4541")]
    [InlineData(150.00000000f, "150.0")]
    public void ApplyFloatWithFractionalPortionTest(float val, string expected)
    {
        var actual = this.Serializer.Serialize(val);

        Assert.Equal(expected, actual);
    }
}
