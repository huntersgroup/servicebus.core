namespace ServiceBus.Formatters.Test.Model;

public record Person
{
    public string? Name { get; set; }

    public decimal AgeDec { get; set; }

    public double AgeD { get; set; }

    public float AgeF { get; set; }

    public int AgeI { get; set; }
}
