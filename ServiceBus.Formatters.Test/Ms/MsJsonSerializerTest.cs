using System.Text.Json;
using System.Text.Json.Serialization;
using ServiceBus.Core.Formatters.Extensions;
using ServiceBus.Core.Formatters.Json;

namespace ServiceBus.Formatters.Test.Ms;

public class MsJsonSerializerTest : JsonSerializerTest
{
    public MsJsonSerializerTest()
    {
        var options = new JsonSerializerOptions
        {
            DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            ReferenceHandler = ReferenceHandler.IgnoreCycles,
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
        };

        options.Converters.Add(new JsonStringEnumConverter());
        options.ApplyRealNumberWithFractionalPortion();

        this.Serializer = new MsJsonSerializer(options);
    }

    protected override IJsonSerializer Serializer { get; }
}
