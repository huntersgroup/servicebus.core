using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using ServiceBus.Core.Formatters.Json;

namespace ServiceBus.Formatters.Test.Newtonsoft;

public class JsonNetSerializerTest : JsonSerializerTest
{
    public JsonNetSerializerTest()
    {
        var settings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            FloatParseHandling = FloatParseHandling.Decimal,
            NullValueHandling = NullValueHandling.Ignore
        };

        settings.Converters.Add(new StringEnumConverter());

        this.Serializer = new JsonNetSerializer(settings);
    }

    protected override IJsonSerializer Serializer { get; }
}
