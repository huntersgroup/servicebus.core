namespace ServiceBus.Demo.Model;

public class ComplexCls
{
    public int Id { get; set; }

    public string Name { get; set; }

    public ClsType Type { get; set; }

    public DateTime BirthDate { get; set; }

    public string Description { get; set; }

    public decimal DecVal { get; set; }

    public float FloatVal { get; set; }

    public double DoubleVal { get; set; }

    public IEnumerable<string> Alias { get; set; }

    public ComplexCls Parent { get; set; }

    public IEnumerable<ComplexCls> Children { get; set; }

    public IDictionary<string, string> Tags { get; set; }

    public IDictionary<string, object> Fields { get; set; }
}

public enum ClsType : byte
{
    Undefined = 0,
    Common = 1,
    Complex = 2
}
