namespace ServiceBus.Demo.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Person
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int YearBorn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Person> Children { get; set; }
    }
}
