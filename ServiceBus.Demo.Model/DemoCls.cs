﻿namespace ServiceBus.Demo.Model
{
    /// <summary>
    /// Demo class
    /// </summary>
    public class DemoCls
    {
        /// <summary>
        /// Gets or sets the given id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the given value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the current price.
        /// </summary>
        public double Price { get; set; }
    }
}
