using System.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using ServiceBus.Core;
using ServiceBus.Core.Clients.Publishers;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Formatters.Json;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.IO;
using ServiceBus.Core.Model;
using ServiceBus.Demo.Common;
using ServiceBus.Demo.Model;
using ServiceResolver.Ioc.Extensions;

namespace ServiceBus.Demo.Pub;

/// <summary>
/// 
/// </summary>
internal class Application
{
    private readonly IDataFormatter dataFormatter;
    private readonly IJsonSerializer jsonSerializer;
    private readonly IServiceProvider serviceProvider;
    private const char TabChar = '\t';

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dataFormatter"></param>
    /// <param name="jsonSerializer"></param>
    /// <param name="serviceProvider"></param>
    public Application(IDataFormatter dataFormatter, IJsonSerializer jsonSerializer, IServiceProvider serviceProvider)
    {
        this.dataFormatter = dataFormatter;
        this.jsonSerializer = jsonSerializer;
        this.serviceProvider = serviceProvider;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private static async Task Main()
    {
        var serviceProvider = AppInitializer.GetServiceProvider();

        try
        {
            var app = serviceProvider.GetService<Application>();

            await app.ExecuteRoutine();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        finally
        {
            serviceProvider.TryToDispose();
        }

        await Task.CompletedTask;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    internal async Task ExecutePublisherBound()
    {
        var descriptor = new PublisherBoundDescriptor
        {
            Message =
            {
                Ttl = TimeSpan.FromHours(2)
            },
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "demo_01", Type = ExchangeTypes.Direct, Durable = true,
                    Queues =
                    {
                        new QueueBoundDescriptor
                        {
                            Name = "demo_01_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "quorum"}
                            }
                        },
                        new QueueBoundDescriptor
                        {
                            Name = "demo_02_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "classic"}
                            }
                        }
                    },
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_01", Type = ExchangeTypes.Direct, Durable = true,
                            Queues =
                            {
                                new QueueBoundDescriptor
                                {
                                    Name = "demo_01_01_q", Durable = true, AutoDelete = false, Arguments =
                                    {
                                        {"x-queue-type", "stream"}
                                    }
                                }
                            }
                        },
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_02", Type = ExchangeTypes.Direct, Durable = true
                        }
                    }
                }
            }
        };

        var channel = this.serviceProvider.GetService<IModel>();

        await using var publisher = new PublisherBound(descriptor, this.dataFormatter, channel);

        ConsoleExt.WriteLine("#### Starting a new producer ####", ConsoleColor.Blue);
        const string exit = "exit";

        while (true)
        {
            var msgInput = ConsoleExt.ReadInput($"** Press Enter to produce a random message, otherwise write 'exit' to exit: {TabChar}");

            if (exit.Equals(msgInput))
            {
                break;
            }

            var dateTime = DateTime.UtcNow.AddYears(-10);
            var index = Random.Shared.Next(1, 1000);
            const decimal dec = 1.15m;
            const double rating = 12.59;

            var instance = new ComplexCls
            {
                Id = index,
                Name = $"name {index}",
                Type = ClsType.Complex,
                BirthDate = dateTime.AddDays(index),
                Description = $"description {Guid.NewGuid():D}",
                DecVal = dec + index,
                DoubleVal = rating,
                Alias = new[] { $"alias {index}", $"alias {index + 1}" },
                Parent = new ComplexCls { Id = 1, Name = $"parent {index}", Type = ClsType.Common },
                Children = new[]
                {
                    new ComplexCls{ Id = index, Name = $"child 1", DecVal = 0m, DoubleVal = 0d},
                    new ComplexCls{ Id = index, Name = $"child 2", DecVal = 0m, DoubleVal = 0d}
                },
                Tags = new Dictionary<string, string>
                {
                    {"tag0", "tag 1"},
                    {"tag1", "tag 2"},
                    {"tag3", "tag 3"}
                },
                Fields = new Dictionary<string, object>
                {
                    {"intVal", 0},
                    {"longVal", 0L},
                    {"doubleVal", 0.25},
                    {"decVal", 0.78m},
                    {"doubleVal_1", 0.0},
                    {"decVal_1", 0.0m},
                    {"par3", DateTime.UtcNow.AddYears(-5)},
                }
            };

            object message = new Message<ComplexCls>
            {
                Data = instance
            };

            await publisher.SendAsync(message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="counter"></param>
    /// <returns></returns>
    internal async Task ExecutePublisherBound_Many(int counter)
    {
        var descriptor = new PublisherBoundDescriptor
        {
            Message =
            {
                Ttl = TimeSpan.FromHours(2)
            },
            Exchanges =
            {
                new ExchangeBoundDescriptor
                {
                    Name = "demo_01", Type = ExchangeTypes.Direct, Durable = true,
                    Queues =
                    {
                        new QueueBoundDescriptor
                        {
                            Name = "demo_01_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "quorum"}
                            }
                        },
                        new QueueBoundDescriptor
                        {
                            Name = "demo_02_q", Durable = true, AutoDelete = false, Arguments =
                            {
                                {"x-queue-type", "classic"}
                            }
                        }
                    },
                    Exchanges =
                    {
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_01", Type = ExchangeTypes.Direct, Durable = true,
                            Queues =
                            {
                                new QueueBoundDescriptor
                                {
                                    Name = "demo_01_01_q", Durable = true, AutoDelete = false, Arguments =
                                    {
                                        {"x-queue-type", "stream"}
                                    }
                                }
                            }
                        },
                        new ExchangeBoundDescriptor
                        {
                            Name = "demo_01_02", Type = ExchangeTypes.Direct, Durable = true
                        }
                    }
                }
            }
        };

        var channel = this.serviceProvider.GetService<IModel>();

        await using var publisher = new PublisherBound(descriptor, this.dataFormatter, channel);

        var dateTime = new DateTime(1980, 1, 10, 0, 10, 0);
        var dec = 1.15m;
        var rating = 12.59;

        for (var i = 0; i < counter; i++)
        {
            var instance = new ComplexCls
            {
                Id = i,
                Name = $"name {i}",
                Type = ClsType.Complex,
                BirthDate = dateTime.AddDays(i),
                Description = $"description {i}",
                DecVal = dec + i,
                DoubleVal = rating,
                Alias = new[] { $"alias {i}", $"alias {i + 1}" },
                Parent = new ComplexCls { Id = 1, Name = $"parent {i}", Type = ClsType.Common },
                Children = new[]
                {
                    new ComplexCls{ Id = i, Name = $"child 1", DecVal = 0m, DoubleVal = 0d},
                    new ComplexCls{ Id = i, Name = $"child 2", DecVal = 0m, DoubleVal = 0d}
                },
                Tags = new Dictionary<string, string>
                {
                    {"tag0", "tag 1"},
                    {"tag1", "tag 2"},
                    {"tag3", "tag 3"}
                },
                Fields = new Dictionary<string, object>
                {
                    {"intVal", 0},
                    {"longVal", 0L},
                    {"doubleVal", 0.25},
                    {"decVal", 0.78m},
                    {"doubleVal_1", 0.0},
                    {"decVal_1", 0.0m},
                    {"par3", new DateTime(2020, 10, 10)},
                }
            };

            object message = new Message<ComplexCls>
            {
                Data = instance
            };

            await publisher.SendAsync(message);
        }

        Console.WriteLine("**** Press any key to close. ****");
        Console.ReadLine();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fullDuplex"></param>
    /// <returns></returns>
    internal async Task ExecuteRestPublisher(bool fullDuplex)
    {
        var descriptor = new RestPublisherBoundDescriptor
        {
            ServiceName = "MyAppDemo",
            Timeout = TimeSpan.FromMinutes(1),
            IncludeContent = true,
            Durable = true
        };

        var channel = this.serviceProvider.GetService<IModel>();
        var httpSerializer = this.serviceProvider.GetService<IHttpRestMessageSerializer>();

        IAsyncRestPublisher client = fullDuplex
            ? new FullDuplexRestPublisherBound(descriptor, channel, this.dataFormatter, httpSerializer)
            : new StatelessRestPublisherBound(descriptor, channel, this.dataFormatter, httpSerializer);

        Console.WriteLine();
        Console.WriteLine("**** Write your service request, payload is a json object (write 'exit' to escape) ****");
        Console.WriteLine();

        while (true)
        {
            try
            {
                var request = new RestRequest(ConsoleExt.ReadInput($"{nameof(RestRequest.Resource)}: {TabChar}").ExitOnCommand())
                {
                    Method = ConsoleExt.ReadInput($"{nameof(RestRequest.Method)}: {TabChar}",
                    input => Enum.Parse<HttpMethods>((input ?? "GET").ExitOnCommand(), true)),
                    Payload = ConsoleExt.ReadInput($"{nameof(RestRequest.Payload)}: {TabChar}",
                        json => this.jsonSerializer.Deserialize<object>(json.ExitOnCommand()))
                };

                var resp = await client.SendAsync<object>(request);

                Console.WriteLine(
                    $"Response: {TabChar}{nameof(resp.IsSuccessful)}: {resp.IsSuccessful}, {nameof(resp.StatusCode)}: {resp.StatusCode}, {nameof(resp.Content)}: {resp.Content}");

                Console.WriteLine(
                    $"Response.Body: {TabChar}{this.jsonSerializer.Serialize(resp.Data)}");

                Console.WriteLine();
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine($"Execution is over: {e.Message}");
                break;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something was happened: {e.Message}");
                Console.WriteLine();
            }
        }

        await Task.CompletedTask;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="counter"></param>
    /// <param name="fullDuplex"></param>
    /// <returns></returns>
    internal async Task ExecuteRestPublisher_Many(int counter, bool fullDuplex)
    {
        var descriptor = new RestPublisherBoundDescriptor
        {
            ServiceName = "MyAppDemo",
            Timeout = TimeSpan.FromMinutes(1),
            IncludeContent = true,
            Durable = true
        };

        var channel = this.serviceProvider.GetService<IModel>();
        var httpSerializer = this.serviceProvider.GetService<IHttpRestMessageSerializer>();

        IAsyncRestPublisher client = fullDuplex
            ? new FullDuplexRestPublisherBound(descriptor, channel, this.dataFormatter, httpSerializer)
            : new StatelessRestPublisherBound(descriptor, channel, this.dataFormatter, httpSerializer);

        var watch = new Stopwatch();

        var list = new List<Task>();

        watch.Start();

        for (var i = 1; i <= counter; i++)
        {
            var request = new RestRequest("api/demo")
            {
                Method = HttpMethods.Post,
                Payload = new { Id = i, Value = $"Custom value {i}", Price = i * 1.5 }
            };

            var tsk = client.SendAsync<object>(request)
                .ContinueWith(task =>
                {
                    try
                    {
                        var resp = task.Result;

                        Console.WriteLine(
                            $"Response: {TabChar}{nameof(resp.IsSuccessful)}: {resp.IsSuccessful}, {nameof(resp.StatusCode)}: {resp.StatusCode}, {nameof(resp.Data)}: {this.jsonSerializer.Serialize(resp.Data)}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception: {TabChar}{e.Message}");
                    }
                });

            list.Add(tsk);

            //await tsk;
        }

        Console.WriteLine("**** Waiting for all tasks completed ****");
        Task.WaitAll(list.ToArray());

        watch.Stop();

        Console.WriteLine($"**** Write any keyword to exit, milliseconds elapsed: {watch.Elapsed.TotalMilliseconds} ****");
        Console.ReadLine();

        await Task.CompletedTask;
    }
}
