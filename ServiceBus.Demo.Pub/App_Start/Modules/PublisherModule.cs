using RabbitMQ.Client;
using ServiceBus.Core.Extensions;
using ServiceBus.Demo.Common.Config;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceBus.Demo.Pub.Modules;

internal class PublisherModule : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        var appSettings = serviceRegister.Cache.Get<AppSettingsBase>();

        serviceRegister.Register<IConnectionFactory>(() => new ConnectionFactory
        {
            Uri = appSettings.EsbAddress,
            UserName = appSettings.EsbCredentials.Username,
            Password = appSettings.EsbCredentials.Password,
            DispatchConsumersAsync = true
        })
        .Register(provider => provider.GetService<IConnectionFactory>().CreateConnection())
        .Register(provider => provider.GetService<IConnection>().CreateChannel());
    }
}
