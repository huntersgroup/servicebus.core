using System.ComponentModel;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using ServiceBus.Demo.Common;
using ServiceBus.Demo.Common.Config;
using ServiceResolver.Ioc.SimpleInjector;
using SimpleInjector;

namespace ServiceBus.Demo.Pub;

/// <summary>
/// 
/// </summary>
internal static class AppInitializer
{
    internal static IServiceProvider GetServiceProvider()
    {
        ConsoleExt.WriteLine("### Choose the Serializer to use: ###", ConsoleColor.Blue);
        Console.WriteLine("### Press 1 for Json.Net");
        Console.WriteLine("### Press any key for Microsoft Text json");

        var formatType = ConsoleExt.ReadInput("*** Write the option: ", s => s.Equals("1") ? SerializerType.Json : SerializerType.Microsoft,
            ConsoleColor.Blue);

        Console.WriteLine();

        var serviceProvider = new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            Initializer = container =>
            {
                container.Options.DefaultLifestyle = Lifestyle.Singleton;
            }
        });

        var appSettings = BuildConfiguration().Get<AppSettingsBase>();

        serviceProvider.Cache.Upsert(formatType);
        serviceProvider.Cache.Upsert(appSettings);

        serviceProvider.Register<Application>();
        serviceProvider.Register<IServiceProvider>(() => serviceProvider);
        serviceProvider.RegisterModulesFrom(Assembly.GetExecutingAssembly());

        serviceProvider.Verify();

        return serviceProvider;
    }

    internal static IConfigurationRoot BuildConfiguration()
    {
        var env = Environment.GetEnvironmentVariable("NETCOREAPP_ENVIRONMENT");

        var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env}.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables();

        var configuration = builder.Build();

        return configuration;
    }

    internal static Task ExecuteRoutine(this Application app)
    {
        ConsoleExt.WriteLine("### Choose the demo to execute: ###", ConsoleColor.Blue);

        Console.WriteLine($"### Press 1 for {nameof(app.ExecutePublisherBound)}");
        Console.WriteLine($"### Press 2 for {nameof(app.ExecutePublisherBound_Many)}");
        Console.WriteLine($"### Press 3 for {nameof(app.ExecuteRestPublisher)}");
        Console.WriteLine($"### Press 4 for {nameof(app.ExecuteRestPublisher_Many)}");

        var code = ConsoleExt.ReadInput("*** Write the option: ", ConsoleColor.Blue);
        Console.WriteLine();

        switch (code)
        {
            case "1":
            {
                return app.ExecutePublisherBound();
            }
            case "2":
            {
                var counter = ConsoleExt.ReadInput("Write the number of message to send: ", Convert.ToInt32);

                return app.ExecutePublisherBound_Many(counter);
            }
            case "3":
            {
                var fullDuplex = ConsoleExt.ReadInput("Do you use a full duplex channel?: ", Convert.ToBoolean);

                return app.ExecuteRestPublisher(fullDuplex);
            }
            case "4":
            {
                var counter = ConsoleExt.ReadInput("Write the number of message to send: ", Convert.ToInt32);
                var fullDuplex = ConsoleExt.ReadInput("Do you use a full duplex channel?: ", Convert.ToBoolean);

                return app.ExecuteRestPublisher_Many(counter, fullDuplex);
            }
            default:
            {
                Console.WriteLine("No option available !!!");
                return Task.CompletedTask;
            }
        }
    }
}
