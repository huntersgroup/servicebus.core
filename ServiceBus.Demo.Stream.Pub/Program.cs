using System.Net;
using System.Text;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Stream.Client;
using RabbitMQ.Stream.Client.Reliable;
using ServiceBus.Demo.Common.Config;
using ServiceBus.Demo.Common.Extensions;

namespace ServiceBus.Demo.Stream.Pub;

internal class Application
{
    public Application()
    {
        this.Configuration = this.BuildConfiguration();
        this.AppSettings = this.Configuration.Get<AppSettingsBase>() ?? new AppSettingsBase();
    }

    public IConfigurationRoot Configuration { get; }

    public AppSettingsBase AppSettings { get; }

    private static async Task Main(params string[] args)
    {
        var app = new Application();
        var cred = app.AppSettings.EsbCredentials;
        var esb = app.AppSettings.EsbAddress;
        var endpoint = new DnsEndPoint(esb.Host, esb.Port);

        var streamCfg = new StreamSystemConfig
        {
            UserName = cred.Username,
            Password = cred.Password,
            Endpoints =
            [
                endpoint
            ],
            AddressResolver = new AddressResolver(endpoint)
        };

        var streamSystem = await StreamSystem.Create(streamCfg);

        Console.Write("write the number of message to produce: ");
        var count = Convert.ToInt64(Console.ReadLine());

        await RunProducer(streamSystem, "hello-stream", count);
        
        await streamSystem.Close();
    }

    private static async Task RunProducer(StreamSystem streamSystem, string streamName, long count)
    {
        await streamSystem.CreateStream(new StreamSpec(streamName)
        {
            MaxLengthBytes = 5_000_000,
            MaxAge = TimeSpan.FromMinutes(1)
        });

        // ProducerConfig has the property for filtering data
        var producer = await Producer.Create(new ProducerConfig(streamSystem, streamName));

        for (var i = 0; i < count; i++)
        {
            await producer.Send(new Message(Encoding.UTF8.GetBytes($"index: {i}, message: Hello, World"))
            {
                //Properties = new Properties()
                //{
                //    AbsoluteExpiryTime = DateTime.UtcNow.AddMinutes(5)
                //},
                //Annotations =
                //{

                //}
            });
        }

        Console.WriteLine(" [x] Press any key to exit");
        Console.ReadKey();

        await producer.Close();
    }
}
