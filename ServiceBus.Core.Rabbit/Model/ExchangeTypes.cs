namespace ServiceBus.Core.Model;

/// <summary>
/// Represents a suit of exchanges types definition.
/// </summary>
public enum ExchangeTypes
{
    /// <summary>
    /// Indicates a direct exchange
    /// </summary>
    Direct = 0,

    /// <summary>
    /// Indicates a broadcast exchange type.
    /// </summary>
    Fanout = 2,

    /// <summary>
    /// Indicates a simple header exchange type.
    /// </summary>
    Headers = 4,

    /// <summary>
    /// Indicates a topic exchange type.
    /// </summary>
    Topic = 5
}
