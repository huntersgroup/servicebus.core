namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
[Obsolete("Descriptor used by legacy client, so it will be removed in the next major version.")]
public class RestBrokerDescriptor : BrokerDescriptor
{
    /// <summary>
    /// Gets or sets the service name that identifies microservice component which hosts a subscriber.
    /// </summary>
    public string ServiceName { get; set; }

    /// <summary>
    /// Gets or sets a boolean value indicating if channel components are persistent.
    /// </summary>
    public bool Durable { get; set; }
}
