namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents an exchange descriptor associated with <see cref="BinderDescriptor"/> instance.
/// </summary>
public class ExchangeBoundDescriptor : ExchangeDescriptor
{
    /// <summary>
    /// Gets or set a binder to use with <see cref="Exchanges"/> instances.
    /// </summary>
    public BinderDescriptor Binder { get; set; } = new();

    /// <summary>
    /// Gets or sets the queues descriptors for this instance.
    /// </summary>
    public List<QueueBoundDescriptor> Queues { get; set; } = [];

    /// <summary>
    /// Gets or sets the exchange descriptor for this instance.
    /// </summary>
    public List<ExchangeBoundDescriptor> Exchanges { get; set; } = [];
}
