using System.Diagnostics;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a binder descriptor for queues and exchanges.
/// </summary>
[DebuggerDisplay("routingKey: {this.RoutingKey}")]
public class BinderDescriptor
{
    /// <summary>
    /// Gets or sets the routing key for this descriptor.
    /// </summary>
    public string RoutingKey { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the use for binding.
    /// </summary>
    public IDictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();
}
