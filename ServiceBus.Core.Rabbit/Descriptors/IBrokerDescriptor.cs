namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a generic definition for all descriptors.
/// </summary>
public interface IBrokerDescriptor
{
    /// <summary>
    /// Gets a boolean value used to identify if channel events must be registered
    /// </summary>
    bool EnableChannelEvents { get; }
}
