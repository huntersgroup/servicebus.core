using System.Diagnostics;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a common information for queue definitions.
/// </summary>
[DebuggerDisplay("prefix: {this.Prefix}, name: {this.Name}, durable: {this.Durable}, exclusive: {this.Exclusive}, auto-delete: {this.AutoDelete}")]
public class QueueDescriptor : ServiceDescriptor
{
    public QueueDescriptor()
    {
        this.Prefix = "esb/queue";
    }

    /// <summary>
    /// Gets or sets a boolean value indicating if queue must be persisted on disk.
    /// </summary>
    public bool Durable { get; set; } = false;

    /// <summary>
    /// Gets or sets a boolean value indicating if this queue is exclusively used by one subscriber.
    /// </summary>
    public bool Exclusive { get; set; } = false;

    /// <summary>
    /// Gets or sets a boolean value indicating if queue could be cancelled after subscribers lose binding connection.
    /// <para>This feature is enabled when <see cref="Durable"/> value is false.</para> 
    /// </summary>
    public bool AutoDelete { get; set; } = true;
}
