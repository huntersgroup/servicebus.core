namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a base common descriptor for all rest clients.
/// </summary>
public class RestBrokerBoundDescriptor : IBrokerDescriptor
{
    ///<inheritdoc />
    public bool EnableChannelEvents { get; set; }

    /// <summary>
    /// Gets or sets the service name that identifies microservice component which hosts a subscriber.
    /// </summary>
    public string ServiceName { get; set; }

    /// <summary>
    /// Gets or sets a boolean value indicating if channel components are persistent.
    /// </summary>
    public bool Durable { get; set; }
}
