namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a common way to define a retry policy for re-sending messages whenever you need to requeue them.
/// </summary>
public class RetryPolicyDescriptor
{
    /// <summary>
    /// Gets or sets the boolean values indicating if basic.nack or basic.reject messages must be forwarded using a Dead Letter Exchange
    /// </summary>
    public bool Enabled { get; set; } = false;

    /// <summary>
    /// Gets or sets the maximum number of retry to queue the nack | reject messages
    /// </summary>
    public short MaxRetry { get; set; } = 5;
        
    /// <summary>
    /// Gets or set the delay period to apply for all nack | reject messages to re-forward into the original queue.
    /// </summary>
    public TimeSpan Delayed { get; set; } = TimeSpan.FromSeconds(5);

    /// <summary>
    /// Gets or sets a boolean value indicating if all behind components (exchanges and queues) must be persistent on disk.
    /// </summary>
    public bool Durable { get; set; } = false;

    /// <summary>
    /// Gets or sets a boolean value indicating if all behind components (exchanges and queues) must be deleted (to verify)
    /// </summary>
    public bool AutoDelete { get; set; } = true;
}
