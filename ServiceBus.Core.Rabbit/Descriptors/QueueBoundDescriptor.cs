namespace ServiceBus.Core.Descriptors;

/// <summary>
/// Represents a queue descriptor associated with <see cref="BinderDescriptor"/> instance.
/// </summary>
public class QueueBoundDescriptor : QueueDescriptor
{
    /// <summary>
    /// Gets or sets a <see cref="BinderDescriptor"/> instance to use with any exchange instance.
    /// </summary>
    public BinderDescriptor Binder { get; set; } = new();
}
