using System.Diagnostics;

namespace ServiceBus.Core.Descriptors;

/// <summary>
/// 
/// </summary>
[DebuggerDisplay("name: {this.Name}, prefix: {this.Prefix}")]
public class ServiceDescriptor : IServiceDescriptor
{
    private string prefix;
    private string name;

    ///<inheritdoc />
    public string Prefix
    {
        get => this.prefix;
        set
        {
            if (string.Equals(this.prefix, value))
            {
                return;
            }

            this.prefix = value;
            this.SetFullName();
        }
    }

    ///<inheritdoc />
    public string Name
    {
        get => this.name;
        set
        {
            if (string.Equals(this.name, value))
            {
                return;
            }

            this.name = value;
            this.SetFullName();
        }
    }

    ///<inheritdoc />
    public string FullName { get; private set; } = "/";

    ///<inheritdoc />
    public IDictionary<string, object> Arguments { get; set; } = new Dictionary<string, object>();

    /// <summary>
    /// 
    /// </summary>
    private void SetFullName()
    {
        this.FullName = $"{this.Prefix}/{this.Name}";
    }
}
