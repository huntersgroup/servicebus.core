namespace ServiceBus.Core.Exceptions;

/// <summary>
/// 
/// </summary>
public class DuplicateDescriptorException : InvalidDescriptorException
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public DuplicateDescriptorException(string message)
        :base(message)
    {
    }

    /// <summary>
    /// A common ctor for this exception.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public DuplicateDescriptorException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    /// <summary>
    /// The names of all descriptors
    /// </summary>
    public IEnumerable<string> Names { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string PropertyPath { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Type Type { get; set; }
}
