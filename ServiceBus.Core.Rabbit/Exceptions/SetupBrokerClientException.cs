namespace ServiceBus.Core.Exceptions;

/// <summary>
/// 
/// </summary>
public class SetupBrokerClientException : InvalidDescriptorException
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    // ReSharper disable once UnusedMember.Global
    public SetupBrokerClientException(string message) : base(message)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public SetupBrokerClientException(string message, Exception innerException)
        : base(message, innerException)
    {
    }
}
