using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class DescriptorLegacyExtensions
{
    /// <summary>
    /// Verifies current status of the given descriptor instance.
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this BrokerDescriptor descriptor)
    {
        if (descriptor.Address == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Address)}"));
        }

        if (descriptor.Credentials == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Credentials)}"));
        }

        Verify(descriptor as dynamic);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    // ReSharper disable once UnusedParameter.Local
    private static void Verify(BrokerDescriptor descriptor)
    {
        // nothing to do !!!
        // this is a fallback method to execute whenever there's no overload to execute using dynamics.
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(BinderSubscriberDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Binder == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Binder)}"));
        }

        if (descriptor.Exchange == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Exchange)}"));
        }

        descriptor.Exchange.VerifyStatus($"{nameof(descriptor.Exchange)}", descriptor);

        Verify(descriptor as SubscriberDescriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(SubscriberDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Queue == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Queue)}"));
        }

        descriptor.Queue.VerifyStatus($"{nameof(descriptor.Queue)}", descriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(BinderPublisherDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Binder == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Binder)}"));
        }

        if (descriptor.Queue == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Queue)}"));
        }

        descriptor.Queue.VerifyStatus($"{nameof(descriptor.Queue)}", descriptor);

        Verify(descriptor as PublisherDescriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    private static void Verify(PublisherDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Exchange == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.Exchange)}"));
        }

        if (descriptor.OnBuildProperties == null)
        {
            descriptor.ThrowException(string.Format(DescriptorExtensions.NullPropertyMessage, $"{nameof(descriptor.OnBuildProperties)}"));
        }

        descriptor.Exchange.VerifyStatus($"{nameof(descriptor)}.{nameof(descriptor.Exchange)}", descriptor);
    }
}
