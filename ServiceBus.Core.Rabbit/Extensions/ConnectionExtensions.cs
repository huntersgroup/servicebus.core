using RabbitMQ.Client;
using ServiceBus.Core.Clients;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class ConnectionExtensions
{
    /// <summary>
    /// Create a new wrapped connection issued by the given connection builder function.
    /// </summary>
    /// <param name="connectionFactory"></param>
    /// <param name="connectionBuilder"></param>
    /// <returns></returns>
    [Obsolete("Not used anymore, use instead the default CreateConnection() method.", true)]
    public static IConnection BuildConnection(this IConnectionFactory connectionFactory, Func<IConnectionFactory, IConnection> connectionBuilder)
    {
        var conn = connectionBuilder(connectionFactory);
        return new ConnectionDecorator(conn);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <returns></returns>
    public static IModel CreateChannel(this IConnection connection)
    {
        return connection.CreateModel();
    }
}
