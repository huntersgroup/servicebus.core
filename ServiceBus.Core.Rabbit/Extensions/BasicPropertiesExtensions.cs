using System.Text;
using RabbitMQ.Client;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// Custom extension class for <see cref="IBasicProperties"/> instances
/// </summary>
public static class BasicPropertiesExtensions
{
    private const string RetryCounter = "x-esb/retry-counter";

    /// <summary>
    /// Increments a custom header value which represents a counter for retry policy messages.
    /// </summary>
    /// <param name="properties"></param>
    /// <returns></returns>
    public static short IncrementRetryCounter(this IBasicProperties properties)
    {
        short ret = 0;
            
        if (properties.ContainsHeader(RetryCounter))
        {
            ret = properties.GetRetryHeaderValue();
        }

        var inc = ret;
        properties.AddRetryHeader(++inc);

        return ret;
    }

    /// <summary>
    /// Gets a boolean value indicating if exists a custom value related to the given header.
    /// </summary>
    /// <param name="properties"></param>
    /// <param name="header"></param>
    /// <returns></returns>
    public static bool ContainsHeader(this IBasicProperties properties, string header)
    {
        return properties.Headers != null && properties.Headers.ContainsKey(header);
    }

    /// <summary>
    /// Adds the custom retry header indicating the given initial value
    /// </summary>
    /// <param name="basicProperties"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static IBasicProperties AddRetryHeader(this IBasicProperties basicProperties, short value)
    {
        return basicProperties.AddHeader(RetryCounter, value.ToString());
    }

    /// <summary>
    /// Add a custom header using the given key and relative value.
    /// </summary>
    /// <param name="basicProperties"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static IBasicProperties AddHeader(this IBasicProperties basicProperties, string key, string value)
    {
        basicProperties.Headers ??= new Dictionary<string, object>();

        basicProperties.Headers[key] = Encoding.UTF8.GetBytes(value);

        return basicProperties;
    }

    /// <summary>
    /// Gets the current retry value
    /// </summary>
    /// <param name="basicProperties"></param>
    /// <returns></returns>
    public static short GetRetryHeaderValue(this IBasicProperties basicProperties)
    {
        const short def = 0;

        if (basicProperties.Headers == null)
            return def;

        return basicProperties.Headers[RetryCounter] is not byte[] val ? def : Convert.ToInt16(Encoding.UTF8.GetString(val));
    }
}
