using System.Net;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class TranslatorExtensions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="restRequest"></param>
    /// <param name="dataFormatter"></param>
    /// <returns></returns>
    public static HttpRestRequest Translate(this RestRequest restRequest, IDataFormatter dataFormatter)
    {
        var req = new HttpRestRequest
        {
            Method = restRequest.Method,
            Version = HttpConstants.DefaultHttpVersion,
            Resource = restRequest.Resource,
            Content = dataFormatter.Encode(restRequest.Payload)
        };
            
        foreach (var requestHeader in restRequest.Headers)
        {
            req.Headers.Add(requestHeader.Key, requestHeader.Value);
        }

        return req;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="httpResponse"></param>
    /// <param name="dataFormatter"></param>
    /// <returns></returns>
    public static RestResponse<TData> Translate<TData>(this HttpRestResponse httpResponse, IDataFormatter dataFormatter)
    {
        var res = new RestResponse<TData>();

        try
        {
            res.InitRestResponse(httpResponse);

            res.Data = httpResponse.ContentAsV2<TData>(dataFormatter);
        }
        catch (Exception ex)
        {
            res.Exception = ex;
            res.ResponseStatus = ResponseStatus.Error;
        }
            
        return res;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="httpResponse"></param>
    /// <returns></returns>
    public static RestResponse Translate(this HttpRestResponse httpResponse)
    {
        var res = new RestResponse();
            
        res.InitRestResponse(httpResponse);

        return res;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="restResponse"></param>
    /// <param name="httpResponse"></param>
    private static void InitRestResponse(this RestResponse restResponse, HttpRestResponse httpResponse)
    {
        restResponse.StatusCode = (HttpStatusCode)httpResponse.StatusCode;
        restResponse.StatusDescription = httpResponse.StatusDescription;
        restResponse.ResponseStatus = ResponseStatus.Completed;
        restResponse.RawBytes = httpResponse.Content;

        try
        {
            foreach (var responseHeader in httpResponse.Headers)
            {
                restResponse.Headers.Add(responseHeader.Key, responseHeader.Value);
            }
        }
        catch (Exception ex)
        {
            restResponse.Exception = ex;
            restResponse.ResponseStatus = ResponseStatus.Error;
        }
    }

    /// <summary>
    /// Deserialize the content for the given message.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="restMessage"></param>
    /// <param name="formatter"></param>
    /// <returns></returns>
    public static TData ContentAsV2<TData>(this HttpRestMessage restMessage, IDataFormatter formatter)
    {
        var contentType = restMessage.Headers.GetFirstHeaderValue(HttpHeaders.ContentType);

        if (restMessage.Content == null || restMessage.Content.Length == 0)
        {
            return default;
        }

        if (contentType != null && contentType.StartsWith(HttpHeaders.JsonContentType))
        {
            return formatter.Decode<TData>(restMessage.Content);
        }

        var text = formatter.Encoder.GetString(restMessage.Content);
        dynamic ret = Convert.ChangeType(text, typeof(TData));

        return ret;
    }
}
