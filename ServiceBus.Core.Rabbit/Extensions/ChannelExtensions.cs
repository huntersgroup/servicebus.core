using RabbitMQ.Client;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// Represents a suit of extension methods for extending <see cref="IModel"/> instance configuration using custom settings.
/// </summary>
public static class ChannelExtensions
{
    private static readonly BinderDescriptor DefaultBinder = new ();

    /// <summary>
    /// Builds an exchange using the given descriptor.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <returns></returns>
    public static IModel BuildExchange(this IModel channel, ExchangeDescriptor exchange)
    {
        channel.ExchangeDeclare(exchange.FullName, exchange.Type.GetName(), exchange.Durable, exchange.AutoDelete, exchange.Arguments);

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <returns></returns>
    public static IModel BuildExchangeBound(this IModel channel, ExchangeBoundDescriptor exchange)
    {
        if (exchange == null)
        {
            return channel;
        }

        channel.BuildExchange(exchange);

        // binding with queues.
        foreach (var queue in exchange.Queues)
        {
            var binder = queue.Binder ?? DefaultBinder;

            channel.BuildQueue(queue)
                .BuildBinder(exchange, binder, queue);
        }

        // binding with exchanges.
        foreach (var destinationEx in exchange.Exchanges)
        {
            var binder = destinationEx.Binder ?? DefaultBinder;

            channel.BuildExchangeBound(destinationEx)
                .BuildBinder(exchange, binder, destinationEx);
        }

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static ExchangeDescriptor BuildDlxExchange(this IModel channel, RetryPolicyDescriptor retryPolicy, QueueDescriptor queue)
    {
        if (retryPolicy is not { Enabled: true })
        {
            return null;
        }

        var dlxExchange = retryPolicy.BuildDlxExchange(queue);
        channel.BuildExchange(dlxExchange);

        var retryExchange = retryPolicy.BuildRetryExchange(queue);
        channel.BuildExchange(retryExchange);

        var retryQueue = retryPolicy.BuildRetryQueue(queue, retryExchange);
        channel.BuildQueue(retryQueue);

        channel.BuildBinder(dlxExchange, new BinderDescriptor(), retryQueue);
        channel.BuildBinder(retryExchange, new BinderDescriptor(), queue);

        return dlxExchange;
    }

    /// <summary>
    /// Builds a queue using the given descriptor.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static IModel BuildQueue(this IModel channel, QueueDescriptor queue)
    {
        channel.QueueDeclare(queue.FullName, queue.Durable, queue.Exclusive, queue.AutoDelete, queue.Arguments);

        return channel;
    }

    /// <summary>
    /// Builds a custom binding for the current descriptor.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchangeBinder"></param>
    /// <returns></returns>
    public static IModel BuildBinder(this IModel channel, IExchangeBinderDescriptor exchangeBinder)
    {
        channel.BuildBinder(exchangeBinder.Exchange, exchangeBinder.Binder, exchangeBinder.Queue);

        return channel;
    }

    /// <summary>
    /// Makes a binding between exchange a queue
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="exchange"></param>
    /// <param name="binder"></param>
    /// <returns></returns>
    public static IModel BuildBinder(this IModel channel, ExchangeDescriptor exchange, BinderDescriptor binder, QueueDescriptor queue)
    {
        binder ??= DefaultBinder;

        channel.QueueBind(queue.FullName, exchange.FullName, binder.RoutingKey, binder.Arguments);
            
        return channel;
    }

    /// <summary>
    /// Makes a binding between exchanges
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="source"></param>
    /// <param name="binder"></param>
    /// <param name="destination"></param>
    /// <returns></returns>
    public static IModel BuildBinder(this IModel channel, ExchangeDescriptor source, BinderDescriptor binder, ExchangeDescriptor destination)
    {
        channel.ExchangeBind(destination.FullName, source.FullName, binder.RoutingKey, binder.Arguments);

        return channel;
    }

    /// <summary>
    /// Builds a custom consumer binding between queue and consumer.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="queue"></param>
    /// <param name="consumer"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public static IModel BindConsumer(this IModel channel, QueueDescriptor queue, IBasicConsumer consumer, IDictionary<string, object> args = null)
    {
        channel.BasicConsume(queue: queue.FullName, consumer: consumer, arguments: args);
            
        return channel;
    }

    /// <summary>
    /// Publish a message into the given exchange, using the given parameters.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route">The current route used to forward message.</param>
    /// <param name="properties">Custom message properties for message.</param>
    /// <param name="body">The original message encoded.</param>
    /// <returns></returns>
    public static IModel PublishMessage(this IModel channel, ExchangeDescriptor exchange, string route, IBasicProperties properties, byte[] body)
    {
        channel.BasicPublish(exchange.FullName, route, properties, body);
            
        return channel;
    }

    /// <summary>
    /// Publish a message into the given exchange, using the given parameters.
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route">The current route used to forward message.</param>
    /// <param name="properties">Custom message properties for message.</param>
    /// <param name="body">The original message encoded.</param>
    /// <returns></returns>
    public static async ValueTask PublishMessageAsync(this IModel channel, ExchangeDescriptor exchange, string route, IBasicProperties properties, byte[] body)
    {
        await Task.Factory.StartNew(() => channel.BasicPublish(exchange.FullName, route, properties, body));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route"></param>
    /// <param name="properties"></param>
    /// <param name="body"></param>
    /// <returns></returns>
    public static IModel PublishMessage(this IModel channel, ExchangeDescriptor exchange, string route, IBasicProperties properties, ReadOnlyMemory<byte> body)
    {
        channel.BasicPublish(exchange.FullName, route, properties, body);

        return channel;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="exchange"></param>
    /// <param name="route"></param>
    /// <param name="properties"></param>
    /// <param name="body"></param>
    /// <returns></returns>
    public static async ValueTask PublishMessageAsync(this IModel channel, ExchangeDescriptor exchange, string route, IBasicProperties properties, ReadOnlyMemory<byte> body)
    {
        await Task.Factory.StartNew(() => channel.BasicPublish(exchange.FullName, route, properties, body));
    }

    /// <summary>
    /// Cancel all consumer tags registered by the given channel
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="consumer"></param>
    public static void Cancel(this IModel channel, DefaultBasicConsumer consumer)
    {
        if (channel == null || consumer == null)
        {
            return;
        }

        foreach (var consumerTag in consumer.ConsumerTags)
        {
            try
            {
                channel.BasicCancel(consumerTag);
            }
            catch (Exception)
            {
                // nothing to do here!!
            }
        }
    }

    /// <summary>
    /// Cancel all consumer tags registered by the given channel
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="consumer"></param>
    public static void Cancel(this IModel channel, AsyncDefaultBasicConsumer consumer)
    {
        if (channel == null || consumer == null)
        {
            return;
        }

        foreach (var consumerTag in consumer.ConsumerTags)
        {
            try
            {
                channel.BasicCancel(consumerTag);
            }
            catch (Exception)
            {
                // nothing to do here!!
            }
        }
    }
}
