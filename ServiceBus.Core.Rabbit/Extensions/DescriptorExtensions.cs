using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Model;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// Represents a custom way to extend some features for Descriptors instances
/// </summary>
public static class DescriptorExtensions
{
    internal const string NullPropertyMessage = "The {0} property cannot be null.";
    private const string NullOrEmptyPropertyMessage = "The {0} property cannot be null or empty.";
    private const string QueueTypeAttr = "x-queue-type";
    private const string QueueTypeClassic = "classic";

    /// <summary>
    /// Gets the name of the given service descriptor, using its own prefix.
    /// <para>
    /// The channel name is composed concatenating prefix and name, like this : "{descriptor.Prefix}/{descriptor.Name}"
    /// </para>
    /// </summary>
    /// <param name="descriptor"></param>
    /// <returns></returns>
    [Obsolete("Use the property FullName instead.", true)]
    public static string GetChannelName(this IServiceDescriptor descriptor)
    {
        return $"{descriptor.Prefix}/{descriptor.Name}";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static ExchangeDescriptor BuildDlxExchange(this RetryPolicyDescriptor retryPolicy, QueueDescriptor queue)
    {
        return new ExchangeDescriptor
        {
            Name = $"{queue.Name}/_dlx",
            Type = ExchangeTypes.Direct,
            Durable = retryPolicy.Durable,
            AutoDelete = retryPolicy.AutoDelete
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <returns></returns>
    public static ExchangeDescriptor BuildRetryExchange(this RetryPolicyDescriptor retryPolicy, QueueDescriptor queue)
    {
        return new ExchangeDescriptor
        {
            Name = $"{queue.Name}/_retry",
            Type = ExchangeTypes.Direct,
            Durable = retryPolicy.Durable,
            AutoDelete = retryPolicy.AutoDelete
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="retryPolicy"></param>
    /// <param name="queue"></param>
    /// <param name="dlxExchange"></param>
    /// <returns></returns>
    public static QueueDescriptor BuildRetryQueue(this RetryPolicyDescriptor retryPolicy, QueueDescriptor queue, ExchangeDescriptor dlxExchange)
    {
        return new QueueDescriptor
        {
            Name = $"{queue.Name}/_retry",
            Durable = retryPolicy.Durable,
            AutoDelete = retryPolicy.AutoDelete,
            Arguments = new Dictionary<string, object>
            {
                {"x-message-ttl", Convert.ToInt64(retryPolicy.Delayed.TotalMilliseconds)},
                {"x-dead-letter-exchange", dlxExchange.FullName},
                {QueueTypeAttr, queue.Arguments != null && queue.Arguments.ContainsKey(QueueTypeAttr) ? queue?.Arguments[QueueTypeAttr]: QueueTypeClassic}
            }
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this RestPublisherBoundDescriptor descriptor)
    {
        descriptor.ThrowIfNull(nameof(descriptor));

        descriptor.ContextType.ThrowIfNull(nameof(RestPublisherBoundDescriptor.ContextType));

        (descriptor as RestBrokerBoundDescriptor).VerifyStatus();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this RestBrokerBoundDescriptor descriptor)
    {
        descriptor.ServiceName.ThrowIfNull(nameof(RestBrokerBoundDescriptor.ServiceName));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this PublisherBoundDescriptor descriptor)
    {
        descriptor.ThrowIfNull(nameof(descriptor));

        if (descriptor.Exchanges == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Exchanges)}"));
        }

        descriptor.Exchanges.RemoveAll(boundDescriptor => boundDescriptor == null);

        if (!descriptor.Exchanges.Any())
        {
            descriptor.ThrowException("There is no exchange for the given descriptor, at least there should be a one.");
        }

        // verify duplicity
        descriptor.Exchanges.ThrowOnDuplicity(descriptor, string.Empty);

        var index = -1;

        foreach (var exchange in descriptor.Exchanges)
        {
            index++;
            descriptor.Verify(exchange, $"{nameof(descriptor.Exchanges)}[{index}]");
        }
    }

    /// <summary>
    /// Validates the given descriptor
    /// </summary>
    /// <param name="descriptor"></param>
    public static void VerifyStatus(this SubscriberBoundDescriptor descriptor)
    {
        descriptor.ThrowIfNull(nameof(descriptor));

        if (descriptor.PrefetchCount < 1)
        {
            descriptor.ThrowException($"The {nameof(descriptor.PrefetchCount)} property cannot be less than 1.");
        }

        if (descriptor.Queue == null)
        {
            descriptor.ThrowException(string.Format(NullPropertyMessage, $"{nameof(descriptor.Queue)}"));
        }
        else
        {
            descriptor.Queue.VerifyStatus(string.Empty, descriptor);
        }

        descriptor.Exchange?.VerifyStatus(string.Empty, descriptor);

        descriptor.RetryPolicy?.VerifyStatus(string.Empty, descriptor);
    }

    private static void VerifyStatus(this RetryPolicyDescriptor descriptor, string parentProperty, IBrokerDescriptor brokerDescriptor)
    {
        if (descriptor == null)
        {
            return;
        }

        if (descriptor.Enabled)
        {
            if (descriptor.MaxRetry < 1)
            {
                brokerDescriptor.ThrowException($"The {parentProperty}.{nameof(descriptor.MaxRetry)} property cannot be less than 1.");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="exchangeBound"></param>
    /// <param name="rootParent"></param>
    private static void Verify(this IBrokerDescriptor descriptor, ExchangeBoundDescriptor exchangeBound, string rootParent)
    {
        var index = -1;

        if (exchangeBound.Queues != null)
        {
            exchangeBound.Queues.RemoveAll(boundDescriptor => boundDescriptor == null);

            exchangeBound.Queues.ThrowOnDuplicity(descriptor, rootParent);

            foreach (var queue in exchangeBound.Queues)
            {
                index++;
                queue.VerifyStatus($"{rootParent}.{nameof(exchangeBound.Queues)}[{index}]", descriptor);
            }
        }

        index = -1;

        if (exchangeBound.Exchanges != null)
        {
            exchangeBound.Exchanges.RemoveAll(boundDescriptor => boundDescriptor == null);

            exchangeBound.Exchanges.ThrowOnDuplicity(descriptor, rootParent);

            foreach (var exchange in exchangeBound.Exchanges)
            {
                index++;
                descriptor.Verify(exchange, $"{rootParent}.{nameof(exchangeBound.Exchanges)}[{index}]");
            }
        }

        exchangeBound.VerifyStatus(rootParent, descriptor);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceDescriptor"></param>
    /// <param name="parentProperty"></param>
    /// <param name="descriptor"></param>
    internal static void VerifyStatus(this IServiceDescriptor serviceDescriptor, string parentProperty, IBrokerDescriptor descriptor)
    {
        if (string.IsNullOrWhiteSpace(serviceDescriptor.Name))
        {
            descriptor.ThrowException(string.Format(NullOrEmptyPropertyMessage, $"{parentProperty}.{nameof(serviceDescriptor.Name)}"));
        }

        if (string.IsNullOrWhiteSpace(serviceDescriptor.Prefix))
        {
            descriptor.ThrowException(string.Format(NullOrEmptyPropertyMessage, $"{parentProperty}.{nameof(serviceDescriptor.Prefix)}"));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="message"></param>
    internal static void ThrowException(this IBrokerDescriptor descriptor, string message)
    {
        throw new InvalidDescriptorException(message)
        {
            ClientType = descriptor.GetType()
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptors"></param>
    /// <param name="parent"></param>
    /// <param name="propertyPath"></param>
    /// <exception cref="DuplicateDescriptorException"></exception>
    internal static void ThrowOnDuplicity(this IEnumerable<IServiceDescriptor> descriptors, IBrokerDescriptor parent, string propertyPath)
    {
        var res = descriptors.GroupBy(desc => desc.FullName)
            .Where(grouping => grouping.Count() > 1)
            .ToList();

        if (res.Any())
        {
            throw new DuplicateDescriptorException("Some descriptors with the same full name were found, see exception details.")
            {
                ClientType = descriptors.GetType(),
                PropertyPath = propertyPath,
                Names = res.Select(grouping => grouping.Key).ToList(),
                Type = descriptors.FirstOrDefault()?.GetType()
            };
        }
    }
}
