using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Events;

/// <summary>
/// 
/// </summary>
/// <param name="decoder"></param>
/// <param name="msgFormat"></param>
public class MessageDecoderEventArgs(BasicDeliverEventArgs eventArg, IDataDecoder decoder, MessageFormat msgFormat)
    : MessageDeliverEventArgs<object>(eventArg.Body), IMessageDecoderEventArgs
{
    ///<inheritdoc />
    public IMessage<TData> Decode<TData>()
    {
        IMessage<TData> message;
        
        switch (msgFormat)
        {
            case MessageFormat.Header:
            {
                message = decoder.Decode<Message<TData>>(eventArg.Body);
                break;
            }
            case MessageFormat.Headless:
            {
                var body = decoder.Decode<TData>(eventArg.Body);

                var properties = eventArg.BasicProperties;

                message = new Message<TData>
                {
                    Route = eventArg.RoutingKey,
                    Persistent = properties?.Persistent,
                    Created = properties?.Timestamp.UnixTime.FromUnixDateTime(),
                    Data = body
                };

                break;
            }
            default:
            {
                throw new InvalidDescriptorException(
                    $"The current message format indicated in the current subscriber descriptor is not supported, value: {msgFormat}");
            }
        }

        this.Message = (IMessage<object>)message;

        return message;
    }
}
