namespace ServiceBus.Core.Threading;

/// <summary>
/// Represents a <see cref="T:System.Threading.Tasks.Task`1"></see> producer as <see cref="T:System.Threading.Tasks.TaskCompletionSource`1"/> class, but injects a translator for result data.
/// </summary>
/// <typeparam name="TIn"></typeparam>
/// <typeparam name="TResult"></typeparam>
public class TranslatorTaskCompletionSource<TIn, TResult> : TaskCompletionSource<TResult>, ITranslatorTaskCompletionSource
{
    private readonly Func<TIn, TResult> translator;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="translator"></param>
    public TranslatorTaskCompletionSource(Func<TIn, TResult> translator)
    {
        this.IngressType = typeof(TIn);
        this.ResultType = typeof(TResult);

        this.translator = translator;
    }

    /// <inheritdoc />
    public Type IngressType { get; }

    /// <inheritdoc />
    public Type ResultType { get; }

    /// <inheritdoc />
    public void SetResult(object result)
    {
        if (result is TIn typedResult)
        {
            var translated = this.translator(typedResult);
            base.SetResult(translated);
        }
        else
        {
            throw new InvalidCastException($"The given result reference is not compatible with Task, resultType: {result.GetType()}");
        }
    }

    /// <inheritdoc />
    public bool TrySetResult(object result)
    {
        if (result is TIn typedResult)
        {
            var translated = this.translator(typedResult);
            base.SetResult(translated);

            return true;
        }

        return false;
    }
}

/// <summary>
/// Represents a common contract used for specialized TaskCompletionSource
/// </summary>
public interface ITranslatorTaskCompletionSource
{
    /// <summary>
    /// Gets the type indicating the instance in input to translate
    /// </summary>
    Type IngressType { get; }

    /// <summary>
    /// Gets the type indicating the type in output after translation.
    /// </summary>
    Type ResultType { get; }
        
    /// <summary>
    /// Sets the result for this instance, checking the given instance is compatible with <see cref="IngressType"/>.
    /// </summary>
    /// <param name="result"></param>
    void SetResult(object result);

    /// <summary>
    /// Tries to set the result checking the input type id It's compatible with <see cref="IngressType"/>.
    /// </summary>
    /// <param name="result"></param>
    /// <returns>return true if result is compatible with <see cref="IngressType"/> and if this operation is done without errors.</returns>
    bool TrySetResult(object result);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="exception"></param>
    void SetException(Exception exception);
}
