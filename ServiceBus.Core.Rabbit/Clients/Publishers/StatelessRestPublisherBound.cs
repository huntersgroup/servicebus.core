using System.Collections.Concurrent;
using System.Globalization;
using System.Net;
using RabbitMQ.Client;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// 
/// </summary>
/// <param name="descriptor"></param>
/// <param name="channel"></param>
/// <param name="dataFormatter"></param>
/// <param name="messageSerializer"></param>
public class StatelessRestPublisherBound(
    RestPublisherBoundDescriptor descriptor,
    IModel channel,
    IDataFormatter dataFormatter,
    IHttpRestMessageSerializer messageSerializer)
    : AsyncRestPublisher(descriptor, channel, dataFormatter, messageSerializer)
{
    private static readonly byte[] EmptyByteArray = [];
    private readonly ConcurrentDictionary<Type, dynamic> dynamicRestResponses = new();
    private readonly RestResponse defaultResponse = new()
    {
        ResponseStatus = ResponseStatus.Completed,
        StatusCode = HttpStatusCode.OK,
        RawBytes = EmptyByteArray,
        Content = string.Empty
    };

    ///<inheritdoc />
    public override async Task<RestResponse> SendAsync(RestRequest request)
    {
        await this.SendRequestAsync(request);

        return this.defaultResponse;
    }

    ///<inheritdoc />
    public override async Task<RestResponse<TData>> SendAsync<TData>(RestRequest request)
    {
        RestResponse<TData> response;

        var type = typeof(TData);

        if (!this.dynamicRestResponses.TryGetValue(type, out var dynResponse))
        {
            response = new RestResponse<TData>
            {
                ResponseStatus = ResponseStatus.Completed,
                StatusCode = HttpStatusCode.OK,
                RawBytes = EmptyByteArray,
                Content = string.Empty,
                Data = default
            };

            this.dynamicRestResponses.TryAdd(type, response);
        }
        else
        {
            response = dynResponse;
        }

        await this.SendRequestAsync(request);

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    private async ValueTask SendRequestAsync(RestRequest request)
    {
        request.AddOrOverrideHeader(HttpHeaders.ContentType, this.Descriptor.ContextType);

        var timeout = request.Timeout ?? this.Descriptor.Timeout ?? TimeSpan.FromSeconds(15);

        var properties = this.Channel.CreateBasicProperties();
        properties.CorrelationId = request.Id;
        properties.Timestamp = new AmqpTimestamp(DateTime.UtcNow.AsUnixDateTime());
        properties.Expiration = timeout.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);

        var httpRequest = request.Translate(this.DataFormatter);
        var payload = this.MessageSerializer.SerializeRequest(httpRequest);

        await this.Channel.PublishMessageAsync(this.Exchange, string.Empty, properties, payload);
    }
}
