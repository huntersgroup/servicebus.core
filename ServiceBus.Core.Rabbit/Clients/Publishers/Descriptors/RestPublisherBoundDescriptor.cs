using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Clients.Publishers.Descriptors;

/// <summary>
/// Represents a publisher descriptor for all rest clients.
/// </summary>
public class RestPublisherBoundDescriptor : RestBrokerBoundDescriptor
{
    /// <summary>
    /// Gets or sets the response deserialized as string from underlying channel.
    /// </summary>
    public bool IncludeContent { get; set; }

    /// <summary>
    /// Gets or sets the default timeout for every request made by rest publishers.
    /// </summary>
    public TimeSpan? Timeout { get; set; }

    /// <summary>
    /// Gets or sets the default content type for all request made by rest publishers.
    /// </summary>
    public string ContextType { get; set; } = HttpHeaders.JsonContentType;
}
