using System.Diagnostics;

namespace ServiceBus.Core.Clients.Publishers.Descriptors;

/// <summary>
/// Represent a configuration settings for publishers
/// </summary>
[DebuggerDisplay("enabled: {this.Enabled}, timeout: {this.Timeout}, waitFor: {this.WaitFor}")]
public class DeliveryConfirmation
{
    /// <summary>
    /// Gets or sets a value indicating that confirmation is enabled
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets a timeout value to use when <see cref="WaitFor"/> property is true.
    /// <para>
    /// In the case of null value of this property, It could be assumed a default value (a few minutes).
    /// </para>
    /// </summary>
    public TimeSpan? Timeout { get; set; }

    /// <summary>
    /// Gets a boolean value indicating if publishers must be attended for confirms, using the property <see cref="Timeout"/> value if set, otherwise it could be set a default value.
    /// </summary>
    public bool WaitFor { get; set; }
}
