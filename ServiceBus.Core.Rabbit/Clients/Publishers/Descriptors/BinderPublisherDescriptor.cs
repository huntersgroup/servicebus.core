using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Publishers.Descriptors;

/// <summary>
/// Represents a custom descriptor indicating a custom <see cref="Binder"/> and <see cref="Queue"/> descriptor.
/// </summary>
[Obsolete("Descriptor used by legacy client, so it will be removed in the next major version.")]
public class BinderPublisherDescriptor : PublisherDescriptor, IExchangeBinderDescriptor
{
    /// <summary>
    /// Gets or sets the binder descriptor.
    /// </summary>
    public BinderDescriptor Binder { get; set; } = new();

    /// <summary>
    /// Gets or sets the queue descriptor.
    /// </summary>
    public QueueDescriptor Queue { get; set; } = new();
}
