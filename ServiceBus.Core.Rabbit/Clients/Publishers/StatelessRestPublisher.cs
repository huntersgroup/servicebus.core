using System.Collections.Concurrent;
using System.Globalization;
using System.Net;
using RabbitMQ.Client;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// 
/// </summary>
[Obsolete("Use StatelessRestPublisherBound instead, it will be removed in the next major version.")]
internal class StatelessRestPublisher : BrokerClient, IRestPublisher
{
    private readonly ExchangeDescriptor exchangeDescriptor;
    private readonly RestPublisherDescriptor descriptor;
    private readonly IHttpRestMessageSerializer messageSerializer;
    private readonly RestResponse defaultResponse;
    private readonly ConcurrentDictionary<Type, dynamic> dynamicRestResponses;
    private static readonly byte[] EmptyByteArray = [];

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="dataFormatter"></param>
    /// <param name="messageSerializer"></param>
    public StatelessRestPublisher(RestPublisherDescriptor descriptor, IDataFormatter dataFormatter, IHttpRestMessageSerializer messageSerializer)
        : base(descriptor)
    {
        this.descriptor = descriptor;
        this.DataFormatter = dataFormatter;
        this.messageSerializer = messageSerializer;

        this.exchangeDescriptor = new ExchangeDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueDescriptor = new QueueDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueBinderDescriptor = new BinderDescriptor();

        // prepares working queue, exchange and working binder for queue flow.
        this.Channel.BuildExchange(this.exchangeDescriptor)
            .BuildQueue(workingQueueDescriptor)
            .BuildBinder(this.exchangeDescriptor, workingQueueBinderDescriptor, workingQueueDescriptor);

        this.defaultResponse = new RestResponse
        {
            ResponseStatus = ResponseStatus.Completed,
            StatusCode = HttpStatusCode.OK,
            RawBytes = EmptyByteArray,
            Content = string.Empty
        };

        this.dynamicRestResponses = new ConcurrentDictionary<Type, dynamic>();
    }

    /// <summary>
    /// 
    /// </summary>
    public IDataFormatter DataFormatter { get; }

    /// <inheritdoc/>
    public Task<RestResponse> SendAsync(RestRequest request)
    {
        return Task.Factory.StartNew(() =>
        {
            this.SendRequest(request);
            return this.defaultResponse;
        });
    }

    /// <inheritdoc/>
    public Task<RestResponse<TData>> SendAsync<TData>(RestRequest request)
    {
        RestResponse<TData> response;

        var type = typeof(TData);

        if (!this.dynamicRestResponses.TryGetValue(type, out var dynResponse))
        {
            response = new RestResponse<TData>
            {
                ResponseStatus = ResponseStatus.Completed,
                StatusCode = HttpStatusCode.OK,
                RawBytes = EmptyByteArray,
                Content = string.Empty,
                Data = default
            };

            this.dynamicRestResponses.TryAdd(type, response);
        }
        else
        {
            response = dynResponse;
        }

        return Task.Factory.StartNew(() =>
        {
            this.SendRequest(request);
            return response;
        });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    private void SendRequest(RestRequest request)
    {
        request.AddOrOverrideHeader(HttpHeaders.ContentType, this.descriptor.ContextType);

        var timeout = request.Timeout ?? this.descriptor.Timeout ?? TimeSpan.FromSeconds(15);

        var properties = this.Channel.CreateBasicProperties();
        properties.CorrelationId = request.Id;
        properties.Timestamp = new AmqpTimestamp(DateTime.UtcNow.AsUnixDateTime());
        properties.Expiration = timeout.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);

        var httpRequest = request.Translate(this.DataFormatter);
        var payload = this.messageSerializer.SerializeRequest(httpRequest);

        this.Channel.PublishMessage(this.exchangeDescriptor, string.Empty, properties, payload);
    }
}
