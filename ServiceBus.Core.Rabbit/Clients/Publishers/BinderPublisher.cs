using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// Represents a publisher that uses binding logic with queues.
/// </summary>
[Obsolete("Use PublisherBound instead, it will be removed in the next major version.")]
public class BinderPublisher : CommonPublisher
{
    /// <summary>
    /// Creates a new instance of <see cref="BinderPublisher"/>.
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="encoder"></param>
    public BinderPublisher(BinderPublisherDescriptor descriptor, IDataEncoder encoder)
        : base(descriptor, encoder)
    {
        try
        {
            this.Channel.BuildQueue(descriptor.Queue)
                .BuildBinder(descriptor);
        }
        catch (Exception ex)
        {
            throw new SetupBrokerClientException("An exception was thrown on setup publisher, see inner exception for details", ex)
            {
                ClientType = typeof(BinderPublisher)
            };
        }
    }
}
