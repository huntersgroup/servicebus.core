using System.Collections.Concurrent;
using System.Globalization;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;
using ServiceBus.Core.Threading;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// A Rest client enabled to establish a full duplex connection with backend Rest service
/// </summary>
[Obsolete("Use FullDuplexRestPublisherBound instead, it will be removed in the next major version.")]
internal class FullDuplexRestPublisher : BrokerClient, IRestPublisher
{
    private readonly string replyQueueName;
    private readonly ExchangeDescriptor exchangeDescriptor;
    private readonly RestPublisherDescriptor descriptor;
    private readonly IHttpRestMessageSerializer messageSerializer;
    private readonly Action<RestResponse> afterTranslateAction = _ => { };
    private readonly ConcurrentDictionary<string, ITranslatorTaskCompletionSource> callBackResponses;
    private EventingBasicConsumer consumer;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="dataFormatter"></param>
    /// <param name="messageSerializer"></param>
    public FullDuplexRestPublisher(RestPublisherDescriptor descriptor, IDataFormatter dataFormatter, IHttpRestMessageSerializer messageSerializer)
        : base(descriptor)
    {
        this.descriptor = descriptor;
        this.DataFormatter = dataFormatter;
        this.messageSerializer = messageSerializer;

        this.exchangeDescriptor = new ExchangeDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueDescriptor = new QueueDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueBinderDescriptor = new BinderDescriptor();

        // prepares working queue, exchange and working binder for queue flow.
        this.Channel.BuildExchange(this.exchangeDescriptor)
            .BuildQueue(workingQueueDescriptor)
            .BuildBinder(this.exchangeDescriptor, workingQueueBinderDescriptor, workingQueueDescriptor);

        // prepares callback queue, and callback binder for callback flow.
        this.replyQueueName = this.Channel.QueueDeclare().QueueName;
        this.Channel.QueueBind(queue: this.replyQueueName, exchange: this.exchangeDescriptor.FullName, this.replyQueueName);

        this.callBackResponses = new ConcurrentDictionary<string, ITranslatorTaskCompletionSource>();

        if (descriptor.IncludeContent)
        {
            this.afterTranslateAction = response =>
            {
                response.Content = this.messageSerializer.Encoder.GetString(response.RawBytes);
            };
        }

        this.consumer = new EventingBasicConsumer(this.Channel);

        this.consumer.Received += this.OnReceived;

        this.Channel.BasicConsume(this.replyQueueName, autoAck: true, this.consumer);
    }

    /// <summary>
    /// 
    /// </summary>
    public IDataFormatter DataFormatter { get; }

    /// <inheritdoc/>
    public Task<RestResponse> SendAsync(RestRequest request)
    {
        var tsc = new TranslatorTaskCompletionSource<HttpRestResponse, RestResponse>(serviceRequest =>
        {
            var resp = serviceRequest.Translate();
            this.afterTranslateAction(resp);

            return resp;
        });

        this.SendRequest(request, tsc);

        return tsc.Task;
    }

    /// <inheritdoc/>
    public Task<RestResponse<TData>> SendAsync<TData>(RestRequest request)
    {
        var tsc = new TranslatorTaskCompletionSource<HttpRestResponse, RestResponse<TData>>(serviceRequest =>
        {
            var resp = serviceRequest.Translate<TData>(this.DataFormatter);
            this.afterTranslateAction(resp);

            return resp;
        });

        this.SendRequest(request, tsc);

        return tsc.Task;
    }

    /// <summary>
    /// Sends the given request using the underlying channel communication.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="taskCompletionSource"></param>
    private void SendRequest(RestRequest request, ITranslatorTaskCompletionSource taskCompletionSource)
    {
        request.AddOrOverrideHeader(HttpHeaders.ContentType, this.descriptor.ContextType);

        var timeout = request.Timeout ?? this.descriptor.Timeout ?? TimeSpan.FromSeconds(15);

        var properties = this.Channel.CreateBasicProperties();
        properties.CorrelationId = request.Id;
        properties.Timestamp = new AmqpTimestamp(DateTime.UtcNow.AsUnixDateTime());
        properties.Expiration = timeout.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
        properties.ReplyTo = this.replyQueueName;

        this.callBackResponses.TryAdd(request.Id, taskCompletionSource);

        var httpRequest = request.Translate(this.DataFormatter);
        var payload = this.messageSerializer.SerializeRequest(httpRequest);

        var cts = new CancellationTokenSource(timeout);

        this.Channel.PublishMessage(this.exchangeDescriptor, string.Empty, properties, payload);

        cts.Token.Register(() =>
        {
            if (!this.callBackResponses.TryRemove(request.Id, out var tcs))
            {
                return;
            }

            tcs.SetException(new TimeoutException($"The request with the given id ({request.Id}) wasn't completed successfully due to timeout problem."));
        });
    }
        
    ///<inheritdoc/>
    protected override void Dispose(bool disposing)
    {
        if (!disposing)
        {
            return;
        }

        this.consumer.Received -= this.OnReceived;

        if (this.consumer.IsRunning)
        {
            this.Channel.Cancel(this.consumer);
        }

        this.consumer = null;

        base.Dispose(true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="ea"></param>
    private void OnReceived(object sender, BasicDeliverEventArgs ea)
    {
        if (!this.callBackResponses.TryRemove(ea.BasicProperties.CorrelationId, out var tcs))
        {
            return;
        }

        var response = this.messageSerializer.DeserializeResponse(ea.Body);
        tcs.TrySetResult(response);
    }
}
