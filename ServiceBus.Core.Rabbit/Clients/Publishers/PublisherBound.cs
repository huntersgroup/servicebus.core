using ServiceBus.Core.Formatters;
using RabbitMQ.Client;
using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Extensions;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using System.Globalization;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// Represents a common way to forward messages into one or more exchanges.
/// <para>
/// This publisher can forward messages into <see cref="PublisherBoundDescriptor.Exchanges"/>, at the same time, each <see cref="ExchangeBoundDescriptor"/> instance could
/// bind with other exchanges and queues, when It's bound with a <see cref="ExchangeBoundDescriptor"/> instance, this operation could be recursive, composing a large routing strategy.
/// </para>
/// </summary>
public class PublisherBound : BoundClient, IAsyncPublisher, IDisposable, IAsyncDisposable
{
    private readonly PublisherBoundDescriptor descriptor;
    private readonly IDataEncoder encoder;
    private readonly object lockObj = new();
    private bool disposed;
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="encoder"></param>
    /// <param name="channel"></param>
    /// <exception cref="SetupBrokerClientException"></exception>
    public PublisherBound(PublisherBoundDescriptor descriptor, IDataEncoder encoder, IModel channel)
        : base(descriptor, channel)
    {
        descriptor.ThrowIfNull(nameof(descriptor));
        encoder.ThrowIfNull(nameof(encoder));
            
        this.descriptor = descriptor;
        this.encoder = encoder;

        this.descriptor.VerifyStatus();

        try
        {
            if (this.descriptor.Confirmation.Enabled)
            {
                this.Channel.ConfirmSelect();

                this.Channel.BasicReturn += this.OnChannelBasicReturn;
                this.Channel.BasicAcks += this.OnChannelBasicAcks;
                this.Channel.BasicNacks += this.OnChannelBasicNacks;
            }

            foreach (var exchange in descriptor.Exchanges)
            {
                this.Channel.BuildExchangeBound(exchange);
            }
        }
        catch (Exception e)
        {
            throw new SetupBrokerClientException("An exception was thrown on setup publisher, see inner exception for details", e)
            {
                ClientType = typeof(PublisherBound)
            };
        }
    }

    ///<inheritdoc/>
    public void Send(object data)
    {
        this.SendAsync(data).GetAwaiter().GetResult();
    }

    ///<inheritdoc/>
    public void Send<TData>(IMessage<TData> message)
    {
        this.SendAsync(message).GetAwaiter().GetResult();
    }

    ///<inheritdoc/>
    public async Task SendAsync(object data)
    {
        if (data is IMessage<dynamic> msg)
        {
            await this.SendAsync(msg);
        }
        else
        {
            var message = new Message<object>
            {
                Created = DateTime.UtcNow,
                Data = data
            };

            await this.SendAsync(message);
        }
    }

    ///<inheritdoc/>
    public async Task SendAsync<TData>(IMessage<TData> message)
    {
        this.Evaluate(message);

        var exchanges = this.descriptor.Exchanges;
        var body = this.BuildMessage(message);

        var tasks = new List<Task>();

        lock (this.lockObj)
        {
            var properties = this.CreateProperties(message);

            foreach (var exchange in exchanges)
            {
                var tsk = this.Channel.PublishMessageAsync(exchange, message.Route ?? string.Empty, properties, body);
                tasks.Add(tsk.AsTask());
            }
        }

        await Task.WhenAll(tasks);
    }

    ///<inheritdoc/>
    void IDisposable.Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc/>
    public ValueTask DisposeAsync()
    {
        if (!this.disposed && this.descriptor.Confirmation.Enabled && this.descriptor.Confirmation.WaitFor)
        {
            var timeout = this.descriptor.Confirmation.Timeout ?? TimeSpan.FromMinutes(1);

            if (this.Channel.IsOpen)
            {
                this.Channel.WaitForConfirms(timeout);
            }

            this.disposed = true;
        }

        return new ValueTask(Task.CompletedTask);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="message"></param>
    private void Evaluate<TData>(IMessage<TData> message)
    {
        if (message == null)
        {
            throw new ArgumentNullException(nameof(message), "The given message cannot be null.");
        }

        message.Created ??= DateTime.UtcNow;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private IBasicProperties CreateProperties(IMessage message)
    {
        var ttl = message.Ttl ?? this.descriptor.Message.Ttl ?? TimeSpan.FromMinutes(15);

        var properties = this.Channel.CreateBasicProperties();

        if (!string.IsNullOrWhiteSpace(message.Id))
        {
            properties.MessageId = message.Id;
        }

        message.Tag = this.Channel.NextPublishSeqNo.ToString();
        properties.Timestamp = new AmqpTimestamp((message.Created ?? DateTime.UtcNow).AsUnixDateTime());
        properties.Expiration = ttl.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
        properties.Persistent = message.Persistent ?? this.descriptor.Message.Persistent;

        this.descriptor.OnBuildProperties(properties);

        return properties;
    }

    private byte[] BuildMessage<TData>(IMessage<TData> message)
    {
        return this.descriptor.Message.Format switch
        {
            MessageFormat.Header => this.encoder.Encode(message),
            MessageFormat.Headless => this.encoder.Encode(message.Data),
            _ => throw new InvalidDescriptorException($"The current message format indicated in the current publisher message descriptor is not supported, value: {this.descriptor.Message.Format}")
        };
    }

    private void OnChannelBasicReturn(object sender, BasicReturnEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Return,
            Properties =
            {
                {nameof(e.ReplyText), e.ReplyText},
                {nameof(e.ReplyCode), e.ReplyCode},
                {nameof(e.BasicProperties), e.BasicProperties},
                {nameof(e.Exchange), e.Exchange},
                {nameof(e.RoutingKey), e.RoutingKey}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }

    private void OnChannelBasicAcks(object sender, BasicAckEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Acks,
            Properties =
            {
                {nameof(e.DeliveryTag), e.DeliveryTag},
                {nameof(e.Multiple), e.Multiple}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }

    private void OnChannelBasicNacks(object sender, BasicNackEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.Nacks,
            Properties =
            {
                {nameof(e.DeliveryTag), e.DeliveryTag},
                {nameof(e.Multiple), e.Multiple},
                {nameof(e.Requeue), e.Requeue}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }
}
