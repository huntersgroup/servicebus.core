using RabbitMQ.Client;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
[Obsolete("Not used anymore, It will be removed on next releases.", true)]
public class ChannelHandler : IDisposable
{
    private readonly Lazy<IConnection> connection;
    private readonly Lazy<IModel> channel;
    private string appTarget;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionFactory"></param>
    /// <param name="providerName"></param>
    public ChannelHandler(IConnectionFactory connectionFactory, string providerName)
    {
        this.appTarget = providerName;
        this.connection = new Lazy<IConnection>(() => connectionFactory.BuildConnection(factory => factory.CreateConnection(this.appTarget)));
        this.channel = new Lazy<IModel>(() => this.connection.Value.CreateChannel());
    }

    /// <summary>
    /// 
    /// </summary>
    public IModel Channel { get { return this.channel.Value; } }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="providerName"></param>
    /// <returns></returns>
    public bool UpdateTarget(string providerName)
    {
        if (this.connection.IsValueCreated && string.IsNullOrWhiteSpace(providerName))
        {
            return false;
        }

        this.appTarget = providerName;

        return true;
    }

    ///<inheritdoc />
    public void Dispose()
    {
        if (this.channel.IsValueCreated)
        {
            this.channel.Value.Dispose();
        }

        if (this.connection.IsValueCreated)
        {
            this.connection.Value.Dispose();
        }
    }
}
