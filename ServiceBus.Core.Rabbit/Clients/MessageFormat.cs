namespace ServiceBus.Core.Clients;

/// <summary>
/// Represents the way messages are sent into message broker.
/// </summary>
public enum MessageFormat
{
    /// <summary>
    /// A standard way to send messages from publishers, including extra information related to the holden messages
    /// </summary>
    Header = 1,

    /// <summary>
    /// A light / simple way to send messages without extra information on related messages
    /// </summary>
    Headless = 2
}
