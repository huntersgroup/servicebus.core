using RabbitMQ.Client;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public class ChannelFactory(IConnectionFactory connectionFactory)
    : IChannelFactory
{
    private readonly List<ChannelReference> channelRefs = [];
    private bool disposed;

    ///<inheritdoc />
    public IModel Build(string target)
    {
        var connection = connectionFactory.CreateConnection(target);
        var instance = new ChannelReference(target, connection, connection.CreateChannel());

        this.channelRefs.Add(instance);

        return instance.Channel;
    }

    ///<inheritdoc />
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc />
    public async ValueTask DisposeAsync()
    {
        if (this.disposed)
        {
            return;
        }

        foreach (var channelRef in this.channelRefs)
        {
            
            try
            {
                channelRef.Channel.Dispose();
                channelRef.Connection.Dispose();
            }
            catch
            {
                // nothing to do !
            }
        }

        this.disposed = true;

        await Task.CompletedTask;
    }
}

/// <summary>
/// 
/// </summary>
/// <param name="Target"></param>
/// <param name="Connection"></param>
/// <param name="Channel"></param>
internal record ChannelReference(string Target, IConnection Connection, IModel Channel);
