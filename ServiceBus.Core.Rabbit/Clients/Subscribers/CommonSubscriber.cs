using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

 namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents a basic subscriber.
/// </summary>
/// <typeparam name="TData"></typeparam>
[Obsolete("Use SubscriberBound<TData> instead, it will be removed in the next major version.")]
public class CommonSubscriber<TData> : BrokerClient, ISubscriber<TData>
 {
     private readonly SubscriberDescriptor descriptor;
     private EventingBasicConsumer consumer;
     private event EventHandler<MessageDeliverEventArgs<TData>> OnReceivedMessage;
     private readonly Func<BasicDeliverEventArgs, Message<TData>> messageDecoderFunc;
        
     /// <summary>
     /// Creates a new <see cref="CommonSubscriber{TData}"/> instance.
     /// </summary>
     /// <param name="descriptor"></param>
     /// <param name="decoder"></param>
     public CommonSubscriber(SubscriberDescriptor descriptor, IDataDecoder decoder)
         : base(descriptor.ExecuteCustomAction(() => decoder.ThrowIfNull(nameof(decoder))))
     {
         this.descriptor = descriptor;
            
         this.SetUp();

         this.messageDecoderFunc = descriptor.MessageFormat switch
         {
             MessageFormat.Header => ea => decoder.Decode<Message<TData>>(ea.Body),
             MessageFormat.Headless => ea =>
             {
                 var body = decoder.Decode<TData>(ea.Body);

                 var properties = ea.BasicProperties;

                 return new Message<TData>
                 {
                     Route = ea.RoutingKey,
                     Persistent = properties?.Persistent,
                     Created = properties?.Timestamp.UnixTime.FromUnixDateTime(),
                     Data = body
                 };
             },
             _ => throw new InvalidDescriptorException(
                 $"The current message format indicated in the current subscriber descriptor is not supported, value: {descriptor.MessageFormat}")
         };
     }

     ///<inheritdoc/>
     public event EventHandler<MessageDeliverEventArgs<TData>> Received
     {
         add
         {
             if (value == null) return;
                
             this.OnReceivedMessage += value;

             if (this.OnReceivedMessage != null && !this.consumer.IsRunning)
             {
                 this.Channel.BindConsumer(this.descriptor.Queue, this.consumer, this.descriptor.Arguments);
             }
         }
         remove
         {
             if (value == null) return;

             this.OnReceivedMessage -= value;
                
             if (this.OnReceivedMessage == null && this.consumer.IsRunning)
             {
                 this.Channel.Cancel(this.consumer);
             }
         }
     }

     ///<inheritdoc/>
     public event EventHandler<MessageExceptionEventArgs> OnException;

     ///<inheritdoc/>
     public event EventHandler<RetryExceptionEventArgs<TData>> OnMaxRetryFailed;

     ///<inheritdoc/>
     protected override void Dispose(bool disposing)
     {
         if (!disposing) return;

         this.consumer.Received -= this.OnReceived;
                
         if (this.consumer.IsRunning)
         {
             this.Channel.Cancel(this.consumer);
         }
            
         base.Dispose(true);
     }

     private void SetUp()
     {
         try
         {
             this.Channel.BasicQos(0, this.descriptor.PrefetchCount, false);

             this.Channel
                 .BuildQueue(this.descriptor.Queue);

             var dlxExchange = this.Channel.BuildDlxExchange(this.descriptor.RetryPolicy, this.descriptor.Queue);

             this.consumer = new EventingBasicConsumer(this.Channel);

             if (dlxExchange == null)
             {
                 this.consumer.Received += (sender, args) =>
                 {
                     try
                     {
                         this.OnReceived(sender, args);
                     }
                     catch (Exception ex)
                     {
                         this.CatchException(sender, ex, args);
                     }
                 };
             }
             else
             {
                 this.consumer.Received += (sender, args) =>
                 {
                     try
                     {
                         this.OnReceived(sender, args, dlxExchange);
                     }
                     catch (Exception ex)
                     {
                         this.CatchException(sender, ex, args);
                     }
                 };
             }
         }
         catch (Exception e)
         {
             throw new SetupBrokerClientException("An exception was thrown on setup subscriber, see inner exception for details", e)
             {
                 ClientType = typeof(CommonSubscriber<TData>)
             };
         }
     }

     /// <summary>
     /// Try to manage the reception of messages without retry policies
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="ea"></param>
     private void OnReceived(object sender, BasicDeliverEventArgs ea)
     {
         var eventArg = this.GetMessageDeliverEventArgs(ea);

         this.OnReceivedMessage?.Invoke(sender, eventArg);
            
         if (eventArg.Acknowledged)
         {
             this.Channel.BasicAck(ea.DeliveryTag, false);
         }
         else
         {
             this.Channel.BasicNack(ea.DeliveryTag, false, eventArg.Requeue);
         }
     }

     /// <summary>
     /// Try to manage the reception of messages with retry policies
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="ea"></param>
     /// <param name="dlxExchange"></param>
     private void OnReceived(object sender, BasicDeliverEventArgs ea, ExchangeDescriptor dlxExchange)
     {
         var eventArg = this.GetMessageDeliverEventArgs(ea);

         this.OnReceivedMessage?.Invoke(sender, eventArg);

         if (eventArg.Acknowledged)
         {
             this.Channel.BasicAck(ea.DeliveryTag, false);
         }
         else
         {
             this.Channel.BasicNack(ea.DeliveryTag, false, false);
                
             if (eventArg.Requeue)
             {
                 var retryCounter = ea.BasicProperties.IncrementRetryCounter();

                 if (retryCounter < this.descriptor.RetryPolicy.MaxRetry)
                 {
                     this.Channel.PublishMessage(dlxExchange, string.Empty, ea.BasicProperties, ea.Body);
                 }
                 else
                 {
                     this.OnMaxRetryFailed?.Invoke(sender, new RetryExceptionEventArgs<TData>
                     {
                         Message = eventArg.Message,
                         RetryCounter = retryCounter,
                         Exception = new Exception("An exception was occurred due to reach the max retry counter for requeue message")
                     });
                 }
             }
         }
     }

     /// <summary>
     /// 
     /// </summary>
     /// <param name="ea"></param>
     /// <returns></returns>
     private MessageDeliverEventArgs<TData> GetMessageDeliverEventArgs(BasicDeliverEventArgs ea)
     {
         try
         {
             var message = this.messageDecoderFunc(ea);

             return new MessageDeliverEventArgs<TData>(ea.Body)
             {
                 Message = message
             };
         }
         catch (Exception ex)
         {
             throw new InvalidOperationException("Something was occurred when the incoming message was deserialized (maybe is It an incompatible type ??), see inner exception for details", ex);
         }
     }

     private void CatchException(object sender, Exception ex, BasicDeliverEventArgs ea)
     {
         this.Channel.BasicReject(ea.DeliveryTag, false);

         this.OnException?.Invoke(sender, new MessageExceptionEventArgs
         {
             PayloadType = typeof(TData),
             Exception = ex
         });
     }
 }
