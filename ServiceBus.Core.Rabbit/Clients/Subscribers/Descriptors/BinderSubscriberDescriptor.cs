using ServiceBus.Core.Descriptors;

namespace ServiceBus.Core.Clients.Subscribers.Descriptors;

/// <summary>
/// Represents a binder descriptors for <see cref="BinderSubscriber{TData}"/> instances.
/// </summary>
[Obsolete("Descriptor used by legacy client, so it will be removed in the next major version.")]
public class BinderSubscriberDescriptor : SubscriberDescriptor, IExchangeBinderDescriptor
{
    /// <summary>
    /// Gets or sets the binder descriptor.
    /// </summary>
    public BinderDescriptor Binder { get; set; } = new();

    /// <summary>
    /// Gets or sets the exchange descriptor.
    /// </summary>
    public ExchangeDescriptor Exchange { get; set; } = new();
}
