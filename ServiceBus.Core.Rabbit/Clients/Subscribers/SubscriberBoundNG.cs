using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents a common subscriber which receives messages from specific queue.
/// </summary>
public class SubscriberBound : BoundClient, IRunnableSubscriber, IDisposable, IAsyncDisposable
{
    private readonly IDataDecoder decoder;
    private readonly SubscriberBoundDescriptor descriptor;
    private readonly AsyncEventingBasicConsumer consumer;
    private readonly AsyncEventHandler<BasicDeliverEventArgs> onMsgReceiveHandler;

    private readonly object lockObj = new();
    private bool disposed;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="decoder"></param>
    /// <param name="channel"></param>
    public SubscriberBound(SubscriberBoundDescriptor descriptor, IDataDecoder decoder, IModel channel)
        : base(descriptor, channel)
    {
        descriptor.ThrowIfNull(nameof(descriptor));
        decoder.ThrowIfNull(nameof(decoder));

        this.decoder = decoder;

        this.descriptor = descriptor;

        this.descriptor.VerifyStatus();

        // setup
        this.Channel.BasicQos(0, this.descriptor.PrefetchCount, false);
        this.Channel.BuildQueue(this.descriptor.Queue);

        // an explicit binding will be applied.
        if (descriptor.Exchange != null)
        {
            this.Channel.BuildExchange(descriptor.Exchange)
                .BuildBinder(descriptor.Exchange, descriptor.Binder, descriptor.Queue);
        }

        this.consumer = new AsyncEventingBasicConsumer(this.Channel);

        // retry logic
        var dlxExchange = this.Channel.BuildDlxExchange(this.descriptor.RetryPolicy, this.descriptor.Queue);

        this.onMsgReceiveHandler = (sender, args) => this.OnMsgReceiveHandler(sender, args, dlxExchange);

        this.consumer.Received += this.onMsgReceiveHandler;

        this.Received = new DelegateHandler<IMessageDecoderEventArgs>();
        this.OnException = new DelegateHandler<MessageExceptionEventArgs>();
        this.OnMaxRetryFailed = new DelegateHandler<RetryExceptionEventArgs<object>>();
    }

    ///<inheritdoc/>
    public bool IsRunning { get; private set; }

    ///<inheritdoc/>
    public IDelegateHandler<IMessageDecoderEventArgs> Received { get; protected set; }

    ///<inheritdoc/>
    public IDelegateHandler<MessageExceptionEventArgs> OnException { get; }

    ///<inheritdoc/>
    public IDelegateHandler<RetryExceptionEventArgs<object>> OnMaxRetryFailed { get; }

    ///<inheritdoc/>
    public virtual async Task Start()
    {
        if (this.IsRunning)
        {
            return;
        }

        var lockTaken = false;

        try
        {
            if (Monitor.TryEnter(this.lockObj, TimeSpan.FromSeconds(3)))
            {
                lockTaken = true;

                if (this.disposed)
                {
                    throw new ObjectDisposedException("The current instance was disposed");
                }

                if (this.IsRunning || this.Received.Count() == 0)
                {
                    return;
                }

                this.Channel.BindConsumer(this.descriptor.Queue, this.consumer, this.descriptor.Arguments);
                this.IsRunning = true;
            }
        }
        finally
        {
            // Ensure that the lock is released.
            if (lockTaken)
            {
                Monitor.Exit(this.lockObj);
            }
        }

        await Task.Yield();
    }

    ///<inheritdoc/>
    public virtual async Task Stop()
    {
        var lockTaken = false;

        try
        {
            if (Monitor.TryEnter(this.lockObj, TimeSpan.FromSeconds(3)))
            {
                lockTaken = true;

                if (!this.IsRunning)
                {
                    return;
                }

                this.Channel.Cancel(this.consumer);

                this.IsRunning = false;
            }
        }
        finally
        {
            // Ensure that the lock is released.
            if (lockTaken)
            {
                Monitor.Exit(this.lockObj);
            }
        }

        await Task.Yield();
    }

    ///<inheritdoc/>
    void IDisposable.Dispose()
    {
        this.Dispose(true);
    }

    ///<inheritdoc/>
    async ValueTask IAsyncDisposable.DisposeAsync()
    {
        this.Dispose(true);

        await Task.Yield();
    }

    /// <summary>
    /// Disposes the current instance if disposing parameter is true.
    /// </summary>
    /// <param name="disposing"></param>
    protected virtual void Dispose(bool disposing)
    {
        if (this.disposed)
        {
            return;
        }

        if (!disposing)
        {
            return;
        }

        this.Stop().GetAwaiter().GetResult();

        this.disposed = true;
        this.consumer.Received -= this.onMsgReceiveHandler;
    }

    /// <summary>
    /// Try to manage the reception of messages without retry policies
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="ea"></param>
    private async Task OnReceived(object sender, BasicDeliverEventArgs ea)
    {
        var eventArg = this.GetMessageDeliverEventArgs(ea);

        if (!this.IsRunning)
        {
            return;
        }

        await this.Received.InvokeAsync(sender, eventArg);

        if (!this.IsRunning)
        {
            return;
        }

        if (eventArg.Acknowledged)
        {
            this.Channel.BasicAck(ea.DeliveryTag, false);
        }
        else
        {
            this.Channel.BasicNack(ea.DeliveryTag, false, eventArg.Requeue);
        }
    }

    /// <summary>
    /// Try to manage the reception of messages with retry policies
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="ea"></param>
    /// <param name="dlxExchange"></param>
    private async Task OnReceived(object sender, BasicDeliverEventArgs ea, ExchangeDescriptor dlxExchange)
    {
        var eventArg = this.GetMessageDeliverEventArgs(ea);

        if (!this.IsRunning)
        {
            return;
        }

        await this.Received.InvokeAsync(sender, eventArg);

        if (!this.IsRunning)
        {
            return;
        }

        if (eventArg.Acknowledged)
        {
            this.Channel.BasicAck(ea.DeliveryTag, false);
        }
        else
        {
            this.Channel.BasicNack(ea.DeliveryTag, false, false);

            if (eventArg.Requeue)
            {
                var retryCounter = ea.BasicProperties.IncrementRetryCounter();

                if (retryCounter < this.descriptor.RetryPolicy.MaxRetry)
                {
                    this.Channel.PublishMessage(dlxExchange, string.Empty, ea.BasicProperties, ea.Body);
                }
                else
                {
                    await this.OnMaxRetryFailed.InvokeAsync(sender, new RetryExceptionEventArgs<object>
                    {
                        Message = eventArg.Message,
                        RetryCounter = retryCounter,
                        Exception = new Exception("An exception was occurred due to reach the max retry counter for requeue message")
                    });
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ea"></param>
    /// <returns></returns>
    private IMessageDecoderEventArgs GetMessageDeliverEventArgs(BasicDeliverEventArgs ea)
    {
        try
        {
            return new MessageDecoderEventArgs(ea, this.decoder, this.descriptor.MessageFormat);
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException("Something was occurred when the incoming message was deserialized (maybe is It an incompatible type ??), see inner exception for details", ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="ex"></param>
    /// <param name="ea"></param>
    private async Task CatchException(object sender, Exception ex, BasicDeliverEventArgs ea)
    {
        this.Channel.BasicReject(ea.DeliveryTag, false);

        await this.OnException.InvokeAsync(sender, new MessageExceptionEventArgs
        {
            PayloadType = typeof(object),
            Exception = ex
        });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    /// <param name="dlxExchange"></param>
    /// <returns></returns>
    private async Task OnMsgReceiveHandler(object sender, BasicDeliverEventArgs args, ExchangeDescriptor dlxExchange = null)
    {
        try
        {
            if (dlxExchange is null)
            {
                await this.OnReceived(sender, args);
            }
            else
            {
                await this.OnReceived(sender, args, dlxExchange);
            }
        }
        catch (Exception ex)
        {
            await this.CatchException(sender, ex, args);
        }
    }
}
