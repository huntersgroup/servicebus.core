using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;

namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents a subscriber using a binding logic with exchange.
/// </summary>
/// <typeparam name="TData"></typeparam>
[Obsolete("Use SubscriberBound<TData> instead, it will be removed in the next major version.")]
public class BinderSubscriber<TData> : CommonSubscriber<TData>
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="decoder"></param>
    public BinderSubscriber(BinderSubscriberDescriptor descriptor, IDataDecoder decoder)
        : base(descriptor, decoder)
    {
        try
        {
            this.Channel
                .BuildExchange(descriptor.Exchange)
                .BuildBinder(descriptor);
        }
        catch (Exception e)
        {
            throw new InvalidDescriptorException("An exception was thrown on setup subscriber, see inner exception for details", e)
            {
                ClientType = typeof(BinderSubscriber<TData>)
            };
        }
    }
}
