using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public class BoundClient : IAsyncBrokerClient
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="channel"></param>
    /// <exception cref="SetupBrokerClientException"></exception>
    public BoundClient(IBrokerDescriptor descriptor, IModel channel)
    {
        descriptor.ThrowIfNull(nameof(descriptor));
        channel.ThrowIfNull(nameof(channel));

        if (channel.IsClosed)
        {
            throw new SetupBrokerClientException("The current channel cannot be used because it's closed.");
        }

        this.Channel = channel;
        this.OnChannelEventFired = new DelegateHandler<ChannelEventArgs>();

        if (!descriptor.EnableChannelEvents)
        {
            return;
        }

        this.Channel.FlowControl += this.OnChannelFlowControl;
        this.Channel.CallbackException += this.OnChannelCallbackException;
        this.Channel.ModelShutdown += this.OnChannelModelShutdown;
    }

    ///<inheritdoc />
    public IDelegateHandler<ChannelEventArgs> OnChannelEventFired { get; protected set; }

    /// <summary>
    /// Gets the underlying rabbit channel.
    /// </summary>
    protected IModel Channel { get; }

    private void OnChannelFlowControl(object sender, FlowControlEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.FlowControl,
            Properties =
            {
                {nameof(e.Active), e.Active}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }

    private void OnChannelCallbackException(object sender, CallbackExceptionEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.CallbackException,
            Properties =
            {
                {nameof(e.Detail), e.Detail},
                {nameof(e.Exception), e.Exception}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }

    private void OnChannelModelShutdown(object sender, ShutdownEventArgs e)
    {
        if (this.OnChannelEventFired.Count() == 0)
        {
            return;
        }

        var args = new ChannelEventArgs
        {
            EventTag = ChannelEvents.ModelShutdown,
            Properties =
            {
                {nameof(e.ClassId), e.ClassId},
                {nameof(e.MethodId), e.MethodId},
                {nameof(e.ReplyCode), e.ReplyCode},
                {nameof(e.ReplyText), e.ReplyText}
            }
        };

        this.OnChannelEventFired.InvokeAsync(sender, args).GetAwaiter().GetResult();
    }
}
