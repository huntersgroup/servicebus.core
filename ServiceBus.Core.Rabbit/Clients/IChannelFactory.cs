using RabbitMQ.Client;

namespace ServiceBus.Core.Clients;

/// <summary>
/// 
/// </summary>
public interface IChannelFactory : IAsyncDisposable, IDisposable
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    IModel Build(string target);
}
