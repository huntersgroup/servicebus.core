using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ServiceBus.Core.Clients;

internal class ModelDecorator : IModel
{
    private readonly IModel decoratee;

    public ModelDecorator(IModel decoratee)
    {
        this.decoratee = decoratee;
    }

    void IDisposable.Dispose()
    {
        IModel current = this;
        current.Close();

        this.decoratee.Dispose();
    }

    void IModel.Abort()
    {
        this.decoratee.Abort();
    }

    void IModel.Abort(ushort replyCode, string replyText)
    {
        this.decoratee.Abort(replyCode, replyText);
    }

    void IModel.BasicAck(ulong deliveryTag, bool multiple)
    {
        this.decoratee.BasicAck(deliveryTag, multiple);
    }

    void IModel.BasicCancel(string consumerTag)
    {
        this.decoratee.BasicCancel(consumerTag);
    }

    void IModel.BasicCancelNoWait(string consumerTag)
    {
        this.decoratee.BasicCancelNoWait(consumerTag);
    }

    string IModel.BasicConsume(string queue, bool autoAck, string consumerTag, bool noLocal, bool exclusive, IDictionary<string, object> arguments,
        IBasicConsumer consumer)
    {
        return this.decoratee.BasicConsume(queue, autoAck, consumerTag, noLocal, exclusive, arguments, consumer);
    }

    BasicGetResult IModel.BasicGet(string queue, bool autoAck)
    {
        return this.decoratee.BasicGet(queue, autoAck);
    }

    void IModel.BasicNack(ulong deliveryTag, bool multiple, bool requeue)
    {
        this.decoratee.BasicNack(deliveryTag, multiple, requeue);
    }

    void IModel.BasicPublish(string exchange, string routingKey, bool mandatory, IBasicProperties basicProperties,
        ReadOnlyMemory<byte> body)
    {
        this.decoratee.BasicPublish(exchange, routingKey, mandatory, basicProperties, body);
    }

    void IModel.BasicQos(uint prefetchSize, ushort prefetchCount, bool global)
    {
        this.decoratee.BasicQos(prefetchSize, prefetchCount, global);
    }

    void IModel.BasicRecover(bool requeue)
    {
        this.decoratee.BasicRecover(requeue);
    }

    void IModel.BasicRecoverAsync(bool requeue)
    {
        this.decoratee.BasicRecoverAsync(requeue);
    }

    void IModel.BasicReject(ulong deliveryTag, bool requeue)
    {
        this.decoratee.BasicReject(deliveryTag, requeue);
    }

    void IModel.Close()
    {
        if (this.decoratee.IsOpen)
        {
            this.decoratee.Close();
        }
    }

    void IModel.Close(ushort replyCode, string replyText)
    {
        if (this.decoratee.IsOpen)
        {
            this.decoratee.Close(replyCode, replyText);
        }
    }

    void IModel.ConfirmSelect()
    {
        this.decoratee.ConfirmSelect();
    }

    IBasicPublishBatch IModel.CreateBasicPublishBatch()
    {
        return this.decoratee.CreateBasicPublishBatch();
    }

    IBasicProperties IModel.CreateBasicProperties()
    {
        return this.decoratee.CreateBasicProperties();
    }

    void IModel.ExchangeBind(string destination, string source, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.ExchangeBind(destination, source, routingKey, arguments);
    }

    void IModel.ExchangeBindNoWait(string destination, string source, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.ExchangeBindNoWait(destination, source, routingKey, arguments);
    }

    void IModel.ExchangeDeclare(string exchange, string type, bool durable, bool autoDelete, IDictionary<string, object> arguments)
    {
        this.decoratee.ExchangeDeclare(exchange, type, durable, autoDelete, arguments);
    }

    void IModel.ExchangeDeclareNoWait(string exchange, string type, bool durable, bool autoDelete, IDictionary<string, object> arguments)
    {
        this.decoratee.ExchangeDeclareNoWait(exchange, type, durable, autoDelete, arguments);
    }

    void IModel.ExchangeDeclarePassive(string exchange)
    {
        this.decoratee.ExchangeDeclarePassive(exchange);
    }

    void IModel.ExchangeDelete(string exchange, bool ifUnused)
    {
        this.decoratee.ExchangeDelete(exchange, ifUnused);
    }

    void IModel.ExchangeDeleteNoWait(string exchange, bool ifUnused)
    {
        this.decoratee.ExchangeDeleteNoWait(exchange, ifUnused);
    }

    void IModel.ExchangeUnbind(string destination, string source, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.ExchangeUnbind(destination, source, routingKey, arguments);
    }

    void IModel.ExchangeUnbindNoWait(string destination, string source, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.ExchangeUnbindNoWait(destination, source, routingKey, arguments);
    }

    void IModel.QueueBind(string queue, string exchange, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.QueueBind(queue, exchange, routingKey, arguments);
    }

    void IModel.QueueBindNoWait(string queue, string exchange, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.QueueBindNoWait(queue, exchange, routingKey, arguments);
    }

    QueueDeclareOk IModel.QueueDeclare(string queue, bool durable, bool exclusive, bool autoDelete, IDictionary<string, object> arguments)
    {
        return this.decoratee.QueueDeclare(queue, durable, exclusive, autoDelete, arguments);
    }

    void IModel.QueueDeclareNoWait(string queue, bool durable, bool exclusive, bool autoDelete, IDictionary<string, object> arguments)
    {
        this.decoratee.QueueDeclareNoWait(queue, durable, exclusive, autoDelete, arguments);
    }

    QueueDeclareOk IModel.QueueDeclarePassive(string queue)
    {
        return this.decoratee.QueueDeclarePassive(queue);
    }

    uint IModel.MessageCount(string queue)
    {
        return this.decoratee.MessageCount(queue);
    }

    uint IModel.ConsumerCount(string queue)
    {
        return this.decoratee.ConsumerCount(queue);
    }

    uint IModel.QueueDelete(string queue, bool ifUnused, bool ifEmpty)
    {
        return this.decoratee.QueueDelete(queue, ifUnused, ifEmpty);
    }

    void IModel.QueueDeleteNoWait(string queue, bool ifUnused, bool ifEmpty)
    {
        this.decoratee.QueueDeleteNoWait(queue, ifUnused, ifEmpty);
    }

    uint IModel.QueuePurge(string queue)
    {
        return this.decoratee.QueuePurge(queue);
    }

    void IModel.QueueUnbind(string queue, string exchange, string routingKey, IDictionary<string, object> arguments)
    {
        this.decoratee.QueueUnbind(queue, exchange, routingKey, arguments);
    }

    void IModel.TxCommit()
    {
        this.decoratee.TxCommit();
    }

    void IModel.TxRollback()
    {
        this.decoratee.TxRollback();
    }

    void IModel.TxSelect()
    {
        this.decoratee.TxSelect();
    }

    bool IModel.WaitForConfirms()
    {
        return this.decoratee.WaitForConfirms();
    }

    bool IModel.WaitForConfirms(TimeSpan timeout)
    {
        return this.decoratee.WaitForConfirms(timeout);
    }

    bool IModel.WaitForConfirms(TimeSpan timeout, out bool timedOut)
    {
        return this.decoratee.WaitForConfirms(timeout, out timedOut);
    }

    void IModel.WaitForConfirmsOrDie()
    {
        this.decoratee.WaitForConfirmsOrDie();
    }

    void IModel.WaitForConfirmsOrDie(TimeSpan timeout)
    {
        this.decoratee.WaitForConfirmsOrDie(timeout);
    }

    int IModel.ChannelNumber => this.decoratee.ChannelNumber;

    ShutdownEventArgs IModel.CloseReason => this.decoratee.CloseReason;

    IBasicConsumer IModel.DefaultConsumer
    {
        get => this.decoratee.DefaultConsumer;
        set => this.decoratee.DefaultConsumer = value;
    }

    bool IModel.IsClosed => this.decoratee.IsClosed;

    bool IModel.IsOpen => this.decoratee.IsOpen;

    ulong IModel.NextPublishSeqNo => this.decoratee.NextPublishSeqNo;

    string IModel.CurrentQueue
    {
        get => this.decoratee.CurrentQueue;
    }

    TimeSpan IModel.ContinuationTimeout
    {
        get => this.decoratee.ContinuationTimeout;
        set => this.decoratee.ContinuationTimeout = value;
    }

    event EventHandler<BasicAckEventArgs> IModel.BasicAcks
    {
        add => this.decoratee.BasicAcks += value;
        remove => this.decoratee.BasicAcks -= value;
    }

    event EventHandler<BasicNackEventArgs> IModel.BasicNacks
    {
        add => this.decoratee.BasicNacks += value;
        remove => this.decoratee.BasicNacks -= value;
    }

    event EventHandler<EventArgs> IModel.BasicRecoverOk
    {
        add => this.decoratee.BasicRecoverOk += value;
        remove => this.decoratee.BasicRecoverOk -= value;
    }

    event EventHandler<BasicReturnEventArgs> IModel.BasicReturn
    {
        add => this.decoratee.BasicReturn += value;
        remove => this.decoratee.BasicReturn -= value;
    }

    event EventHandler<CallbackExceptionEventArgs> IModel.CallbackException
    {
        add => this.decoratee.CallbackException += value;
        remove => this.decoratee.CallbackException -= value;
    }

    event EventHandler<FlowControlEventArgs> IModel.FlowControl
    {
        add => this.decoratee.FlowControl += value;
        remove => this.decoratee.FlowControl -= value;
    }

    event EventHandler<ShutdownEventArgs> IModel.ModelShutdown
    {
        add => this.decoratee.ModelShutdown += value;
        remove => this.decoratee.ModelShutdown -= value;
    }
}
