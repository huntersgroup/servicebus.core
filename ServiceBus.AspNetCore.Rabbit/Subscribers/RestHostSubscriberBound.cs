using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBus.AspNetCore.Extensions;
using ServiceBus.AspNetCore.Hosting;
using ServiceBus.AspNetCore.Subscribers.Descriptors;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Events;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.AspNetCore.Subscribers;

/// <summary>
/// 
/// </summary>
public class RestHostSubscriberBound
    : BoundClient, IRestHostSubscriber, IDisposable, IAsyncDisposable
{
    private readonly RestSubscriberBoundDescriptor descriptor;
    private readonly IHttpRestMessageSerializer restMessageSerializer;
    private readonly IMemoryStreamResolver streamResolver;
    private readonly IHttpContextFactory httpContextFactory;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly ILogger logger;
    private readonly string exchangeName;
    private readonly string queueName;

    // todo: tobe implemented on next features.
    //private readonly ExchangeDescriptor dlxExchange;
    private readonly object lockObj = new();
    private readonly AsyncEventingBasicConsumer consumer;
    private IHttpApplication<ContextScope> application;
    private bool disposed;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="descriptor"></param>
    /// <param name="restMessageSerializer"></param>
    /// <param name="streamResolver"></param>
    /// <param name="httpContextFactory"></param>
    /// <param name="serviceScopeFactory"></param>
    /// <param name="logger"></param>
    public RestHostSubscriberBound(RestSubscriberBoundDescriptor descriptor,
        IModel channel,
        IHttpRestMessageSerializer restMessageSerializer,
        IMemoryStreamResolver streamResolver,
        IHttpContextFactory httpContextFactory,
        IServiceScopeFactory serviceScopeFactory,
        ILogger<RestHostSubscriberBound> logger)
    : base(descriptor, channel)
    {
        this.descriptor = descriptor;
        this.restMessageSerializer = restMessageSerializer;
        this.streamResolver = streamResolver;
        this.httpContextFactory = httpContextFactory;
        this.serviceScopeFactory = serviceScopeFactory;
        this.logger = logger;

        var exchangeDescriptor = new ExchangeDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueDescriptor = new QueueDescriptor { Name = descriptor.ServiceName, Durable = descriptor.Durable, AutoDelete = false };
        var workingQueueBinderDescriptor = new BinderDescriptor();

        this.exchangeName = exchangeDescriptor.FullName;

        // prepares working queue, exchange and working binder for queue flow.
        channel.BuildExchange(exchangeDescriptor)
            .BuildQueue(workingQueueDescriptor)
            .BuildBinder(exchangeDescriptor, workingQueueBinderDescriptor, workingQueueDescriptor);

        this.consumer = new AsyncEventingBasicConsumer(channel);

        // todo: probably needs to implement it.
        // this.dlxExchange = null;

        this.consumer.Received += this.OnMsgReceiveHandler;

        this.queueName = workingQueueDescriptor.FullName;

        channel.BasicQos(0, descriptor.PrefetchCount, false);

        this.OnException = new DelegateHandler<MessageExceptionEventArgs>();
    }

    ///<inheritdoc/>
    public bool IsRunning { get; private set; }

    public IDelegateHandler<MessageExceptionEventArgs> OnException { get; }

    ///<inheritdoc/>
    public void Init(RequestDelegate middleware)
    {
        this.application = new RestHostApplication(middleware, this.httpContextFactory);
    }

    ///<inheritdoc/>
    public async Task Start()
    {
        if (this.IsRunning)
        {
            return;
        }

        if (this.application is null)
        {
            throw new InvalidOperationException($"The current hosted subscriber wasn't initialized correctly, please before start invoke {nameof(this.Init)} method.");
        }

        var lockTaken = false;

        try
        {
            if (Monitor.TryEnter(this.lockObj, TimeSpan.FromSeconds(3)))
            {
                lockTaken = true;

                if (this.disposed)
                {
                    throw new ObjectDisposedException("The current instance was disposed");
                }

                if (this.IsRunning)
                {
                    return;
                }

                this.Channel.BasicConsume(queue: this.queueName, autoAck: this.descriptor.AutoAck, consumer: this.consumer);
                this.IsRunning = true;
            }
        }
        finally
        {
            // Ensure that the lock is released.
            if (lockTaken)
            {
                Monitor.Exit(this.lockObj);
            }
        }

        await Task.Yield();
    }

    ///<inheritdoc/>
    public async Task Stop()
    {
        var lockTaken = false;

        try
        {
            if (Monitor.TryEnter(this.lockObj, TimeSpan.FromSeconds(3)))
            {
                lockTaken = true;

                if (!this.IsRunning)
                {
                    return;
                }

                this.Channel.Cancel(this.consumer);

                this.IsRunning = false;
            }
        }
        finally
        {
            // Ensure that the lock is released.
            if (lockTaken)
            {
                Monitor.Exit(this.lockObj);
            }
        }

        await Task.Yield();
    }

    ///<inheritdoc/>
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc/>
    public async ValueTask DisposeAsync()
    {
        this.Dispose(true);
        await ValueTask.CompletedTask;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="disposing"></param>
    protected virtual void Dispose(bool disposing)
    {
        if (this.disposed)
        {
            return;
        }

        this.Stop().GetAwaiter().GetResult();
        this.disposed = true;
        this.consumer.Received -= this.OnMsgReceiveHandler;
    }

    private async Task OnMsgReceiveHandler(object sender, BasicDeliverEventArgs ea)
    {
        try
        {
            using var scope = this.serviceScopeFactory.CreateScope();

            var body = ea.Body;
            var props = ea.BasicProperties;
            var replyProps = this.Channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;

            var request = this.restMessageSerializer.DeserializeRequest(body);

            var feature = request.AsFeatureCollection(this.streamResolver);
            var context = this.application.CreateContext(feature);
            var appException = default(Exception);

            try
            {
                await this.application.ProcessRequestAsync(context)
                    .ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                appException = ex;
                this.logger.LogError($"Application error: {ex.Message}");

                feature.Dispose();
                feature = ex.AsFeatureCollection(this.streamResolver, this.restMessageSerializer.Encoder);
            }

            // prepare response.
            HttpRestResponse response;

            try
            {
                response = feature.AsHttpRestResponse();
            }
            catch (Exception ex)
            {
                response = ex.AsFeatureCollection(this.streamResolver, this.restMessageSerializer.Encoder)
                    .AsHttpRestResponse();
            }

            // finalize response.

            try
            {
                if (!string.IsNullOrWhiteSpace(props.ReplyTo))
                {
                    var responseBytes = this.restMessageSerializer.SerializeResponse(response);

                    this.Channel.BasicPublish(exchange: this.exchangeName, routingKey: props.ReplyTo, basicProperties: replyProps, body: responseBytes);
                }

                this.Channel.BasicAck(ea.DeliveryTag, false);
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Something was happened when the response was sent into client publisher, message: {ex.Message}");
            }

            this.application.DisposeContext(context, appException);

            feature.Dispose();

            await Task.CompletedTask;
        }
        catch (Exception ex)
        {
            this.Channel.BasicReject(ea.DeliveryTag, false);
            await this.CatchException(sender, ex, ea);
        }
    }

    private async Task CatchException(object sender, Exception ex, BasicDeliverEventArgs ea)
    {
        this.Channel.BasicReject(ea.DeliveryTag, false);

        await this.OnException.InvokeAsync(sender, new MessageExceptionEventArgs
        {
            PayloadType = typeof(ContextScope),
            Exception = ex
        });
    }
}
