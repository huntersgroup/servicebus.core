using Microsoft.AspNetCore.Http;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Events;

namespace ServiceBus.AspNetCore.Subscribers;

/// <summary>
/// 
/// </summary>
public interface IRestHostSubscriber : IAsyncRunnable
{
    /// <summary>
    /// 
    /// </summary>
    IDelegateHandler<MessageExceptionEventArgs> OnException { get; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="middleware"></param>
    void Init(RequestDelegate middleware);

}
