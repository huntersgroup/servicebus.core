using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace ServiceBus.AspNetCore.Features;

/// <summary>
/// 
/// </summary>
/// <param name="stream"></param>
/// <param name="priorFeature"></param>
public class StreamResponseFeature(Stream stream, IHttpResponseBodyFeature priorFeature = null)
    : StreamResponseBodyFeature(stream, priorFeature!), IHttpResponseFeature
{
    private Stream localStream;

    ///<inheritdoc />
    public void OnStarting(Func<object, Task> callback, object state)
    {
    }

    ///<inheritdoc />
    public void OnCompleted(Func<object, Task> callback, object state)
    {
    }

    ///<inheritdoc />
    public int StatusCode { get; set; } = 200;

    ///<inheritdoc />
    public string ReasonPhrase { get; set; }

    ///<inheritdoc />
    public IHeaderDictionary Headers { get; set; } = new HeaderDictionary();

    ///<inheritdoc />
    Stream IHttpResponseFeature.Body
    {
        get
        {
            return this.localStream ?? this.Stream;
        }
        set
        {
            this.localStream = value;
        }
    }

    ///<inheritdoc />
    public bool HasStarted
    {
        get
        {
            return this.Stream.Length > 0;
        }
    }
}
