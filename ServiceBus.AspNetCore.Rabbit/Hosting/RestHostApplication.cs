using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace ServiceBus.AspNetCore.Hosting;

/// <summary>
/// 
/// </summary>
/// <param name="middleware"></param>
/// <param name="httpContextFactory"></param>
public class RestHostApplication(RequestDelegate middleware, IHttpContextFactory httpContextFactory)
    : IHttpApplication<ContextScope>
{
    
    public ContextScope CreateContext(IFeatureCollection contextFeatures)
    {
        return new ContextScope
        {
            HttpContext = httpContextFactory.Create(contextFeatures)
        };
    }

    public async Task ProcessRequestAsync(ContextScope context)
    {
        context.StartTime ??= DateTime.UtcNow;
        await middleware(context.HttpContext);
    }

    public void DisposeContext(ContextScope context, Exception exception)
    {
        context.EndTime ??= DateTime.UtcNow;
        httpContextFactory.Dispose(context.HttpContext);
    }
}
