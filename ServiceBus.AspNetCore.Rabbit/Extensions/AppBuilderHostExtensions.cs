using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceBus.AspNetCore.Subscribers;

namespace ServiceBus.AspNetCore.Extensions;

/// <summary>
/// 
/// </summary>
public static class AppBuilderHostExtensions
{
    /// <summary>
    /// Adds an instance of <see cref="IRestHostSubscriber"/> to be running in parallel to the host.
    /// <para>
    /// The scope of this <see cref="IRestHostSubscriber"/> instance is to deque http messages from message broker, then forwarding into the aspnet pipeline.
    /// </para>
    /// </summary>
    /// <param name="app"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseSubscriberHost(this IApplicationBuilder app)
    {
        var serviceProvider = app.ApplicationServices;

        var lifetime = serviceProvider.GetRequiredService<IHostApplicationLifetime>();

        var host = serviceProvider.GetRequiredService<IRestHostSubscriber>();

        lifetime.ApplicationStarted.Register(() =>
        {
            host.Init(app.Build());

            host.Start().GetAwaiter().GetResult();
        });

        lifetime.ApplicationStopping.Register(() =>
        {
            host.Stop().GetAwaiter().GetResult();
        });

        return app;
    }
}
