using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Primitives;
using ServiceBus.AspNetCore.Features;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;
using ServiceBus.Core.IO;

namespace ServiceBus.AspNetCore.Extensions;

/// <summary>
/// 
/// </summary>
public static class FeatureCollectionsExtensions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    /// <param name="streamResolver"></param>
    /// <returns></returns>
    public static IFeatureCollection AsFeatureCollection(this HttpRestRequest request, IMemoryStreamResolver streamResolver)
    {
        var feature = new FeatureCollection();

        var uri = request.Resource.BuildUri(null);

        // setup request feature
        var requestFeature = new HttpRequestFeature
        {
            Path = uri.AbsolutePath.UnEscapeUriString(),
            RawTarget = uri.AbsolutePath,
            Protocol = $"{HttpConstants.PrefixHttpVersion}{request.Version}",
            QueryString = uri.Query,
            Method = request.Method.Translate(),
            Body = streamResolver.Build(request.Content)
        };

        requestFeature.Headers.Upsert(request.Headers);
        requestFeature.Headers.ContentLength = request.Content?.Length;

        // add request
        feature.Set<IHttpRequestFeature>(requestFeature);

        // setup connection feature
        var connectionFeature = new HttpConnectionFeature
        {
            RemoteIpAddress = IPAddress.Any,
            LocalIpAddress = null,
            ConnectionId = Guid.NewGuid().ToString("D")
        };

        // add connection feature
        feature.Set<IHttpConnectionFeature>(connectionFeature);

        var responseFeature = new StreamResponseFeature(streamResolver.Build())
        {
            Headers =
            {
                [nameof(HttpHeaders.Server)] = HttpHeaders.Server
            }
        };

        feature.Set<IHttpResponseFeature>(responseFeature);
        feature.Set<IHttpResponseBodyFeature>(responseFeature);

        return feature;
    }

    public static IFeatureCollection AsFeatureCollection(this Exception ex, IMemoryStreamResolver streamResolver, Encoding encoder)
    {
        var sb = new StringBuilder();
        sb.Append("Exception: \r\n\r\n");
        sb.Append(ex.Message);
        sb.Append("\r\n\r\nStackTrace: \r\n\r\n");
        sb.Append(ex.StackTrace);

        if (ex.InnerException != null)
        {
            sb.Append("Inner Exception: \r\n\r\n");
            sb.Append(ex.InnerException.Message);
            sb.Append("\r\n\r\nStackTrace: \r\n\r\n");
            sb.Append(ex.InnerException.StackTrace);
        }

        return HttpStatusCode.InternalServerError.AsFeatureCollection("An unexpected exception was thrown.",
            streamResolver,
            encoder.GetBytes(sb.ToString()));
    }

    public static IFeatureCollection AsFeatureCollection(
        this HttpStatusCode status,
        string reasonPhrase,
        IMemoryStreamResolver streamResolver,
        byte[] body = null)
    {
        var feature = new FeatureCollection();

        var res = new StreamResponseFeature(streamResolver.Build(body))
        {
            StatusCode = (int)status,
            ReasonPhrase = reasonPhrase,
        };

        feature.Set<IHttpResponseFeature>(res);
        feature.Set<IHttpResponseBodyFeature>(res);

        return feature;
    }

    public static HttpRestResponse AsHttpRestResponse(this IFeatureCollection feature)
    {
        if (feature.Get<IHttpResponseFeature>() is not StreamResponseFeature res)
        {
            throw new InvalidOperationException();
        }

        var response = new HttpRestResponse
        {
            Version = HttpConstants.DefaultHttpVersion,
            StatusCode = res.StatusCode,
            StatusDescription = string.IsNullOrEmpty(res.ReasonPhrase) ? res.StatusCode.ToReasonPhrase() : res.ReasonPhrase,
            Content = res.Stream.ToArray()
        };

        response.Headers.Upsert(res.Headers);

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="feature"></param>
    public static void Dispose(this IFeatureCollection feature)
    {
        var reqFeature = feature.Get<IHttpRequestFeature>();
        var respFeature = feature.Get<IHttpResponseBodyFeature>();

        reqFeature?.Body?.Dispose();
        respFeature?.Stream?.Dispose();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dictionary"></param>
    /// <param name="args"></param>
    private static void Upsert(this IDictionary<string, StringValues> dictionary, IDictionary<string, List<string>> args)
    {
        foreach (var arg in args)
        {
            dictionary[arg.Key] = arg.Value.ToArray();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dictionary"></param>
    /// <param name="args"></param>
    private static void Upsert(this IDictionary<string, List<string>> dictionary, IDictionary<string, StringValues> args)
    {
        foreach (var arg in args)
        {
            dictionary[arg.Key] = arg.Value.ToList();
        }
    }
}
