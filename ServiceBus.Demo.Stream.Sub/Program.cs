using System.Net;
using System.Text;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Stream.Client;
using RabbitMQ.Stream.Client.Reliable;
using ServiceBus.Demo.Common.Config;
using ServiceBus.Demo.Common.Extensions;

namespace ServiceBus.Demo.Stream.Sub;

internal class Application
{
    public Application()
    {
        this.Configuration = this.BuildConfiguration();
        this.AppSettings = this.Configuration.Get<AppSettingsBase>() ?? new AppSettingsBase();
    }

    public IConfigurationRoot Configuration { get; }

    public AppSettingsBase AppSettings { get; }

    private static async Task Main(params string[] args)
    {
        var app = new Application();
        var cred = app.AppSettings.EsbCredentials;
        var esb = app.AppSettings.EsbAddress;
        var endpoint = new DnsEndPoint(esb.Host, esb.Port);

        var streamCfg = new StreamSystemConfig
        {
            UserName = cred.Username,
            Password = cred.Password,
            Endpoints =
            [
                endpoint
            ],
            AddressResolver = new AddressResolver(endpoint)
        };

        var streamSystem = await StreamSystem.Create(streamCfg);

        Console.Write("write the consumer name: ");
        var consumerName = Console.ReadLine() ?? "my-demo-app";

        await RunConsumer(streamSystem, "hello-stream", consumerName);

        await streamSystem.Close();
    }

    private static async Task RunConsumer(StreamSystem streamSystem, string streamName, string consumerName)
    {
        var consumer = await Consumer.Create(new ConsumerConfig(streamSystem, streamName)
        {
            //OffsetSpec = new OffsetTypeFirst(),
            OffsetSpec = new OffsetTypeNext(),
            Reference = consumerName,
            MessageHandler = async (stream, consumer, context, message) =>
            {
                Console.WriteLine($"Stream: {stream} - time: {DateTime.UtcNow:u} - " +
                                  $"Received message: {Encoding.UTF8.GetString(message.Data.Contents)}");

                //consumer.StoreOffset(context.Offset);
                await Task.Delay(TimeSpan.FromSeconds(5));

                await Task.CompletedTask;
            }
        });

        Console.WriteLine(" [x] Press any key to exit");
        Console.ReadKey();

        await consumer.Close();
    }
}
