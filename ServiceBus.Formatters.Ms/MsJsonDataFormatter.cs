using System.Text;
using System.Text.Json;

namespace ServiceBus.Core.Formatters;

/// <summary>
/// Represents a common way to encode / decode messages from underlying channels using a <see cref="JsonSerializer"/> serializer.
/// <para>
/// This client uses <see cref="JsonSerializer"/> instance for reading and writing data.
/// </para>
/// </summary>
public class MsJsonDataFormatter : IDataFormatter
{
    private readonly JsonSerializerOptions options;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="options"></param>
    public MsJsonDataFormatter(JsonSerializerOptions options)
    {
        this.options = options;
        this.Encoder = Encoding.UTF8;
    }

    ///<inheritdoc/>
    public Encoding Encoder { get; }

    ///<inheritdoc/>
    public TData Decode<TData>(byte[] data)
    {
        var readOnlySpan = new ReadOnlySpan<byte>(data);

        return JsonSerializer.Deserialize<TData>(readOnlySpan, this.options);
    }

    ///<inheritdoc/>
    public TData Decode<TData>(ReadOnlyMemory<byte> data)
    {
        return JsonSerializer.Deserialize<TData>(data.Span, this.options);
    }

    ///<inheritdoc/>
    public object Decode(byte[] data, Type type)
    {
        var readOnlySpan = new ReadOnlySpan<byte>(data);

        return JsonSerializer.Deserialize(readOnlySpan, type, this.options);
    }

    ///<inheritdoc/>
    public object Decode(ReadOnlyMemory<byte> data, Type type)
    {
        return JsonSerializer.Deserialize(data.Span, type, this.options);
    }

    ///<inheritdoc/>
    public byte[] Encode(object data)
    {
        var type = data?.GetType() ?? typeof(object);
        return JsonSerializer.SerializeToUtf8Bytes(data, type, this.options);
    }
}
