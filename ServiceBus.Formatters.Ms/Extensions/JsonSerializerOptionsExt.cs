using ServiceBus.Core.Formatters.Converters;
using System.Text.Json;

namespace ServiceBus.Core.Formatters.Extensions;

/// <summary>
/// 
/// </summary>
public static class JsonSerializerOptionsExt
{
    /// <summary>
    /// Add some custom converters (for double, float and decimal types) in order to normalize serialization, whenever is missing the fractional portion.
    /// <code>
    /// example:
    /// var decimalVal = 10m; // -> serialized in 10.0
    /// var doubleVal = 10d;  // -> serialized in 10.0
    /// var floatVal = 10f;   // -> serialized in 10.0
    ///
    /// decimalVal = 10.00000m // -> serialized in 10.0
    /// </code>
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public static JsonSerializerOptions ApplyRealNumberWithFractionalPortion(this JsonSerializerOptions options)
    {
        options.Converters.Add(new FractionalPortionDecimalConverter());
        options.Converters.Add(new FractionalPortionDoubleConverter());
        options.Converters.Add(new FractionalPortionFloatConverter());

        return options;
    }

    /// <summary>
    /// Add a custom converter in order to normalize serialization for <see cref="decimal"/> types, whenever is missing the fractional portion.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public static JsonSerializerOptions ApplyDecimalWithFractionalPortion(this JsonSerializerOptions options)
    {
        options.Converters.Add(new FractionalPortionDecimalConverter());

        return options;
    }

    /// <summary>
    /// Add a custom converter in order to normalize serialization for <see cref="double"/> types, whenever is missing the fractional portion.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public static JsonSerializerOptions ApplyDoubleWithFractionalPortion(this JsonSerializerOptions options)
    {
        options.Converters.Add(new FractionalPortionDoubleConverter());

        return options;
    }

    /// <summary>
    /// Add a custom converter in order to normalize serialization for <see cref="float"/> types, whenever is missing the fractional portion.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public static JsonSerializerOptions ApplyFloatWithFractionalPortion(this JsonSerializerOptions options)
    {
        options.Converters.Add(new FractionalPortionFloatConverter());

        return options;
    }
}
