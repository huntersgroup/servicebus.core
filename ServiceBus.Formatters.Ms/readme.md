**ServiceBus.Formatters.Ms** is the implementation of **ServiceBus.Formatters** library, which is based on System.Text.Json serializer.

for details, see the [documentation](https://servicebuscore.readthedocs.io/en/latest/).
