using System.Globalization;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace ServiceBus.Core.Formatters.Converters;

/// <summary>
/// Represents a custom json converter for <see cref="double"/> types, adding and normalizing fractional portion during serialization.
/// <code>
/// example: 
/// var floatVal = 10d;   // -> serialized in 10.0
/// 
/// </code>
/// </summary>
public class FractionalPortionDoubleConverter : JsonConverter<double>
{
    private const string Format = "F1";

    ///<inheritdoc />
    public override double Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return reader.GetDouble();
    }

    ///<inheritdoc />
    public override void Write(Utf8JsonWriter writer, double value, JsonSerializerOptions options)
    {
        if (value % 1 == 0)
        {
            writer.WriteRawValue(value.ToString(Format, CultureInfo.InvariantCulture));
        }
        else
        {
            writer.WriteNumberValue(value);
        }
    }
}
