using System.Globalization;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace ServiceBus.Core.Formatters.Converters;

/// <summary>
/// Represents a custom json converter for <see cref="float"/> types, adding and normalizing fractional portion during serialization.
/// <code>
/// example: 
/// var floatVal = 10f;   // -> serialized in 10.0
/// 
/// </code>
/// </summary>
public class FractionalPortionFloatConverter : JsonConverter<float>
{
    private const string Format = "F1";

    ///<inheritdoc />
    public override float Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return reader.GetSingle();
    }

    ///<inheritdoc />
    public override void Write(Utf8JsonWriter writer, float value, JsonSerializerOptions options)
    {
        if (value % 1 == 0)
        {
            writer.WriteRawValue(value.ToString(Format, CultureInfo.InvariantCulture));
        }
        else
        {
            writer.WriteNumberValue(value);
        }
    }
}
