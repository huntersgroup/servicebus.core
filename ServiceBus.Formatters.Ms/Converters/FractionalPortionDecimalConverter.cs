using System.Globalization;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace ServiceBus.Core.Formatters.Converters;

/// <summary>
/// Represents a custom json converter for <see cref="decimal"/> types, adding and normalizing fractional portion during serialization.
/// <code>
/// example: 
/// var floatVal1 = 10m;       // -> serialized in 10.0
/// var floatVal2 = 10.000m;   // -> serialized in 10.0
/// 
/// </code>
/// </summary>
public class FractionalPortionDecimalConverter : JsonConverter<decimal>
{
    private const string Format = "F1";

    ///<inheritdoc />
    public override decimal Read(ref Utf8JsonReader reader, Type objectType, JsonSerializerOptions options)
    {
        return reader.GetDecimal();
    }

    ///<inheritdoc />
    public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options)
    {
        if (value % 1 == 0)
        {
            writer.WriteRawValue(value.ToString(Format, CultureInfo.InvariantCulture));
        }
        else
        {
            writer.WriteNumberValue(value);
        }
    }
}
