using System.Text.Json;

namespace ServiceBus.Core.Formatters.Json;

/// <summary>
/// Represents a json serializer implemented by System.Text.Json
/// </summary>
/// <param name="options"></param>
public class MsJsonSerializer(JsonSerializerOptions options) : IJsonSerializer
{
    ///<inheritdoc />
    public string Serialize(object instance)
    {
        var type = instance?.GetType() ?? typeof(object);
        return JsonSerializer.Serialize(instance, type, options);
    }

    ///<inheritdoc />
    public TData Deserialize<TData>(string json)
    {
        if (string.IsNullOrWhiteSpace(json))
        {
            return default;
        }

        return JsonSerializer.Deserialize<TData>(json, options);
    }
}
