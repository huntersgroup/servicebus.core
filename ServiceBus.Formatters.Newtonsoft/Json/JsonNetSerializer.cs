using Newtonsoft.Json;

namespace ServiceBus.Core.Formatters.Json;

/// <summary>
/// Represents a json serializer implemented by Json.net
/// </summary>
/// <param name="settings"></param>
public class JsonNetSerializer(JsonSerializerSettings settings)
    : IJsonSerializer
{
    ///<inheritdoc />
    public string Serialize(object instance)
    {
        return JsonConvert.SerializeObject(instance, settings);
    }

    ///<inheritdoc />
    public TData Deserialize<TData>(string json)
    {
        return JsonConvert.DeserializeObject<TData>(json, settings);
    }
}
