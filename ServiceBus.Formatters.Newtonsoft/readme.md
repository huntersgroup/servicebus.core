**ServiceBus.Formatters.Newtonsoft** is the implementation of **ServiceBus.Formatters** library, which is based on newtownsoft serializer.

for details, see the [documentation](https://servicebuscore.readthedocs.io/en/latest/).
