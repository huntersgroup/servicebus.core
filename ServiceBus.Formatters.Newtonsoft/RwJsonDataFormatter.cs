using Newtonsoft.Json;
using System.Text;

namespace ServiceBus.Core.Formatters;

/// <summary>
/// Represents a common way to encode / decode messages from underlying channels using a json serializer and using the given encoder.
/// <para>
/// This client uses <see cref="JsonSerializer"/> instance for reading and writing data.
/// </para>
/// </summary>
public class RwJsonDataFormatter(JsonSerializer serializer, Encoding encoder)
    : IDataFormatter
{
    ///<inheritdoc/>
    public Encoding Encoder { get => encoder; }

    ///<inheritdoc/>
    public byte[] Encode(object data)
    {
        using (var writer = new StringWriter())
        {
            serializer.Serialize(writer, data);
            return this.Encoder.GetBytes(writer.ToString());
        }
    }

    ///<inheritdoc/>
    public TData Decode<TData>(byte[] data)
    {
        using (var reader = new JsonTextReader(new StringReader(this.Encoder.GetString(data))))
        {
            return serializer.Deserialize<TData>(reader);
        }
    }

    ///<inheritdoc/>
    public TData Decode<TData>(ReadOnlyMemory<byte> data)
    {
        return this.Decode<TData>(data.ToArray());
    }

    ///<inheritdoc/>
    public object Decode(byte[] data, Type type)
    {
        using (var reader = new JsonTextReader(new StringReader(this.Encoder.GetString(data))))
        {
            return serializer.Deserialize(reader, type);
        }
    }

    ///<inheritdoc/>
    public object Decode(ReadOnlyMemory<byte> data, Type type)
    {
        return this.Decode(data.ToArray(), type);
    }
}
