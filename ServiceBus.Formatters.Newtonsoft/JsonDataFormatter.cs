using Newtonsoft.Json;
using System.Text;

namespace ServiceBus.Core.Formatters;

/// <summary>
/// Represents a common way to encode / decode messages from underlying channels using a json serializer and using the given encoder.
/// </summary>
public class JsonDataFormatter(JsonSerializerSettings jsonSettings, Encoding encoder)
    : IDataFormatter
{
    ///<inheritdoc/>
    public Encoding Encoder { get => encoder; }

    ///<inheritdoc/>
    public byte[] Encode(object data)
    {
        var json = JsonConvert.SerializeObject(data, jsonSettings);

        return this.Encoder.GetBytes(json);
    }

    ///<inheritdoc/>
    public TData Decode<TData>(byte[] data)
    {
        var json = this.Encoder.GetString(data);

        return JsonConvert.DeserializeObject<TData>(json, jsonSettings);
    }

    ///<inheritdoc/>
    public TData Decode<TData>(ReadOnlyMemory<byte> data)
    {
        return this.Decode<TData>(data.ToArray());
    }

    ///<inheritdoc/>
    public object Decode(byte[] data, Type type)
    {
        var json = this.Encoder.GetString(data);

        return JsonConvert.DeserializeObject(json, type);
    }

    ///<inheritdoc/>
    public object Decode(ReadOnlyMemory<byte> data, Type type)
    {
        return this.Decode(data.ToArray(), type);
    }
}
