namespace ServiceBus.Demo.Common;

/// <summary>
/// 
/// </summary>
public class Credentials
{
    /// <summary>
    /// Gets or sets the username.
    /// </summary>
    public string Username { get; set; }

    /// <summary>
    /// Gets or sets the password.
    /// </summary>
    public string Password { get; set; }
}
