namespace ServiceBus.Demo.Common;

public static class ConsoleExt
{
    public static void WriteLine(string text, ConsoleColor color)
    {
        Console.ForegroundColor = color;
        Console.WriteLine(text);
        Console.ResetColor();
    }

    public static void Write(string text, ConsoleColor color)
    {
        Console.ForegroundColor = color;
        Console.Write(text);
        Console.ResetColor();
    }

    public static string ReadInput(string question)
    {
        return ReadInput(question, Console.ForegroundColor);
    }

    public static string ReadInput(string question, ConsoleColor foregroundColor)
    {
        Console.ForegroundColor = foregroundColor;
        Console.Write(question);
        Console.ResetColor();

        var value = Console.ReadLine();

        return value;
    }

    public static TInput ReadInput<TInput>(string question, Func<string, TInput> converter)
    {
        return ReadInput<TInput>(question, converter, Console.ForegroundColor);
    }

    public static TInput ReadInput<TInput>(string question, Func<string, TInput> converter, ConsoleColor foregroundColor)
    {
        Console.ForegroundColor = foregroundColor;
        Console.Write(question);
        Console.ResetColor();

        var value = Console.ReadLine();

        try
        {
            return converter(value);
        }
        catch (OperationCanceledException)
        {
            throw;
        }
        catch
        {
            return default;
        }
    }

    public static string ExitOnCommand(this string command)
    {
        const string exit = "exit";

        if (command != null && command.Equals(exit, StringComparison.OrdinalIgnoreCase))
        {
            throw new OperationCanceledException("**** Exit command was sent. ****");
        }

        return command;
    }
}
