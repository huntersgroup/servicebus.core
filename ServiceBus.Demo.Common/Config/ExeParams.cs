namespace ServiceBus.Demo.Common.Config;

public class ExeParams
{
    public SerializerType SerializerType { get; init; }

    public SubscriberType SubscriberType { get; init; }
}

public enum SerializerType : byte
{
    Microsoft = 0,
    Json = 1
}

public enum SubscriberType : byte
{
    Bound = 0,

    Bridge = 1,

    BoundNoGeneric = 2,
}
