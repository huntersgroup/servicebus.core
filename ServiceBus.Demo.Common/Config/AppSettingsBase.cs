namespace ServiceBus.Demo.Common.Config;

public class AppSettingsBase
{
    public Uri EsbAddress { get; set; }

    public Credentials EsbCredentials { get; set; }
}
