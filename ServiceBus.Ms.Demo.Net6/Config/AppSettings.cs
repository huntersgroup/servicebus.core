using ServiceBus.AspNetCore.Subscribers.Descriptors;

namespace ServiceBus.Ms.Demo.Config;

/// <summary>
/// 
/// </summary>
public class AppSettings
{
    /// <summary>
    /// 
    /// </summary>
    public RestSubscriberBoundDescriptor BrokerDescriptor { get; set; }
}
