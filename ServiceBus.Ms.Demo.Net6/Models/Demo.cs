namespace ServiceBus.Ms.Demo.Models;

/// <summary>
/// Demo class
/// </summary>
public class Demo
{
    /// <summary>
    /// Gets or sets the given id.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the given value.
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// Gets or sets the current price.
    /// </summary>
    public double Price { get; set; }
}
