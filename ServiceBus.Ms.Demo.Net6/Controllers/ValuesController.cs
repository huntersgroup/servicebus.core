using Microsoft.AspNetCore.Mvc;

namespace ServiceBus.Ms.Demo.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    public class ValuesController : ControllerBase
    {
        /// <summary>
        /// Gets all demo instances.
        /// </summary>
        /// <returns></returns>
        // GET: api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.FromResult(this.Ok(new List<string>{ "value1", "value2", "value3" }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            return await Task.FromResult(this.Ok(id));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] string val)
        {
            return await Task.FromResult(this.Ok(val));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("_date")]
        public async Task<IActionResult> GetDate()
        {
            return await Task.FromResult(this.Ok(DateTime.UtcNow));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        [HttpGet("_date/{currentDate}")]
        public async Task<IActionResult> PrintDate(DateTime currentDate)
        {
            return await Task.FromResult(this.Ok(currentDate));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("_int")]
        public async Task<IActionResult> GetInt()
        {
            return await Task.FromResult(this.Ok(int.MaxValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet("_search")]
        public async Task<IActionResult> Get(string parameter, int count)
        {
            if (count <= 0)
            {
                throw new ArgumentException($"count parameter cannot be less or equals than zero, value: {count}");
            }

            return await Task.FromResult(this.Ok(new
            {
                parameter,
                count
            }));
        }
    }
}
