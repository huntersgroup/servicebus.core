using Microsoft.AspNetCore.Mvc;

namespace ServiceBus.Ms.Demo.Controllers
{
    /// <summary>
    /// Represents a demo controller, only for demo.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly IList<Models.Demo> demoStorage;

        /// <summary>
        /// Creates a new <see cref="DemoController"/> instance.
        /// </summary>
        /// <param name="demoStorage"></param>
        public DemoController(IList<Models.Demo> demoStorage)
        {
            this.demoStorage = demoStorage;
        }

        /// <summary>
        /// Gets all demo instances.
        /// </summary>
        /// <returns></returns>
        // GET: api/demo
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.FromResult(this.Ok(this.demoStorage));
        }

        /// <summary>
        /// /Gets a given resource with the specified id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/demo/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            var instance = this.demoStorage.FirstOrDefault(demo => demo.Id == id);

            IActionResult res;

            if (instance == null)
            {
                res = this.NotFound();
            }
            else
            {
                res = this.Ok(instance);
            }

            return await Task.FromResult(res);
        }

        /// <summary>
        /// Creates or updates the given resource.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        // POST: api/demo
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Models.Demo instance)
        {
            if (this.UpdateInstance(instance))
            {
                return await Task.FromResult(this.Ok(instance));
            }

            if (instance.Id == default)
            {
                instance.Id = this.demoStorage.Any() ? this.demoStorage.Max(demo => demo.Id) + 1 : 1;
            }

            this.demoStorage.Add(instance);

            return await Task.FromResult(this.Created($"{this.HttpContext.Request.Path}/{instance.Id}", instance));
        }

        /// <summary>
        /// Updates the given resource.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        // PUT: api/demo
        [HttpPut("")]
        public async Task<IActionResult> Put([FromBody] Models.Demo instance)
        {
            if (this.UpdateInstance(instance))
            {
                return await Task.FromResult(this.Ok(instance));
            }

            this.ModelState.AddModelError("not-found", $"The given instance doesn't exists, id: {instance.Id}");

            return this.NotFound();
        }

        /// <summary>
        /// Deletes a resource with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/demo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var demo = this.demoStorage.FirstOrDefault(dm => dm.Id == id);

            if (demo == null)
            {
                this.ModelState.AddModelError("not-found", $"The given instance doesn't exists, id: {id}");

                return this.NotFound();
            }

            this.demoStorage.Remove(demo);

            return await Task.FromResult(this.Accepted());
        }

        [HttpGet("_count")]
        public async Task<IActionResult> Count()
        {
            return await Task.FromResult(this.Ok(new { count = this.demoStorage.Count }));
        }

        private bool UpdateInstance(Models.Demo instance)
        {
            var storedInstance = this.demoStorage.FirstOrDefault(dm => dm.Id == instance.Id);

            if (storedInstance != null)
            {
                storedInstance.Price = instance.Price;
                storedInstance.Value = instance.Value;

                return true;
            }

            return false;
        }
    }
}
