using System.Text;
using Microsoft.IO;
using RabbitMQ.Client;
using ServiceBus.AspNetCore.Subscribers;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.IO;
using ServiceBus.Ms.Demo.Config;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceBus.Ms.Demo.Modules;

public class SubscriberModule : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        var descriptor = serviceRegister.Cache.Get<AppSettings>().BrokerDescriptor;

        serviceRegister.Register<IRestHostSubscriber, RestHostSubscriberBound>(LifetimeScope.Singleton);

        // IModel registration
        serviceRegister
            .Register(provider => provider.GetService<IConnection>().CreateChannel(), LifetimeScope.Singleton)
            .Register(provider => provider.GetService<IConnectionFactory>().CreateConnection(), LifetimeScope.Singleton)
            .Register<IConnectionFactory>(_ => new ConnectionFactory
            {
                Uri = descriptor.Address,
                UserName = descriptor.Credentials.Username,
                Password = descriptor.Credentials.Password,
                DispatchConsumersAsync = true
            }, LifetimeScope.Singleton);

        // descriptor
        serviceRegister
            .Register(_ => descriptor, LifetimeScope.Singleton);

        // IHttpRestMessageSerializer & IMemoryStreamResolver
        serviceRegister
            .Register<IHttpRestMessageSerializer, HttpRestMessageSerializer>(LifetimeScope.Singleton)
            .Register<IMemoryStreamResolver>(serviceProvider =>
            {
                var streamManager = serviceProvider.GetService<RecyclableMemoryStreamManager>();
                var ms = new FuncMemoryStreamResolver(() => streamManager.GetStream(), bytes => streamManager.GetStream(bytes));

                return ms;
            }, LifetimeScope.Singleton)
            .Register(() => new RecyclableMemoryStreamManager(), LifetimeScope.Singleton)
            .Register(() => Encoding.UTF8, LifetimeScope.Singleton);
    }
}
