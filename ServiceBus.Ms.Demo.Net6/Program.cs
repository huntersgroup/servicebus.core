using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using ServiceBus.AspNetCore.Extensions;
using ServiceBus.Ms.Demo.Config;
using ServiceResolver.Ioc.AspNetCore.Extensions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;

var builder = WebApplication.CreateBuilder(args);

builder.Host
    .UseServiceResolver(context => new SimpleInjectorOptions
    {
        //ConstructorResolutionType = ResolutionType.Hybrid,
        IgnoreMissingRegistrations = true,
        BeforeRegistering = collection =>
        {
            collection.AddControllers()
                .AddJsonOptions(opt =>
                {
                    opt.AllowInputFormatterExceptionMessages = true;

                    var options = opt.JsonSerializerOptions;
                    options.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                    options.PropertyNameCaseInsensitive = true;
                    options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    options.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                    options.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;

                    //options.Converters.Add(new JsonStringEnumConverter());
                    //options.ApplyRealNumberWithFractionalPortion();
                });
        },
        OnRegistering = serviceRegister =>
        {
            var appSettings = context.Configuration.Get<AppSettings>();

            serviceRegister.Cache.Upsert(appSettings);

            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation()
                ;

            serviceRegister.RegisterModulesFrom(Assembly.GetExecutingAssembly());
        },
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    });

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

//app.MapControllers();

app.UseSubscriberHost();

await app.RunAsync();
