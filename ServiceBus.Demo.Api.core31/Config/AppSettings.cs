﻿using ServiceBus.AspNetCore.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Demo.Api.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public RestHostDescriptor BrokerDescriptor { get; set; }
    }
}
