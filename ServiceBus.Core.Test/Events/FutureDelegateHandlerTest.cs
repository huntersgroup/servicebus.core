using ServiceBus.Core.Events;
using Xunit;
using Xunit.Abstractions;

namespace ServiceBus.Core.Test.Events;

public class FutureDelegateHandlerTest
{
    private readonly ITestOutputHelper output;

    public FutureDelegateHandlerTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    [Fact]
    private async Task InvokeAsyncTest()
    {
        var builder = new NextDelegateHandler<int>(null, null);

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync1);

        for (var i = 0; i < 5; i++)
        {
            await builder.InvokeAsync(this, i);
        }

        await Task.Yield();
    }

    [Fact]
    private async Task InvokeAsyncWithFutureTest()
    {
        var future = new DelegateHandler<int>();
        future.Add(this.NextHandlerAsync1);

        var builder = new NextDelegateHandler<int>(future, null);

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync1);

        for (var i = 0; i < 2; i++)
        {
            await builder.InvokeAsync(this, i);
        }

        await Task.Yield();
    }

    [Fact]
    private async Task InvokeAsyncWithListenersTest()
    {
        var listeners = new List<IDelegateHandler<int>>
        {
            new DelegateHandler<int>(),
            new DelegateHandler<int>()
        };

        var builder = new NextDelegateHandler<int>(null, listeners);

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync2)
            .Add(this.Handler)
            .Add(this.HandlerAsync1)
            .Add(this.HandlerAsync2);

        var expected = 5;

        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        ////
        // acts
        builder.Remove(this.HandlerAsync2);
        expected--;

        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        ////
        // acts
        builder.Remove(this.HandlerAsync2);
        expected--;

        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        ////
        // acts
        builder.Remove(this.HandlerAsync);
        expected--;
        Assert.Equal(expected, builder.Count());

        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        ////
        // acts
        builder.Remove(this.HandlerAsync2);
        
        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        ////
        // acts
        builder.Remove(this.HandlerAsync1);
        expected--;
        Assert.Equal(expected, builder.Count());

        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        ////
        // acts
        builder.Remove(this.Handler);
        expected--;
        Assert.Equal(expected, builder.Count());

        // asserts
        Assert.Equal(expected, builder.Count());
        foreach (var listener in listeners)
        {
            Assert.Equal(expected, listener.Count());
        }

        await Task.Yield();
    }

    [Fact]
    private async Task AddRemoveTest()
    {
        var builder = new NextDelegateHandler<int>(null, null);

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync2)
            .Add(this.Handler)
            .Add(this.HandlerAsync1)
            .Add(this.HandlerAsync2);

        Assert.Equal(5, builder.Count());

        builder.Remove(this.HandlerAsync2);
        Assert.Equal(4, builder.Count());

        builder.Remove(this.HandlerAsync2);
        Assert.Equal(3, builder.Count());

        builder.Remove(this.HandlerAsync);
        Assert.Equal(2, builder.Count());

        builder.Remove(this.HandlerAsync2);
        Assert.Equal(2, builder.Count());

        builder.Remove(this.HandlerAsync1);
        Assert.Equal(1, builder.Count());

        builder.Remove(this.Handler);
        Assert.Equal(0, builder.Count());

        await Task.Yield();
    }

    private async Task HandlerAsync(object sender, int s)
    {
        this.output.WriteLine($"### id: {s}, started: {DateTime.Now:u}");
        this.output.WriteLine($"### id: {s}, working now.");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### id: {s}, task is over: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private async Task HandlerAsync1(object sender, int s)
    {
        this.output.WriteLine($"### id: {s}, started: {DateTime.Now:u}");
        this.output.WriteLine($"### id: {s}, working now.");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### id: {s}, task is over: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private async Task HandlerAsync2(object sender, int s)
    {
        this.output.WriteLine($"### id: {s}, started: {DateTime.Now:u}");
        this.output.WriteLine($"### id: {s}, working now.");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### id: {s}, task is over: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private async Task NextHandlerAsync1(object sender, int s)
    {
        this.output.WriteLine($"### next handler 1 executed, id: {s}, time: {DateTime.Now:u}");
        this.output.WriteLine($"### next handler 1 working, id: {s}");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### next handler 1 is over, id: {s}, time: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private void Handler(object sender, int s)
    {
        this.output.WriteLine($"ciaone, value: {s} ####");
    }
}
