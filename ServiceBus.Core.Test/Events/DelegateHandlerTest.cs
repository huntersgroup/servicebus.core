using ServiceBus.Core.Events;
using Xunit;
using Xunit.Abstractions;

namespace ServiceBus.Core.Test.Events;

public class DelegateHandlerTest
{
    private readonly ITestOutputHelper output;

    public DelegateHandlerTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    [Fact]
    private async Task InvokeAsyncTest()
    {
        var builder = new DelegateHandler<int>();

        builder.Add(this.HandlerAsync)
            .Add((_, s) =>
            {
                this.output.WriteLine($">>> id: {s}, started: {DateTime.Now:u}");
                this.output.WriteLine($">>> id: {s}, working now.");
                Task.Delay(TimeSpan.FromSeconds(5)).GetAwaiter().GetResult();
                this.output.WriteLine($">>> id: {s}, task is over: {DateTime.Now:u}");
                this.output.WriteLine(string.Empty);
            })
            .Add(async (_, s) =>
            {
                this.output.WriteLine($"||| id: {s}, started: {DateTime.Now:u}");
                this.output.WriteLine($"||| id: {s}, working now.");
                await Task.Delay(TimeSpan.FromSeconds(5));
                this.output.WriteLine($"||| id: {s}, task is over: {DateTime.Now:u}");
                this.output.WriteLine(string.Empty);
            });

        for (var i = 0; i < 5; i++)
        {
            await builder.InvokeAsync(this, i);
        }

        await Task.Yield();
    }

    [Fact]
    private async Task AddRemoveTest()
    {
        var builder = new DelegateHandler<int>();

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync2)
            .Add(this.Handler)
            .Add(this.HandlerAsync1)
            .Add(this.HandlerAsync2);

        Assert.Equal(5, builder.Count());

        builder.Remove(this.HandlerAsync2);

        Assert.Equal(4, builder.Count());

        builder.Remove(this.HandlerAsync2);
        Assert.Equal(3, builder.Count());

        builder.Remove(this.HandlerAsync);
        Assert.Equal(2, builder.Count());

        builder.Remove(this.HandlerAsync2);
        Assert.Equal(2, builder.Count());

        builder.Remove(this.HandlerAsync1);
        Assert.Equal(1, builder.Count());

        builder.Remove(this.Handler);
        Assert.Equal(0, builder.Count());

        await Task.Yield();
    }

    [Fact]
    public void ContainsTest()
    {
        var builder = new DelegateHandler<int>();

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync1)
            .Add(this.Handler);

        Assert.True(builder.Contains(this.HandlerAsync));
        Assert.True(builder.Contains(this.HandlerAsync1));
        Assert.True(builder.Contains(this.Handler));
        Assert.False(builder.Contains(this.HandlerAsync2));
    }

    [Fact]
    public void CountTest()
    {
        var builder = new DelegateHandler<int>();

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync1)
            .Add(this.Handler)
            .Add(this.HandlerAsync1);

        // asserts
        Assert.Equal(4, builder.Count());

        // acts
        builder.Remove(this.Handler);

        // asserts
        Assert.Equal(3, builder.Count());
    }

    [Fact]
    public void ClearTest()
    {
        var builder = new DelegateHandler<int>();

        builder.Add(this.HandlerAsync)
            .Add(this.HandlerAsync1)
            .Add(this.Handler)
            .Add(this.HandlerAsync1);

        // acts
        builder.Clear();

        // asserts
        Assert.Equal(0, builder.Count());
    }

    private async Task HandlerAsync(object sender, int s)
    {
        this.output.WriteLine($"### id: {s}, started: {DateTime.Now:u}");
        this.output.WriteLine($"### id: {s}, working now.");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### id: {s}, task is over: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private async Task HandlerAsync1(object sender, int s)
    {
        this.output.WriteLine($"### id: {s}, started: {DateTime.Now:u}");
        this.output.WriteLine($"### id: {s}, working now.");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### id: {s}, task is over: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private async Task HandlerAsync2(object sender, int s)
    {
        this.output.WriteLine($"### id: {s}, started: {DateTime.Now:u}");
        this.output.WriteLine($"### id: {s}, working now.");
        await Task.Delay(TimeSpan.FromSeconds(5));
        this.output.WriteLine($"### id: {s}, task is over: {DateTime.Now:u}");
        this.output.WriteLine(string.Empty);
    }

    private void Handler(object sender, int s)
    {
        this.output.WriteLine($"ciaone, value: {s} ####");
    }
}
