using ServiceBus.Core.Extensions;
using Xunit;

namespace ServiceBus.Core.Test.Extensions;

public class CommonUriExtensionsTest
{
    [Theory]
    [InlineData("/api/Values/my%2Fvalue%209", "/api/Values/my%2Fvalue 9")]
    public void UnEscapeUriStringTest(string uriEncoded, string uriDecoded)
    {
        var decoded = uriEncoded.UnEscapeUriString();

        Assert.Equal(uriDecoded, decoded);
    }
}
