using ServiceBus.Core.Http.Request;
using Xunit;

namespace ServiceBus.Core.Test.Http.Request;

public class RequestParameterTest
{
    [Fact]
    public void ToStringTest()
    {
        var par1 = new RequestParameter
        {
            Name = "arg",
            Value = "val",
            Type = ParameterType.Route
        };

        // asserts
        Assert.Equal(par1.Value, par1.ToString());

        // acts
        par1.Type = ParameterType.Query;

        // asserts
        Assert.Equal($"{par1.Name}={par1.Value}", par1.ToString());

        // acts
        par1.Type = ParameterType.QueryPlaceHolder;

        // asserts
        Assert.Equal(par1.Value, par1.ToString());
    }
}
