using ServiceBus.Core.Http.Request;
using Xunit;

namespace ServiceBus.Core.Test.Http.Request;

public class RestRequestTest
{
    [Fact]
    public void BadInstanceCreation()
    {
        // asserts
        Assert.Throws<ArgumentException>(() => new RestRequest(null));
        Assert.Throws<ArgumentException>(() => new RestRequest("  "));
        Assert.Throws<ArgumentException>(() => new RestRequest(string.Empty));
    }

    [Fact]
    public void InstanceCreation()
    {
        // acts
        var req = new RestRequest("/demo?par1=1&par2=2");

        // asserts
        Assert.Equal("/demo?par1=1&par2=2", req.Resource);

        // acts
        req = new RestRequest("demo?par1=1&par2=2");
        Assert.Equal("/demo?par1=1&par2=2", req.Resource);
    }
        
    [Fact]
    public void SetAndAddQueryParameterTest()
    {
        var path = "api/demo/1/devices";
        var req = new RestRequest(path);

        req.SetQueryParameter("par1", "val1")
            .SetQueryParameter("par2", "val2")
            .SetQueryParameter("par3", "value 9")
            .SetQueryParameter("par4", "value%20C", false);

        // asserts
        Assert.Equal("/api/demo/1/devices?par1=val1&par2=val2&par3=value%209&par4=value%20C", req.Resource);
    }
        
    [Fact]
    public void SetRouteParametersTest()
    {
        var req = new RestRequest("api/{controller}/1/{property}?myarg1=1&myarg2=2&myarg3=value%209");
            
        req.SetRouteParameter("controller", "demo")
            .SetRouteParameter("property", "devices");

        // asserts
        Assert.Equal("/api/demo/1/devices?myarg1=1&myarg2=2&myarg3=value%209", req.Resource);
    }

    [Fact]
    public void SetBadRouteParameter()
    {
        var req = new RestRequest("/api/{myController}");

        // asserts
        Assert.Throws<ArgumentException>(() => req.SetRouteParameter("myController", null));
        Assert.Throws<ArgumentException>(() => req.SetRouteParameter("myController", string.Empty));
        Assert.Throws<ArgumentException>(() => req.SetRouteParameter("myController", "  "));
        Assert.Throws<ArgumentException>(() => req.SetRouteParameter("myController", "{fuckParameter}", false));
        Assert.Throws<UriFormatException>(() => req.Resource);
    }

    [Fact]
    public void SetQueryParametersTest()
    {
        var req = new RestRequest("api/demo/1/devices?myarg1=1&myarg2=2&myarg3=value%209&");
            
        req.SetQueryParameter("myarg1", "110")
            .SetQueryParameter("myarg2", "220")
            // acts, this is a bad parameter which will be ignored.
            .SetQueryParameter("{custom}", "custom value");
            
        // asserts
        Assert.True(req.HasParameter("myarg1", ParameterType.Query));
        Assert.True(req.HasParameter("myarg2", ParameterType.Query));
        Assert.False(req.HasParameter("{custom}", ParameterType.Query));
        Assert.Equal("/api/demo/1/devices?myarg1=110&myarg2=220&myarg3=value%209", req.Resource);
    }

    [Fact]
    public void RemoveQueryParameterTest()
    {
        var req = new RestRequest("api/demo");

        // acts
        req.SetQueryParameter("arg1", "value1")
            .SetQueryParameter("arg2", "value2");

        // asserts
        Assert.Equal("/api/demo?arg1=value1&arg2=value2", req.Resource);

        // acts
        req.RemoveQueryParameter("arg1");

        // asserts
        Assert.Equal("/api/demo?arg2=value2", req.Resource);

        // acts
        req.SetQueryParameter("arg1", "value1");

        // asserts
        Assert.Equal("/api/demo?arg2=value2&arg1=value1", req.Resource);

        // acts
        req.RemoveQueryParameter("arg1")
            .RemoveQueryParameter("arg2");

        // asserts
        Assert.Equal("/api/demo", req.Resource);
    }

    [Fact]
    public void RemoveQueryParameterWithPlaceholdersTest()
    {
        var req = new RestRequest("api/demo?arg1={par1}");

        // asserts
        Assert.True(req.HasParameter("par1", ParameterType.QueryPlaceHolder));
        Assert.Equal("/api/demo?arg1=", req.Resource);

        // acts (parameter name is not case-sensitive)
        req.RemoveQueryParameter("ARG1");

        // asserts (placeholder was removed 'cause the query parameter associated was removed)
        Assert.False(req.HasParameter("par1", ParameterType.QueryPlaceHolder));
        Assert.Equal("/api/demo", req.Resource);
    }

    [Fact]
    public void SetBadQueryParametersTest()
    {
        var req = new RestRequest("api/demo/1/devices?myarg1=1");

        // asserts
        Assert.Throws<ArgumentException>(() => req.SetQueryParameter("myarg1", "{fuckValue}", false));
        Assert.Equal("/api/demo/1/devices?myarg1=1", req.Resource);
    }

    [Fact]
    public void SetRouteAndQueryParametersTest()
    {
        var req = new RestRequest("api/{controller}/1/{property}?myarg1=1&myarg2=2&myarg3=value%209");

        req.SetRouteParameter("controller", "demo")
            .SetRouteParameter("property", "devices");

        var routeBuilt0 = req.Resource;
        Assert.Equal("/api/demo/1/devices?myarg1=1&myarg2=2&myarg3=value%209", routeBuilt0);

        req.SetQueryParameter("myarg1", "110")
            .SetQueryParameter("myarg2", "220");

        var routeBuilt1 = req.Resource;
        Assert.Equal("/api/demo/1/devices?myarg1=110&myarg2=220&myarg3=value%209", routeBuilt1);
    }

    [Fact]
    public void SetQueryPlaceHoldersTest()
    {
        var req = new RestRequest("/api/demo/1/devices?arg1=1&arg2={arg0}&arg3=200&arg4={arg0}");

        req.SetQueryPlaceHolder("arg0", "myValue");
        var resource0 = req.Resource;

        Assert.Equal("/api/demo/1/devices?arg1=1&arg2=myValue&arg3=200&arg4=myValue", resource0);
    }

    [Fact]
    public void SetBadQueryPlaceHoldersTest()
    {
        var req = new RestRequest("/api/demo/1/devices?arg1=1&arg2={arg0}&arg3=200&arg4={arg0}");

        Assert.Throws<ArgumentException>(() => req.SetQueryPlaceHolder("arg0", "{fuckValue}", false));
    }

    [Fact]
    public void SetInvalidQueryPlaceholderParameterTest()
    {
        var req = new RestRequest("/api/demo/1/devices?arg1=1&arg2=val2&arg3=200&arg4=val4&{myargN}={myargNValue}");

        Assert.False(req.HasParameter("{myargN}", ParameterType.Query));
        Assert.False(req.HasParameter("myargNValue", ParameterType.QueryPlaceHolder));

        Assert.Equal("/api/demo/1/devices?arg1=1&arg2=val2&arg3=200&arg4=val4", req.Resource);
    }

    [Fact]
    public void InvalidResourceValue()
    {
        var req = new RestRequest("/api/demo/{docId}/devices?myarg={myargNValue}");

        // asserts (contains a query place holder not set yet !)
        Assert.Throws<UriFormatException>(() => req.Resource);
    }

    [Fact]
    public void VerifyResource()
    {
        var req = new RestRequest("/api/demo/{docId}/devices?{arg1}={arg1_value}&myarg={myargNValue}");

        Assert.True(req.HasParameter("docId", ParameterType.Route));
        Assert.False(req.HasParameter("arg1", ParameterType.Query));
        Assert.False(req.HasParameter("arg1_value", ParameterType.QueryPlaceHolder));
        Assert.True(req.HasParameter("myarg", ParameterType.Query));
        Assert.True(req.HasParameter("myargNValue", ParameterType.QueryPlaceHolder));
            
        // asserts (contains a query place holder not set yet !)
        Assert.Throws<UriFormatException>(() => req.Resource);

        // acts (set route parameter)
        req.SetRouteParameter("docId", "system");

        // asserts (It doesn't throw an exception)
        Assert.NotNull(req.Resource);

        Assert.Equal("/api/demo/system/devices?myarg=", req.Resource);

        // acts (sets a placeholder parameter)
        req.SetQueryPlaceHolder("myargNValue", "myArgValue01");

        // asserts (after setting the query palce holder parameter)
        Assert.Equal("/api/demo/system/devices?myarg=myArgValue01", req.Resource);
            
        // acts (add new query parameter)
        req.SetQueryParameter("myCustomArg", "1");

        // asserts the new parameter
        Assert.Equal("/api/demo/system/devices?myarg=myArgValue01&myCustomArg=1", req.Resource);
    }
}
