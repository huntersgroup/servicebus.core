using System.Text;
using Newtonsoft.Json;
using ServiceBus.Core.Formatters;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Formatters;

public class JsonDataFormatterTest
{
    [Theory]
    [InlineData("This is your message sent: demo")]
    [InlineData("This is a custom message sent by users!!!")]
    public void EncodeDecodeTest(string input)
    {
        var formatter = new JsonDataFormatter(new JsonSerializerSettings(), Encoding.UTF8);

        var encoded = formatter.Encode(input);

        var decoded = formatter.Decode<string>(encoded);

        Assert.Equal(input, decoded);
    }

    [Fact]
    public void ComplexEncodeDecodeTest()
    {
        var formatter = new JsonDataFormatter(new JsonSerializerSettings(), Encoding.UTF8);

        var input = new
        {
            Id = 10, Code = "myCode", Date = DateTime.UtcNow,
            Children = new[]
            {
                new { Id = 100, Code = "child01", Date = DateTime.UtcNow },
                new { Id = 101, Code = "child01", Date = DateTime.UtcNow },
                new { Id = 102, Code = "child01", Date = DateTime.UtcNow }
            }
        };

        var encoded = formatter.Encode(input);

        var decoded = formatter.Decode<DemoCls>(encoded);

        Assert.NotNull(decoded);
    }

    public class DemoCls
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public DateTime Date { get; set; }

        public IEnumerable<DemoCls> Children { get; set; }
    }
}
