namespace ServiceBus.Core.Rabbit.Test.Clients.Publishers;

public interface IMessageV2<out TData> : IMessage<TData>
{
    int Counter { get; set; }
}


public class MessageV2<TData> : Message<TData>, IMessageV2<TData>
{
    public int Counter { get; set; }
}
