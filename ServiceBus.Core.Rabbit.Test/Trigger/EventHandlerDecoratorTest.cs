using ServiceBus.Core.Events;
using ServiceBus.Core.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace ServiceBus.Core.Rabbit.Test.Trigger;

public class EventHandlerDecoratorTest
{
    private readonly ITestOutputHelper output;

    public EventHandlerDecoratorTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    private event EventHandler<MessageDeliverEventArgs<string>> Demo;

    [Fact]
    public void TestOnEvent()
    {
        this.Demo = this.TriggerOnReceived1;
        this.Demo += this.TriggerOnReceived1;

        Assert.Equal(2, this.Demo.Count());

        this.Demo += null;

        Assert.Equal(2, this.Demo.Count());

        this.Demo -= null;

        Assert.Equal(2, this.Demo.Count());

        this.Demo += this.TriggerOnReceived2;

        Assert.Equal(3, this.Demo.Count());

        this.Demo = null;

        Assert.Equal(0, this.Demo.Count());

        this.Demo += this.TriggerOnReceived1;

        Assert.Equal(1, this.Demo.Count());

        this.Demo += null;

        Assert.Equal(1, this.Demo.Count());
    }

    private void TriggerOnReceived1(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnReceived1)}, data: {e.Message.Data}");
    }

    private void TriggerOnReceived2(object sender, MessageDeliverEventArgs<string> e)
    {
        this.output.WriteLine($"method: {nameof(this.TriggerOnReceived2)}, data: {e.Message.Data}");
    }
}
