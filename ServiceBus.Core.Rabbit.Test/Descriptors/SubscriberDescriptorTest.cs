using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class SubscriberDescriptorTest
{
    [Fact]
    public void VerifyStatusTest()
    {
        // arrange
        var descriptor = new SubscriberDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = Guid.NewGuid().ToString("N") }
        };

        // assert (no exception is thrown!)
        descriptor.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusWithExceptionsTest()
    {
        // arrange
        var rightDescriptor = new SubscriberDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = Guid.NewGuid().ToString("N") }
        };

        var descriptor = new SubscriberDescriptor
        {
            Address = rightDescriptor.Address,
            Credentials = rightDescriptor.Credentials
        };

        // asserts
        Assert.Throws<InvalidDescriptorException>(() => descriptor.VerifyStatus());
    }
}
