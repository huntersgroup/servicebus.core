using ServiceBus.Core.Descriptors;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class ServiceDescriptorTest
{
    [Theory]
    [InlineData("a", "b", "a/b")]
    [InlineData(null, null, "/")]
    [InlineData(null, "b", "/b")]
    [InlineData("a", null, "a/")]
    public void FullName_Init_Test(string prefix, string name, string expected)
    {
        var desc0 = new ServiceDescriptor
        {
            Prefix = prefix,
            Name = name
        };

        var desc1 = new ExchangeDescriptor
        {
            Prefix = prefix,
            Name = name
        };

        var desc2 = new QueueDescriptor
        {
            Prefix = prefix,
            Name = name
        };

        Assert.Equal(expected, desc0.FullName);
        Assert.Equal(expected, desc1.FullName);
        Assert.Equal(expected, desc2.FullName);
    }
}
