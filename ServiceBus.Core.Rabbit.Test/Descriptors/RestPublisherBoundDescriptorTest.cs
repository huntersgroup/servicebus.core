using ServiceBus.Core.Clients.Publishers.Descriptors;
using ServiceBus.Core.Extensions;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class RestPublisherBoundDescriptorTest
{
    [Fact]
    public void VerifyStatus_Test()
    {
        var desc = new RestPublisherBoundDescriptor { ServiceName = "app" };

        desc.VerifyStatus();
    }

    [Fact]
    public void VerifyStatus_NoServiceName_Test()
    {
        var desc = new RestPublisherBoundDescriptor();

        Assert.Throws<ArgumentNullException>(() => desc.VerifyStatus());
    }

    [Fact]
    public void VerifyStatus_NoContentType_Test()
    {
        var desc = new RestPublisherBoundDescriptor
        {
            ServiceName = "app",
            ContextType = null
        };

        Assert.Throws<ArgumentNullException>(() => desc.VerifyStatus());
    }
}
