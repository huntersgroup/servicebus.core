using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class BinderSubscriberDescriptorTest
{
    [Fact]
    public void VerifyStatusTest()
    {
        // arrange
        var descriptor = new BinderSubscriberDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = Guid.NewGuid().ToString("N") },
            Exchange = { Name = Guid.NewGuid().ToString("N") }
        };

        // assert (no exception is thrown!)
        descriptor.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusWithExceptionsTest()
    {
        // arrange
        var rightDescriptor = new BinderSubscriberDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" },
            Queue = { Name = Guid.NewGuid().ToString("N") },
            Exchange = { Name = Guid.NewGuid().ToString("N") }
        };

        var badDescriptor = new BinderSubscriberDescriptor
        {
            Address = rightDescriptor.Address,
            Credentials = rightDescriptor.Credentials,
            Queue = rightDescriptor.Queue
        };

        // asserts (misses Exchange property)
        Assert.Throws<InvalidDescriptorException>(() => badDescriptor.VerifyStatus());

        // acts
        badDescriptor.Exchange = rightDescriptor.Exchange;

        // asserts (no exception is thrown!)
        badDescriptor.VerifyStatus();

        // acts
        badDescriptor.Queue = null;

        // asserts (misses Queue property)
        Assert.Throws<InvalidDescriptorException>(() => badDescriptor.VerifyStatus());
    }
}
