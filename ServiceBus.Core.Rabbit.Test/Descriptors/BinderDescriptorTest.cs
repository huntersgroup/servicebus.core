using ServiceBus.Core.Descriptors;
using Xunit;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class BinderDescriptorTest
{
    [Fact]
    public void BinderDescriptorStateTest()
    {
        // arrange
        var descriptor = new BinderDescriptor();

        // test
        Assert.Equal(string.Empty, descriptor.RoutingKey);
        Assert.NotNull(descriptor.Arguments);
    }
}
