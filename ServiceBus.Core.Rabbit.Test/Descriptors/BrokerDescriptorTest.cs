using ServiceBus.Core.Clients;
using ServiceBus.Core.Descriptors;
using ServiceBus.Core.Exceptions;
using ServiceBus.Core.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace ServiceBus.Core.Rabbit.Test.Descriptors;

public class BrokerDescriptorTest(ITestOutputHelper output)
{
    [Fact]
    public void VerifyStatusTest()
    {
        var descriptor = new BrokerDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" }
        };

        // assert (no exception is thrown!)
        descriptor.VerifyStatus();
    }

    [Fact]
    public void VerifyStatusWithExceptionsTest()
    {
        // arrange
        var rightDescriptor = new BrokerDescriptor
        {
            Address = new Uri("amqp://localhost:5672"),
            Credentials = new Credentials { Username = "guest", Password = "guest" }
        };

        var descriptor = new BrokerDescriptor
        {
            Credentials = rightDescriptor.Credentials
        };

        // asserts
        Assert.Throws<InvalidDescriptorException>(() => descriptor.VerifyStatus());

        // arrange
        descriptor.Address = rightDescriptor.Address;
        descriptor.Credentials = null;

        // asserts
        Assert.Throws<InvalidDescriptorException>(() => descriptor.VerifyStatus());
    }
}
