using ServiceBus.Core.Events;

namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents a basic contract for subscribing into underlying queue channel in an async way.
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IAsyncSubscriber<TData> : IAsyncBrokerClient
{
    /// <summary>
    /// Gets a delegate handler for receiving data for brokers.
    /// </summary>
    IDelegateHandler<MessageDeliverEventArgs<TData>> Received { get; }

    /// <summary>
    /// Gets a delegate handler for intercepting exception during incoming message processing.
    /// </summary>
    IDelegateHandler<MessageExceptionEventArgs> OnException { get; }

    /// <summary>
    /// Gets a delegate handler for intercepting exception when the given instance reaches the max number of retries to send a particular message.
    /// </summary>
    IDelegateHandler<RetryExceptionEventArgs<TData>> OnMaxRetryFailed { get; }
}

/// <summary>
/// Represents a basic contract for subscribing into underlying queue channel in an async way.
/// </summary>
public interface IAsyncSubscriber : IAsyncBrokerClient
{
    /// <summary>
    /// Gets a delegate handler for receiving data for brokers.
    /// </summary>
    IDelegateHandler<IMessageDecoderEventArgs> Received { get; }

    /// <summary>
    /// Gets a delegate handler for intercepting exception during incoming message processing.
    /// </summary>
    IDelegateHandler<MessageExceptionEventArgs> OnException { get; }

    /// <summary>
    /// Gets a delegate handler for intercepting exception when the given instance reaches the max number of retries to send a particular message.
    /// </summary>
    IDelegateHandler<RetryExceptionEventArgs<object>> OnMaxRetryFailed { get; }
}
