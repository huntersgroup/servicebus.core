namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents a common subscriber which receives messages from queue brokers, when an instance of this reference starts.
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IRunnableSubscriber<TData> : IAsyncSubscriber<TData>, IAsyncRunnable;

/// <summary>
/// Represents a common subscriber which receives messages from queue brokers, when an instance of this reference starts.
/// </summary>
public interface IRunnableSubscriber : IAsyncSubscriber, IAsyncRunnable;
