namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents the way to run a service in an async way.
/// </summary>
public interface IAsyncRunnable
{
    /// <summary>
    /// Gets if the current instance is running.
    /// </summary>
    bool IsRunning { get; }

    /// <summary>
    /// Starts the calling service
    /// </summary>
    /// <returns>Returns true if the calling instance is started, otherwise it means it was started before.</returns>
    Task Start();

    /// <summary>
    /// Stops the calling service.
    /// </summary>
    /// /// <returns>Returns true if the calling instance is stopped, otherwise it means it was stopped before.</returns>
    Task Stop();
}
