using ServiceBus.Core.Events;

namespace ServiceBus.Core.Clients.Subscribers;

/// <summary>
/// Represents a basic contract for subscribing into underlying queue channel.
/// </summary>
/// <typeparam name="TData"></typeparam>
[Obsolete("Use the new contract IAsyncSubscriber instead. It will be removed in the next major version.")]
public interface ISubscriber<TData> : IBrokerClient
{
    /// <summary>
    /// Gets or sets event handlers for receiving data for brokers.
    /// </summary>
    event EventHandler<MessageDeliverEventArgs<TData>> Received;

    /// <summary>
    /// Gets or sets event handlers for intercepting exception during incoming message processing.
    /// </summary>
    event EventHandler<MessageExceptionEventArgs> OnException;

    /// <summary>
    /// Gets or sets event handlers for intercepting exception when the given instance reaches the max number of retries to send a particular message.
    /// </summary>
    event EventHandler<RetryExceptionEventArgs<TData>> OnMaxRetryFailed;
}
