using ServiceBus.Core.Clients.Subscribers;

namespace ServiceBus.Core.Clients.Consumers;

/// <summary>
/// Represents a simple message consumer that doesn't subscribe in queues, but It downloads directly messages from queues.
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IConsumer<TData> : ISubscriber<TData>
{
    /// <summary>
    /// Executes the underlying actions in order to dequeue messages.
    /// </summary>
    /// <returns></returns>
    Task Execute();
}
