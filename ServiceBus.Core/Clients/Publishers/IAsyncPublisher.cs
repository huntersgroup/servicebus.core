namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// Represents a basic contract for publishing messages into underlying communication channel.
/// </summary>
public interface IAsyncPublisher : IAsyncBrokerClient
{
    /// <summary>
    /// Sends the given payload data into underlying channel.
    /// </summary>
    /// <param name="data"></param>
    void Send(object data);

    /// <summary>
    /// Sends the given message which encapsulates the payload data to send into underlying channel.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="message"></param>
    void Send<TData>(IMessage<TData> message);

    /// <summary>
    /// Sends the given payload in async way.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    Task SendAsync(object data);

    /// <summary>
    /// Sends the given payload in async way.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="message"></param>
    /// <returns></returns>
    Task SendAsync<TData>(IMessage<TData> message);
}
