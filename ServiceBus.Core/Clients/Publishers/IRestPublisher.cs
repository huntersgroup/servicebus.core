using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;

namespace ServiceBus.Core.Clients.Publishers;

/// <summary>
/// Represents a common way to send message into Rest microservice by underlying message broker.
/// </summary>
[Obsolete("Use the new contract IAsyncRestPublisher instead. It will be removed in the next major version.")]
public interface IRestPublisher : IBrokerClient
{
    /// <summary>
    /// Sends the given request into Rest microservice subscriber. 
    /// </summary>
    /// <param name="request">the request to send</param>
    /// <returns>Returns the subscriber response.</returns>
    Task<RestResponse> SendAsync(RestRequest request);

    /// <summary>
    /// Sends the given request into Rest microservice subscriber. 
    /// </summary>
    /// <typeparam name="TData">The type of response by subscriber.</typeparam>
    /// <param name="request">the request to send</param>
    /// <returns>returns the subscriber response with a typed payload.</returns>
    Task<RestResponse<TData>> SendAsync<TData>(RestRequest request);
}
