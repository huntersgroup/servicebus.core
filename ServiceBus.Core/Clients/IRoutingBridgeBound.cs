using ServiceBus.Core.Events;

namespace ServiceBus.Core.Clients;

/// <summary>
/// Represents a way to forward messages incoming from other channels.
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IRoutingBridgeBound<TData>
{
    /// <summary>
    /// Gets the handler to execute before forwarding messages.
    /// </summary>
    IDelegateHandler<MessageDeliverEventArgs<TData>> BeforeForwarding { get; }

    /// <summary>
    /// Gets the handler which captures exceptions during the forwarding process.
    /// </summary>
    IDelegateHandler<ForwardExceptionEventArg<TData>> OnForwardingException { get; }
}
