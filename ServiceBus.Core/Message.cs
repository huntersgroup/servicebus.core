namespace ServiceBus.Core;

/// <summary>
/// Represents a common message implementation for holding custom data to forward into underlying broker system.
/// </summary>
/// <typeparam name="TData"></typeparam>
public class Message<TData> : IMessage<TData>
{
    ///<inheritdoc/>
    public string Id { get; set; }

    ///<inheritdoc/>
    public string Tag { get; set; }

    ///<inheritdoc/>
    public string Route { get; set; } = string.Empty;

    ///<inheritdoc/>
    public DateTime? Created { get; set; }

    ///<inheritdoc/>
    public TData Data { get; set; }

    ///<inheritdoc/>
    public TimeSpan? Ttl { get; set; }

    ///<inheritdoc/>
    public bool? Persistent { get; set; }

    ///<inheritdoc/>
    public IDictionary<string, object> Properties { get; init; } = new Dictionary<string, object>();
}

/// <summary>
/// Represents a custom message holder for sending custom data into underlying broker message manager.
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IMessage<out TData> : IMessage
{
    /// <summary>
    /// Gets the custom data holden by this message.
    /// </summary>
    TData Data { get; }
}

/// <summary>
/// Represents a custom message holder for sending custom data into underlying broker message manager.
/// </summary>
public interface IMessage
{
    /// <summary>
    /// Gets a custom Id related to this message.
    /// </summary>
    string Id { get; }

    /// <summary>
    /// Gets an unique tag related to this message, this is an alternative for the <see cref="Id"/> property, but it could be set by publishers internally.
    /// </summary>
    string Tag { get; set; }

    /// <summary>
    /// Gets or sets a custom routing for forwarding this message.
    /// </summary>
    string Route { get; }

    /// <summary>
    /// Gets the date when this message was created.
    /// </summary>
    DateTime? Created { get; set; }
        
    /// <summary>
    /// Gets the default time-to-live for each message sent by publishers.
    /// </summary>
    TimeSpan? Ttl { get; }

    /// <summary>
    /// Gets or sets the default value for all messages (if It must be persistent or not)
    /// </summary>
    bool? Persistent { get; set; }

    /// <summary>
    /// Gets or sets the custom headers for this message.
    /// <para>
    /// This property could be used to sent additional info related to this message.
    /// </para>
    /// </summary>
    IDictionary<string, object> Properties { get; }
}
