using ServiceBus.Core.Events;

namespace ServiceBus.Core;

/// <summary>
/// Represents a way to get events for underlying broker channels to notify consumers / publishers.
/// </summary>
[Obsolete("Use the new contract IAsyncBrokerClient instead. It will be removed in the next major version.")]
public interface IBrokerClient
{
    /// <summary>
    /// Gets or sets event handlers for receiving a confirmation for messages that arrives from broker.
    /// </summary>
    event EventHandler<ChannelEventArgs> OnChannelEventFired;
}
