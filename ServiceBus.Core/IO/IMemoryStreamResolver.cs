namespace ServiceBus.Core.IO;

/// <summary>
/// Represents a common way to build a MemoryStream instances.
/// </summary>
public interface IMemoryStreamResolver
{
    /// <summary>
    /// Creates a new MemoryStream instance.
    /// </summary>
    /// <returns></returns>
    [Obsolete("use Build method instead")]
    MemoryStream Resolve();

    /// <summary>
    /// Creates a new MemoryStream initializing with the given buffer.
    /// </summary>
    /// <param name="buffer"></param>
    /// <returns></returns>
    [Obsolete("use Build method instead")]
    MemoryStream Resolve(byte[] buffer);

    /// <summary>
    /// Creates a new Stream, initializing with the given <see cref="bytes"/> array instance if it's not null.
    /// </summary>
    /// <param name="bytes"></param>
    /// <returns></returns>
    Stream Build(byte[] bytes = null);
}
