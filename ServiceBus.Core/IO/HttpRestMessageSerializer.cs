using System.Text;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;

namespace ServiceBus.Core.IO;

/// <summary>
/// 
/// </summary>
public class HttpRestMessageSerializer : IHttpRestMessageSerializer
{
    private readonly IMemoryStreamResolver streamResolver;
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamResolver"></param>
    /// <param name="encoder"></param>
    public HttpRestMessageSerializer(IMemoryStreamResolver streamResolver, Encoding encoder)
    {
        this.streamResolver = streamResolver;
        this.Encoder = encoder;
    }

    /// <inheritdoc />
    public Encoding Encoder { get; }

    /// <inheritdoc />
    public byte[] SerializeRequest(HttpRestRequest request)
    {
        using (var stream = this.streamResolver.Build())
        {
            // writing first line about http request (ex: POST /api/values HTTP/1.1)
            stream.Write(request.Method.Translate(), this.Encoder)
                .WriteSpace()
                .Write(request.Resource ?? HttpConstants.SlashValue, this.Encoder)
                .WriteSpace()
                .Write(request.Version == null ? HttpConstants.DefaultHttpVersionTarget : $"{HttpConstants.PrefixHttpVersion}{request.Version}", this.Encoder)
                .WriteNewline();

            // writing headers
            stream.WriteHeaders(request.Headers, this.Encoder)
                .WriteNewline();

            // writing payload
            if (request.Content?.Length > 0)
            {
                stream.Write(request.Content, 0, request.Content.Length);
            }

            return stream.ToArray();
        }
    }

    /// <inheritdoc />
    public byte[] SerializeResponse(HttpRestResponse response)
    {
        using (var stream = this.streamResolver.Build())
        {
            // writing first line (ex. HTTP/1.1 200 OK)
            stream.Write(response.Version == null ? HttpConstants.DefaultHttpVersionTarget : $"{HttpConstants.PrefixHttpVersion}{response.Version}", this.Encoder)
                .WriteSpace()
                .Write(response.StatusCode.ToString(), this.Encoder)
                .WriteSpace()
                .Write(string.IsNullOrWhiteSpace(response.StatusDescription) ? string.Empty : response.StatusDescription.Trim(), this.Encoder)
                .WriteNewline();

            // writing headers
            stream.WriteHeaders(response.Headers, this.Encoder)
                .WriteNewline();

            // writing payload
            if (response.Content?.Length > 0)
            {
                stream.Write(response.Content, 0, response.Content.Length);
            }

            return stream.ToArray();
        }
    }

    /// <inheritdoc />
    public HttpRestRequest DeserializeRequest(byte[] data)
    {
        var reader = new HttpServiceMessageReader(data, this.Encoder);
        var request = new HttpRestRequest();
        var isFirstLine = true;

        if (reader.ReadLine())
        {
            isFirstLine = false;

            var components = reader.CurrentLine.Split(new[] { StreamExtensions.SpaceCharValue }, StringSplitOptions.RemoveEmptyEntries);

            if (components.Length < 3)
            {
                throw new InvalidOperationException("The current message cannot be deserialized due to number of components on the first line of the current request.");
            }

            request.Method = components[0].ToHttpVerb();
            request.Version = components[components.Length - 1].Substring(5).Trim();
                
            if (components.Length == 3)
            {
                request.Resource = components[1];
            }
            else
            {
                // it's composed the resource path with eventually blank spaces.
                var parts = new StringBuilder(components[1]);

                for (var i = 2; i < components.Length - 1; i ++)
                {
                    parts.Append($" {components[i]}");
                }

                request.Resource = parts.ToString();
            }

            // headers after first raw
            while (reader.ReadLine())
            {
                reader.CurrentLine.ReadInlIneHeaders(request.Headers);
            }
        }

        if (isFirstLine || !reader.IsContentReady)
        {
            throw new InvalidOperationException("Unable to deserialize data due to there's no components into the first row, or the current reader is not in the right position for reading payload body.");
        }

        // reads the response payload.
        request.Content = reader.GetContent();

        return request;
    }

    /// <inheritdoc />
    public HttpRestRequest DeserializeRequest(ReadOnlyMemory<byte> data)
    {
        return this.DeserializeRequest(data.ToArray());
    }

    /// <inheritdoc />
    public HttpRestResponse DeserializeResponse(byte[] data)
    {
        var reader = new HttpServiceMessageReader(data, this.Encoder);
        var response = new HttpRestResponse();
        var isFirstLine = true;

        if (reader.ReadLine())
        {
            isFirstLine = false;

            var components = reader.CurrentLine.Split(new[] { StreamExtensions.SpaceCharValue }, StringSplitOptions.RemoveEmptyEntries);

            if (components.Length < 2)
            {
                throw new InvalidOperationException("The current message cannot be deserialized due to number of components on the first line of the current response.");
            }

            response.Version = components[0].Substring(5).Trim();
            response.StatusCode = int.Parse(components[1]);

            if (components.Length > 2)
            {
                var statusDescription = new StringBuilder(components[2].Trim());

                for (var i = 3; i < components.Length; i++)
                {
                    statusDescription.Append(StreamExtensions.SpaceCharValue);
                    statusDescription.Append(components[i]);
                }

                response.StatusDescription = statusDescription.ToString();
            }

            // headers after first raw
            while (reader.ReadLine())
            {
                reader.CurrentLine.ReadInlIneHeaders(response.Headers);
            }
        }

        if (isFirstLine || !reader.IsContentReady)
        {
            throw new InvalidOperationException("Unable to deserialize data due to there's no components into the first row, or the current reader is not in the right position for reading payload body.");
        }

        // reads the response payload.
        response.Content = reader.GetContent();

        return response;
    }

    /// <inheritdoc />
    public HttpRestResponse DeserializeResponse(ReadOnlyMemory<byte> data)
    {
        return this.DeserializeResponse(data.ToArray());
    }
}
