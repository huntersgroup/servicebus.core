using System.Text;
using ServiceBus.Core.Http.Request;
using ServiceBus.Core.Http.Response;

namespace ServiceBus.Core.IO;

/// <summary>
/// Represents a common contract for serializing http requests and responses.
/// </summary>
public interface IHttpRestMessageSerializer
{
    /// <summary>
    /// Gets the current encoding for this serializer
    /// </summary>
    Encoding Encoder { get; }

    /// <summary>
    /// Serializes the given request
    /// </summary>
    /// <param name="request">the request to serialize</param>
    /// <returns></returns>
    byte[] SerializeRequest(HttpRestRequest request);

    /// <summary>
    /// Serializes the given response
    /// </summary>
    /// <param name="response">the response to serialize</param>
    /// <returns></returns>
    byte[] SerializeResponse(HttpRestResponse response);

    /// <summary>
    /// Deserialize the given data into <see cref="HttpRestRequest"/> type.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    HttpRestRequest DeserializeRequest(byte[] data);

    /// <summary>
    /// Deserialize the given data into <see cref="HttpRestRequest"/> type.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    HttpRestRequest DeserializeRequest(System.ReadOnlyMemory<byte> data);

    /// <summary>
    /// Deserialize the given data into <see cref="HttpRestResponse"/> type.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    HttpRestResponse DeserializeResponse(byte[] data);

    /// <summary>
    /// Deserialize the given data into <see cref="HttpRestResponse"/> type.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    HttpRestResponse DeserializeResponse(ReadOnlyMemory<byte> data);
}
