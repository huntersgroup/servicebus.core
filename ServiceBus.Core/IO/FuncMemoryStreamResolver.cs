namespace ServiceBus.Core.IO;

/// <summary>
/// 
/// </summary>
public class FuncMemoryStreamResolver : IMemoryStreamResolver
{
    private readonly Func<MemoryStream> resolverFunc;
    private readonly Func<byte[], MemoryStream> resolverWithBuffer;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="resolverFunc"></param>
    /// <param name="resolverWithBuffer"></param>
    public FuncMemoryStreamResolver(Func<MemoryStream> resolverFunc, Func<byte[], MemoryStream> resolverWithBuffer)
    {
        this.resolverFunc = resolverFunc;
        this.resolverWithBuffer = resolverWithBuffer;
    }

    /// <inheritdoc />
    public MemoryStream Resolve()
    {
        return this.resolverFunc.Invoke();
    }

    /// <inheritdoc />
    public MemoryStream Resolve(byte[] buffer)
    {
        return this.resolverWithBuffer(buffer);
    }

    /// <inheritdoc />
    public Stream Build(byte[] bytes = null)
    {
        return bytes == null ? this.resolverFunc() : this.resolverWithBuffer(bytes);
    }
}
