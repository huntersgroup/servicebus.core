namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class HttpRestMessageExtensions
{
    /// <summary>
    /// Get the first header value.
    /// </summary>
    /// <param name="headers"></param>
    /// <param name="header"></param>
    /// <returns></returns>
    public static string GetFirstHeaderValue(this IDictionary<string, List<string>> headers, string header)
    {
        return headers.TryGetValue(header, out var headerValues) ? headerValues.FirstOrDefault() : null;
    }
}
