namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class HttpConstants
{
    /// <summary>
    /// 
    /// </summary>
    public const string DnsLocalhost = "localhost";

    /// <summary>
    /// 
    /// </summary>
    public const int DefaultPort = 80;

    /// <summary>
    /// 
    /// </summary>
    public const string SlashValue = "/";

    /// <summary>
    /// 
    /// </summary>
    public const string SlashEncoded = "%2F";

    /// <summary>
    /// 
    /// </summary>
    public const char QuestionChar = '?';

    /// <summary>
    /// 
    /// </summary>
    public const string DefaultSchema = "http";

    /// <summary>
    /// 
    /// </summary>
    public const string PrefixHttpVersion = "HTTP/";

    /// <summary>
    /// 
    /// </summary>
    public const string DefaultHttpVersion = "1.1";

    /// <summary>
    /// 
    /// </summary>
    public const string DefaultHttpVersionTarget = "HTTP/1.1";
}
