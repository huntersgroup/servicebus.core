using System.Text;
using System.Text.RegularExpressions;
using ServiceBus.Core.Http;
using ServiceBus.Core.Http.Request;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class CommonUriExtensions
{
    //private static readonly Regex PlaceHolderParameter = new Regex(@"(?<=\{)[^}]*\D(?=\})");
    private static readonly Regex PlaceHolderParameter = new Regex(@"(?<=\{)[^}]*(?=\})");
    private const string EqualsString = "=";
    private const string LogicalAndString = "&";
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="resource"></param>
    /// <returns></returns>
    public static UriTemplate AsUriTemplate(this string resource)
    {
        if (string.IsNullOrWhiteSpace(resource))
        {
            throw new InvalidOperationException("Resource address cannot be null or empty");
        }

        if (!resource.StartsWith(HttpConstants.SlashValue))
        {
            resource = $"{HttpConstants.SlashValue}{resource}";
        }

        var query = string.Empty;
        var path = resource;

        var questionIndex = resource.IndexOf(HttpConstants.QuestionChar);

        if (questionIndex > 0)
        {
            path = resource.Substring(0, questionIndex);
            query = resource.Substring(questionIndex);
        }

        return new UriTemplate
        {
            Path = path,
            Query = query
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="hostname"></param>
    /// <returns></returns>
    public static Uri BuildUri(this string resource, string hostname)
    {
        var template = resource.AsUriTemplate();

        hostname ??= HttpConstants.DnsLocalhost;

        var uriBuilder = new UriBuilder(HttpConstants.DefaultSchema, hostname, HttpConstants.DefaultPort, template.Path, template.Query);
            
        return uriBuilder.Uri;
    }

    /// <summary>
    /// Analyzes the given path, decoding each routing parameter present.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string UnEscapeUriString(this string path)
    {
        var segments = path.Split(new []{ HttpConstants.SlashValue }, StringSplitOptions.RemoveEmptyEntries);
        var buffer = new StringBuilder();
            
        foreach (var segment in segments)
        {
            buffer.Append(HttpConstants.SlashValue);

            var decoded = Uri.UnescapeDataString(segment);
                
            if (decoded.Contains(HttpConstants.SlashValue))
            {
                decoded = decoded.Replace(HttpConstants.SlashValue, HttpConstants.SlashEncoded);
            }

            buffer.Append(decoded);
        }

        return buffer.ToString();
    }

    /// <summary>
    /// Gets all routing parameters for the given <see cref="UriTemplate"/> instance.
    /// </summary>
    /// <param name="uri"></param>
    /// <returns></returns>
    public static IEnumerable<RequestParameter> GetRouteParams(this UriTemplate uri)
    {
        return PlaceHolderParameter.Matches(uri.Path)
            .Cast<Match>()
            .Select(match => new RequestParameter
            {
                Placeholder = $"{{{ match.Value }}}",
                Name = match.Value,
                Type = ParameterType.Route,
                Encode = false
            })
            .ToList();
    }

    /// <summary>
    /// Gets all query place holders parameters present into the given query template.
    /// </summary>
    /// <param name="uri"></param>
    /// <returns></returns>
    public static IEnumerable<RequestParameter> GetQueryPlaceHolders(this UriTemplate uri)
    {
        return PlaceHolderParameter.Matches(uri.Query)
            .Cast<Match>()
            .GroupBy(match => match.Value)
            .Select(match => new RequestParameter
            {
                Placeholder = $"{{{ match.Key }}}",
                Name = match.Key,
                Type = ParameterType.QueryPlaceHolder,
                Encode = false
            })
            .ToList();
    }

    /// <summary>
    /// Gets all query place holders parameters present into the given query parameters collection.
    /// </summary>
    /// <param name="queryParameters"></param>
    /// <returns></returns>
    public static IEnumerable<RequestParameter> GetQueryPlaceHolders(this IEnumerable<RequestParameter> queryParameters)
    {
        return queryParameters.Where(parameter =>
                parameter.Type == ParameterType.Query && parameter.Value.HasPlaceHolders())
            .GroupBy(parameter => parameter.Value)
            .Select(parameter => new RequestParameter
            {
                Placeholder = parameter.Key,
                Name = PlaceHolderParameter.Match(parameter.Key).Value,
                Type = ParameterType.QueryPlaceHolder,
                Encode = false
            })
            .ToList();
    }

    /// <summary>
    /// Gets all query parameters for the given <see cref="UriTemplate"/> instance.
    /// </summary>
    /// <param name="uri"></param>
    /// <returns></returns>
    public static IEnumerable<RequestParameter> GetQueryParams(this UriTemplate uri)
    {
        var list = new List<RequestParameter>();

        if (!string.IsNullOrWhiteSpace(uri.Query))
        {
            var query = uri.Query.Substring(1);

            var pars = query.Split(new[] { LogicalAndString }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var par in pars)
            {
                var parts = par.Split(new[] { EqualsString }, StringSplitOptions.RemoveEmptyEntries);
                    
                // the parameter name cannot be a placeholder, instead the value surely.
                if (parts.Length == 2 && !parts[0].HasPlaceHolders())
                {
                    list.Add(new RequestParameter
                    {
                        Name = parts[0],
                        Value = parts[1],
                        Type = ParameterType.Query,
                        Encode = false
                    });
                }
            }
        }

        return list;
    }

    /// <summary>
    /// Formats a query string using the given parameters, replacing eventually placeholders using the given parameter.
    /// </summary>
    /// <param name="parameters">All parameters for this query string representation</param>
    /// <param name="placeHolders">All placeholders for </param>
    /// <returns></returns>
    public static string FormatQueryString(this IEnumerable<RequestParameter> parameters, IEnumerable<RequestParameter> placeHolders)
    {
        var pars = parameters.Where(parameter => parameter.Type == ParameterType.Query)
            .ToList();

        var plHolders = placeHolders.Where(parameter => parameter.Type == ParameterType.QueryPlaceHolder)
            .ToList();

        var list = new List<string>();

        foreach (var par in pars)
        {
            var placeholder = plHolders.FirstOrDefault(parameter =>
                parameter.Placeholder.Equals(par.Value, StringComparison.OrdinalIgnoreCase));

            list.Add(placeholder != null ? $"{par.Name}={placeholder}" : par.ToString());
        }

        return string.Join(LogicalAndString, list);
    }

    /// <summary>
    /// Takes the path and rebuild it using the given parameters replacing all placeholders.
    /// </summary>
    /// <param name="parameters"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string FormatStringPath(this IEnumerable<RequestParameter> parameters, string path)
    {
        var buffer = new StringBuilder(path);

        var list = parameters.Where(parameter => parameter.Type == ParameterType.Route)
            .ToList();

        foreach (var parameter in list)
        {
            buffer.Replace(parameter.Placeholder, parameter.Value ?? string.Empty);
        }

        return buffer.ToString();
    }

    /// <summary>
    /// Indicates if the given input contains curly brackets, so it's considered like a placeholder parameter.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static bool HasPlaceHolders(this string input)
    {
        return PlaceHolderParameter.IsMatch(input);
    }
}
