namespace ServiceBus.Core.Extensions;

/// <summary>
/// Common methods extensions.
/// </summary>
public static class CommonExtensions
{
    /// <summary>
    /// Verify if the given input is null, if it's true an exception will be thrown, otherwise returns the given input.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="instance"></param>
    /// <param name="paramName"></param>
    /// <returns></returns>
    public static TData ThrowIfNull<TData>(this TData instance, string paramName)
        where TData : class
    {
        if (instance == null)
        {
            throw new ArgumentNullException(paramName, "The given parameter cannot be null.");
        }

        return instance;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="instance"></param>
    /// <param name="action"></param>
    /// <returns></returns>
    public static TData ExecuteCustomAction<TData>(this TData instance, Action action)
        where TData : class
    {
        action();

        return instance;
    }
}
