namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class HttpHeaders
{
    /// <summary>
    /// 
    /// </summary>
    public const string ContentLength = "CONTENT-LENGTH";

    /// <summary>
    /// 
    /// </summary>
    public const string Server = "ServiceBus.AspNetCore";

    /// <summary>
    /// 
    /// </summary>
    public const string ContentType = "Content-Type";

    /// <summary>
    /// 
    /// </summary>
    public const string JsonContentType = "application/json";

    /// <summary>
    /// 
    /// </summary>
    public const string TextPlainContentType = "text/plain";
}
