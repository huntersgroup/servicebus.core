using System.Text;

namespace ServiceBus.Core.Extensions;

/// <summary>
/// 
/// </summary>
public static class StreamExtensions
{
    /// <summary>
    /// 
    /// </summary>
    internal const byte SpaceByteValue = (byte)SpaceCharValue;

    /// <summary>
    /// 
    /// </summary>
    internal const char SpaceCharValue = ' ';

    /// <summary>
    /// 
    /// </summary>
    internal const byte ColonByteValue = (byte)ColonCharValue;

    /// <summary>
    /// 
    /// </summary>
    internal const char ColonCharValue = ':';

    /// <summary>
    /// 
    /// </summary>
    internal const string InvalidSequenceOnHeader = "/n";

    /// <summary>
    /// 
    /// </summary>
    internal static byte NewlineChar = (byte)'\n';

    /// <summary>
    /// 
    /// </summary>
    internal static readonly List<string> CommonHttpHeaders = [HttpHeaders.ContentLength];

    /// <summary>
    /// Gets the byte array read from the given stream.
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public static byte[] ToArray(this Stream stream)
    {
        if (stream == null || !stream.CanRead || !stream.CanSeek)
        {
            return null;
        }

        stream.Seek(0, SeekOrigin.Begin);

        var buffer = new byte[stream.Length];

        stream.Read(buffer, 0, buffer.Length);
        
        return buffer;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public static Stream WriteNewline(this Stream stream)
    {
        stream.WriteByte(NewlineChar);

        return stream;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public static Stream WriteSpace(this Stream stream)
    {
        stream.WriteByte(SpaceByteValue);

        return stream;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Stream Write(this Stream stream, byte value)
    {
        stream.WriteByte(value);

        return stream;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="encoder"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    public static Stream Write(this Stream stream, string text, Encoding encoder)
    {
        var buffer = encoder.GetBytes(text);
        stream.Write(buffer, 0, buffer.Length);

        return stream;
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="headers"></param>
    /// <param name="encoder"></param>
    /// <returns></returns>
    internal static Stream WriteHeaders(this Stream stream, IDictionary<string, List<string>> headers, Encoding encoder)
    {
        var allHeaders = headers.Where(pair => !CommonHttpHeaders.Contains(pair.Key, StringComparer.OrdinalIgnoreCase))
            .ToList();

        foreach (var header in allHeaders)
        {
            if (header.Value == null)
            {
                stream.WriteSingleHeader(header.Key, string.Empty, encoder);
            }
            else
            {
                foreach (var currentValue in header.Value)
                {
                    stream.WriteSingleHeader(header.Key, currentValue, encoder);
                }
            }
        }

        return stream;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="text"></param>
    /// <param name="headers"></param>
    internal static void ReadInlIneHeaders(this string text, IDictionary<string, List<string>> headers)
    {
        var colonIndex = text.IndexOf(ColonCharValue);

        if (colonIndex == -1)
        {
            throw new InvalidOperationException($"Invalid text header, value: {text}");
        }

        var name = text.Substring(0, colonIndex).Trim();
        var value = text.Substring(colonIndex + 1).TrimStart();

        if (headers.ContainsKey(name))
        {
            headers[name].Add(value);
        }
        else
        {
            headers[name] = new List<string> { value };
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="name"></param>
    /// <param name="value"></param>
    /// <param name="encoder"></param>
    private static void WriteSingleHeader(this Stream stream, string name, string value, Encoding encoder)
    {
        var sanitizedValue = value.Sanitize();

        stream.Write(name, encoder)
            .Write(ColonByteValue)
            .WriteSpace()
            .Write(sanitizedValue, encoder)
            .WriteNewline();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private static string Sanitize(this string value)
    {
        return value.Contains(InvalidSequenceOnHeader) ? value.Replace(InvalidSequenceOnHeader, string.Empty) : value;
    }
}
