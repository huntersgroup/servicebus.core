namespace ServiceBus.Core.Http.Response;

/// <summary>
/// Represents status of the current response.
/// </summary>
public enum ResponseStatus
{
    /// <summary>
    /// Default status.
    /// </summary>
    None,
    /// <summary>
    /// Response was complete.
    /// </summary>
    Completed,
    /// <summary>
    /// Something was happened during processing response.
    /// </summary>
    Error,
    /// <summary>
    /// A timeout was occurred.
    /// </summary>
    TimedOut,
    /// <summary>
    /// An aborted request was done.
    /// </summary>
    Aborted
}
