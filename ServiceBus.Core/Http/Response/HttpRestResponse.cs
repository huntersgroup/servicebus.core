namespace ServiceBus.Core.Http.Response;

/// <summary>
/// 
/// </summary>
public class HttpRestResponse : HttpRestMessage
{
    /// <summary>
    /// Gets or sets the status code.
    /// </summary>
    /// <value>
    /// The status code.
    /// </value>
    public int StatusCode { get; set; }

    /// <summary>
    /// Gets or sets the status description.
    /// </summary>
    /// <value>
    /// The status description.
    /// </value>
    public string StatusDescription { get; set; }
}
