using System.Net;

namespace ServiceBus.Core.Http.Response;

/// <summary>
/// Represents a common rest microservice response, sent by underlying esb channel.
/// </summary>
public class RestResponse
{
    private const HttpStatusCode LastGoodStatusCode = (HttpStatusCode)299;

    /// <summary>
    /// 
    /// </summary>
    public RestResponse()
    {
        this.Headers = new Dictionary<string, List<string>>();
    }

    /// <summary>
    /// 
    /// </summary>
    public HttpStatusCode StatusCode { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string StatusDescription { get; set; }

    /// <summary>
    /// Gets or sets the current response status.
    /// </summary>
    public ResponseStatus ResponseStatus { get; set; }

    /// <summary>
    /// Gets or sets the original response from underlying stream channel.
    /// </summary>
    public byte[] RawBytes { get; set; }

    /// <summary>
    /// Gets or sets the string representation of response content.
    /// </summary>
    public string Content { get; set; }

    /// <summary>
    /// Gets a boolean value indicating if the current response was processed correctly.
    /// </summary>
    public bool IsSuccessful
    {
        get
        {
            if (this.StatusCode >= HttpStatusCode.OK && this.StatusCode <= LastGoodStatusCode)
            {
                return this.ResponseStatus == ResponseStatus.Completed;
            }

            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public IDictionary<string, List<string>> Headers { get; }

    /// <summary>
    /// Gets or sets an exception thrown by client side.
    /// </summary>
    public Exception Exception { get; set; }
}

/// <summary>
/// Represents a common rest micro service response, sent by underlying esb channel.
/// </summary>
/// <typeparam name="TResult"></typeparam>
public class RestResponse<TResult> : RestResponse
{
    /// <summary>
    /// 
    /// </summary>
    public TResult Data { get; set; }
}
