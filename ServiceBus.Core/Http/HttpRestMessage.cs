namespace ServiceBus.Core.Http;

/// <summary>
/// 
/// </summary>
public class HttpRestMessage
{
    /// <summary>
    /// The version
    /// </summary>
    public string Version { get; set; }

    /// <summary>
    /// The headers
    /// </summary>
    public Dictionary<string, List<string>> Headers { get; } = new();

    /// <summary>
    /// The content
    /// </summary>
    public byte[] Content { get; set; }
}
