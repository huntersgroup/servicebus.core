namespace ServiceBus.Core.Http.Request;

/// <summary>
/// Represents a service request message
/// </summary>
public class HttpRestRequest : HttpRestMessage
{
    /// <summary>
    /// Gets or sets the Http verb for this request
    /// </summary>
    public HttpMethods Method { get; set; }

    /// <summary>
    /// Gets or sets the partial Resource related to this request.
    /// </summary>
    public string Resource { get; set; }
}
