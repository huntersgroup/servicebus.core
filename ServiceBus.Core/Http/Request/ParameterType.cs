namespace ServiceBus.Core.Http.Request;

/// <summary>
/// Represents the parameter type for resources. 
/// </summary>
public enum ParameterType
{
    /// <summary>
    /// Indicates a route parameter
    /// </summary>
    Route = 0,

    /// <summary>
    /// Indicates a query parameter
    /// </summary>
    Query = 1,

    /// <summary>
    /// Indicates a query place holder parameter
    /// </summary>
    QueryPlaceHolder = 2
}
