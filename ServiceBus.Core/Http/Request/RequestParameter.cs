using System.Diagnostics;

namespace ServiceBus.Core.Http.Request;

/// <summary>
/// Represents a common parameter for service request.
/// </summary>
[DebuggerDisplay("name: {Name}, value: {Value}, encode: {Encode}, type: {Type}")]
public class RequestParameter
{
    /// <summary>
    /// Gets or sets the placeholder related to this parameter.
    /// </summary>
    public string Placeholder { get; set; }
        
    /// <summary>
    /// Gets or sets the name for this parameter.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the current value for this parameter.
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// Gets or sets the value if this current parameter needs to be encoded
    /// </summary>
    public bool Encode { get; set; }

    /// <summary>
    /// Gets or sets the current parameter type.
    /// </summary>
    public ParameterType Type { get; set; }

    /// <inheritdoc />
    public override string ToString()
    {
        var val = this.Encode ? Uri.EscapeDataString(this.Value) : this.Value;

        return this.Type == ParameterType.Query ? $"{this.Name}={val}" : val;
    }
}
