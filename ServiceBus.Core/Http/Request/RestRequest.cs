using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Http.Request;

/// <summary>
/// Represents a class which prepares Rest request to forward into underlying service bus channel.
/// </summary>
public class RestRequest
{
    private readonly List<RequestParameter> queryParameters = [];
    private readonly List<RequestParameter> routeParameters = [];
    private readonly List<RequestParameter> queryPlaceholders = [];
    private readonly UriTemplate uriTemplate;

    /// <summary>
    /// Creates a new <see cref="RestRequest"/> instance.
    /// </summary>
    public RestRequest(string resource)
    {
        if (string.IsNullOrWhiteSpace(resource))
        {
            throw new ArgumentException("The given Uri resource cannot be null or empty.", nameof(resource));
        }

        this.Id = Guid.NewGuid().ToString("D");
        this.OriginalResource = resource.Trim();

        this.uriTemplate = resource.AsUriTemplate();
            
        this.routeParameters.AddRange(this.uriTemplate.GetRouteParams());

        this.queryParameters.AddRange(this.uriTemplate.GetQueryParams());

        this.queryPlaceholders.AddRange(this.queryParameters.GetQueryPlaceHolders());
    }

    /// <summary>
    /// Gets the unique identifier for this request.
    /// </summary>
    public string Id { get; }

    /// <summary>
    /// Gets the current resource uri compiled with current arguments.
    /// </summary>
    public string Resource
    {
        get
        {
            if (this.routeParameters.Exists(parameter => string.IsNullOrWhiteSpace(parameter.Value)))
            {
                throw new UriFormatException($"The current path for this resource is invalid due to contain route parameters not set yet!, values: {string.Join(",", this.routeParameters.Select(parameter => parameter.Placeholder))}");
            }

            var path = this.routeParameters.FormatStringPath(this.uriTemplate.Path);

            var query = this.queryParameters.FormatQueryString(this.queryPlaceholders);

            if (query.HasPlaceHolders())
            {
                throw new UriFormatException($"The current query string contains placeholders values, they must be set before using this resource, value: {query}");
            }

            return string.IsNullOrWhiteSpace(query) ? path : $"{path}?{query}";
        }
    }

    /// <summary>
    /// Gets the original resource used to this instance.
    /// </summary>
    public string OriginalResource { get; }

    /// <summary>
    /// Gets or sets the current Http method for this request
    /// </summary>
    public HttpMethods Method { get; set; } = HttpMethods.Get;

    /// <summary>
    /// Gets or sets the timeout for the given request.
    /// </summary>
    public TimeSpan? Timeout { get; set; }

    /// <summary>
    /// Gets or sets the payload for this request.
    /// </summary>
    public object Payload { get; set; }

    /// <summary>
    /// Gets headers for this given request.
    /// </summary>
    public IDictionary<string, List<string>> Headers { get; } = new Dictionary<string, List<string>>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public RestRequest AddHeader(string key, string value)
    {
        if (this.Headers.TryGetValue(key, out var headerValues))
        {
            headerValues.Add(value);
        }
        else
        {
            this.Headers.Add(key, [value]);
        }

        return this;
    }
        
    /// <summary>
    /// Indicates if exists a query placeholder parameter with the given name and type.
    /// </summary>
    /// <param name="name">The parameter name</param>
    /// <param name="type">The parameter type</param>
    /// <returns></returns>
    public bool HasParameter(string name, ParameterType type)
    {
        return type switch
        {
            ParameterType.Route => this.routeParameters.Any(parameter => parameter.Name.Equals(name, StringComparison.InvariantCulture)),
            ParameterType.Query => this.queryParameters.Any(parameter => parameter.Name.Equals(name, StringComparison.OrdinalIgnoreCase)),
            ParameterType.QueryPlaceHolder => this.queryPlaceholders.Any(parameter =>
                parameter.Name.Equals(name, StringComparison.InvariantCulture)),
            _ => false
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public RestRequest AddOrOverrideHeader(string key, string value)
    {
        if (this.Headers.TryGetValue(key, out var headerValues))
        {
            headerValues.Clear();

            headerValues.Add(value);
        }
        else
        {
            this.Headers.Add(key, [value]);
        }

        return this;
    }

    /// <summary>
    /// Sets the route parameter value.
    /// </summary>
    /// <param name="name">The route parameter name</param>
    /// <param name="value">value to set</param>
    /// <param name="encode">Indicates if it will be applied an escaping for the given parameter value.</param>
    /// <returns></returns>
    public RestRequest SetRouteParameter(string name, string value, bool encode = true)
    {
        if (string.IsNullOrWhiteSpace(name) || name.HasPlaceHolders())
        {
            return this;
        }

        if (string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentException($"The value used to set value parameter cannot be null or empty.", name);
        }

        if (value.HasPlaceHolders() && !encode)
        {
            throw new ArgumentException($"The given parameter value cannot be a placeholder, if te intention is using a placeholder value, it must be encoded, value: {value}", name);
        }

        var param = this.routeParameters.FirstOrDefault(parameter =>
            parameter.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            
        if (param != null)
        {
            param.Value = value;
            param.Encode = encode;
        }

        return this;
    }

    /// <summary>
    /// Sets the query parameter value if exists, otherwise It will be added.
    /// </summary>
    /// <param name="name">The route parameter name</param>
    /// <param name="value">value to set</param>
    /// <param name="encode">Indicates if it will be applied an escaping for the given parameter value</param>
    /// <returns></returns>
    public RestRequest SetQueryParameter(string name, string value, bool encode = true)
    {
        if (string.IsNullOrWhiteSpace(name) || name.HasPlaceHolders())
        {
            return this;
        }

        if (value != null && value.HasPlaceHolders() && !encode)
        {
            throw new ArgumentException($"The given parameter value cannot be a placeholder, value: {value}", name);
        }

        var param = this.queryParameters.FirstOrDefault(parameter =>
            parameter.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

        if (param != null)
        {
            param.Value = value;
            param.Encode = encode;
        }
        else
        {
            this.queryParameters.Add(new RequestParameter
            {
                Name = name,
                Value = value,
                Encode = encode,
                Type = ParameterType.Query
            });
        }

        return this;
    }

    /// <summary>
    /// Removes the given query parameter from this request.
    /// </summary>
    /// <param name="name">Parameter name</param>
    /// <returns></returns>
    public RestRequest RemoveQueryParameter(string name)
    {
        var param = this.queryParameters.FirstOrDefault(parameter =>
            parameter.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

        if (param != null)
        {
            if (param.Value.HasPlaceHolders())
            {
                this.queryPlaceholders.RemoveAll(parameter =>
                    parameter.Placeholder.Equals(param.Value, StringComparison.InvariantCulture));
            }

            this.queryParameters.Remove(param);
        }

        return this;
    }

    /// <summary>
    /// Sets all placeholders which matches with the given name, and sets the respective value.
    /// <para>
    /// The parameter name is related to value for any query string parameter:
    /// </para>
    /// <example>
    /// The {arg1} is a placeholder parameter value:
    /// <code>
    /// var request = new RestRequest("/api?par1=<b>{arg1}</b>");<br></br>
    /// 
    /// request.SetQueryPlaceHolder("arg1", "myvalue");<br></br><br/>
    ///
    /// // this prints: /api?par1=myvalue<br></br>
    /// 
    /// Console.WriteLine(Request.Resource);<br></br>
    /// 
    /// </code>
    /// </example>
    /// </summary>
    /// <param name="name">Name of placeholder parameter</param>
    /// <param name="value">The value which replaces placeholder parameter</param>
    /// <param name="encode">Indicates if the given parameter must be encoded</param>
    /// <returns></returns>
    public RestRequest SetQueryPlaceHolder(string name, string value, bool encode = true)
    {
        if (string.IsNullOrWhiteSpace(name) || name.HasPlaceHolders())
        {
            return this;
        }

        if (value != null && value.HasPlaceHolders() && !encode)
        {
            throw new ArgumentException($"The given parameter value cannot be a placeholder, value: {value}", name);
        }

        var param = this.queryPlaceholders.FirstOrDefault(parameter =>
            parameter.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

        if (param != null)
        {
            param.Value = value;
            param.Encode = encode;
        }

        return this;
    }
}
