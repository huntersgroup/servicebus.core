namespace ServiceBus.Core.Http;

/// <summary>
/// Represents all http verbs for http request.
/// </summary>
public enum HttpMethods
{
    /// <summary>
    /// 
    /// </summary>
    Get = 0,

    /// <summary>
    /// 
    /// </summary>
    Post = 1,

    /// <summary>
    /// 
    /// </summary>
    Put = 2,

    /// <summary>
    /// 
    /// </summary>
    Delete = 3,

    /// <summary>
    /// 
    /// </summary>
    Head = 5,

    /// <summary>
    /// 
    /// </summary>
    Patch = 6
}
