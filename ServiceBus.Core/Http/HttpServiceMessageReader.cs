using System.Text;
using ServiceBus.Core.Extensions;

namespace ServiceBus.Core.Http;

/// <summary>
/// Represents a HttpRestMessage reader
/// </summary>
public class HttpServiceMessageReader
{
    private int lineStart;
    private int lineEnd = -1;
    private bool readLines;
    private readonly byte[] data;
    private readonly int length;
    private readonly Encoding encoder;
    private static readonly byte[] EmptyByteArray = [];

    /// <summary>
    /// Creates a new <see cref="HttpServiceMessageReader"/> instance.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="encoder"></param>
    public HttpServiceMessageReader(byte[] data, Encoding encoder)
    {
        this.data = data;
        this.length = data.Length;
        this.encoder = encoder;
    }

    /// <summary>
    /// Gets the current line read from underlying stream.
    /// </summary>
    public string CurrentLine { get; private set; }

    /// <summary>
    /// Gets a boolean value indicating the current stream position is reading content body
    /// </summary>
    public bool IsContentReady { get; private set; }

    /// <summary>
    /// Reads a new line from underlying stream.
    /// </summary>
    /// <returns>Returns a true value if a new line was read</returns>
    public bool ReadLine()
    {
        if (this.IsContentReady || this.lineStart >= this.length)
        {
            this.CurrentLine = null;
            return false;
        }

        while (++this.lineEnd < this.length)
        {
            if (this.data[this.lineEnd] == StreamExtensions.NewlineChar)
            {
                if (this.lineEnd == this.lineStart && this.readLines)
                {
                    this.IsContentReady = true;
                    this.lineStart = this.lineEnd + 1;

                    this.CurrentLine = null;
                    return false;
                }

                var text = this.encoder.GetString(this.data, this.lineStart, this.lineEnd - this.lineStart);
                this.lineStart = this.lineEnd + 1;
                this.readLines = true;

                this.CurrentLine = text;
                return true;
            }
        }

        this.CurrentLine = null;

        return false;
    }

    /// <summary>
    /// Gets the content.
    /// </summary>
    /// <returns></returns>
    public byte[] GetContent()
    {
        if (this.lineStart >= this.length)
        {
            return EmptyByteArray;
        }

        var content = new byte[this.length - this.lineStart];
        Array.Copy(this.data, this.lineStart, content, 0, this.length - this.lineStart);

        return content;
    }
}
