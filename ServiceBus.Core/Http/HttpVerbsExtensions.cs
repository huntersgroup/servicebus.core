namespace ServiceBus.Core.Http;

/// <summary>
/// 
/// </summary>
public static class HttpVerbsExtensions
{
    private static readonly IDictionary<HttpMethods, string> AllHttpVerbs = new Dictionary<HttpMethods, string>
    {
        { HttpMethods.Get, nameof(HttpMethods.Get).ToUpper() },
        { HttpMethods.Post, nameof(HttpMethods.Post).ToUpper() },
        { HttpMethods.Put, nameof(HttpMethods.Put).ToUpper() },
        { HttpMethods.Delete, nameof(HttpMethods.Delete).ToUpper() },
        { HttpMethods.Head, nameof(HttpMethods.Head).ToUpper() },
        { HttpMethods.Patch, nameof(HttpMethods.Patch).ToUpper() }
    };
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="httpMethod"></param>
    /// <returns></returns>
    public static string Translate(this HttpMethods httpMethod)
    {
        return AllHttpVerbs[httpMethod];
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="httpVerb"></param>
    /// <returns></returns>
    public static HttpMethods ToHttpVerb(this string httpVerb)
    {
        foreach (var allHttpVerb in AllHttpVerbs)
        {
            if (allHttpVerb.Value.Equals(httpVerb, StringComparison.OrdinalIgnoreCase))
            {
                return allHttpVerb.Key;
            }
        }

        return HttpMethods.Get;
    }
}
