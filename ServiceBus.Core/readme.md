**ServiceBus.Core** is easy library which exposes many contracts used to implement custom standard way to comunicate with message platforms.

for details, see the [documentation](https://servicebuscore.readthedocs.io/en/latest/).
