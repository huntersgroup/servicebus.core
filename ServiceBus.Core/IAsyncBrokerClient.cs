using ServiceBus.Core.Events;

namespace ServiceBus.Core;

/// <summary>
/// Represents a way to get events for underlying broker channels to notify consumers / publishers.
/// </summary>
public interface IAsyncBrokerClient
{
    /// <summary>
    /// Gets delegate handler for receiving events from broker.
    /// </summary>
    IDelegateHandler<ChannelEventArgs> OnChannelEventFired { get; }
}
