namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a delegate decorator which can register many delegates to execute.
/// </summary>
public class DelegateHandler<TArgs> : IDelegateHandler<TArgs>
{
    private readonly List<Delegate> delegates;

    /// <summary>
    /// Creates a new <see cref="DelegateHandler{TArgs}"/> instance.
    /// </summary>
    public DelegateHandler()
    {
        this.delegates = new List<Delegate>();
    }

    ///<inheritdoc />
    public virtual IDelegateListener<TArgs> Add(Action<object, TArgs> action)
    {
        this.delegates.Add(action);
        return this;
    }

    ///<inheritdoc />
    public virtual IDelegateListener<TArgs> Add(Func<object, TArgs, Task> func)
    {
        this.delegates.Add(func);
        return this;
    }

    ///<inheritdoc />
    public virtual IDelegateListener<TArgs> Remove(Action<object, TArgs> action)
    {
        var index = this.delegates.FindLastIndex(@delegate => @delegate.Equals(action));

        if (index > -1)
        {
            this.delegates.RemoveAt(index);
        }

        return this;
    }

    ///<inheritdoc />
    public virtual IDelegateListener<TArgs> Remove(Func<object, TArgs, Task> func)
    {
        var index = this.delegates.FindLastIndex(@delegate => @delegate.Equals(func));

        if (index > -1)
        {
            this.delegates.RemoveAt(index);
        }

        return this;
    }

    ///<inheritdoc />
    public bool Contains(Action<object, TArgs> action)
    {
        return this.delegates.Contains(action);
    }

    ///<inheritdoc />
    public bool Contains(Func<object, TArgs, Task> func)
    {
        return this.delegates.Contains(func);
    }

    ///<inheritdoc />
    public virtual async Task InvokeAsync(object sender, TArgs args)
    {
        foreach (var del in this.delegates)
        {
            var ret = del.DynamicInvoke(sender, args);

            if (ret is Task tsk)
            {
                await tsk;
            }
        }
    }

    ///<inheritdoc />
    public int Count()
    {
        return this.delegates.Count;
    }

    ///<inheritdoc />
    public void Clear()
    {
        this.delegates.Clear();
    }
}
