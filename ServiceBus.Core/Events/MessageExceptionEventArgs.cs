namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common way to indicate clients whenever an exception was occurred when messages are processed.
/// </summary>
public class MessageExceptionEventArgs : EventArgs
{
    /// <summary>
    /// Gets or sets the payload type used to deserialize the incoming messages from underlying queues.
    /// </summary>
    public Type PayloadType { get; set; }

    /// <summary>
    /// Gets or sets the exception caught 
    /// </summary>
    public Exception Exception { get; set; }
}
