namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common delegate handler, which can execute a next delegate handler.
/// <para>
/// Other features of this handler is registering a collection of <see cref="IDelegateListener{TArgs}"/> instances,
/// which add or remove all delegates present in the current <see cref="NextDelegateHandler{TArgs}"/> instance.
/// </para>
/// </summary>
/// <typeparam name="TArgs"></typeparam>
public class NextDelegateHandler<TArgs> : DelegateHandler<TArgs>
{
    private readonly IDelegateExecutor<TArgs> nextDelegateExecutor;
    private readonly IEnumerable<IDelegateListener<TArgs>> listeners;

    /// <summary>
    /// Creates a new <see cref="NextDelegateHandler{TArgs}"/> instance.
    /// </summary>
    /// <param name="nextDelegateExecutor">represents the next handler, to execute at the end of execution of this handler. It can be null.</param>
    /// <param name="listeners">a collection of <see cref="IDelegateListener{TArgs}"/> which add or remove all delegates registered by this instance, it can be null.</param>
    public NextDelegateHandler(IDelegateExecutor<TArgs> nextDelegateExecutor, IEnumerable<IDelegateListener<TArgs>> listeners)
    {
        this.nextDelegateExecutor = nextDelegateExecutor;
        this.listeners = listeners ?? [];
    }

    ///<inheritdoc />
    public override IDelegateListener<TArgs> Add(Action<object, TArgs> action)
    {
        base.Add(action);

        foreach (var listener in this.listeners)
        {
            listener.Add(action);
        }

        return this;
    }

    ///<inheritdoc />
    public override IDelegateListener<TArgs> Add(Func<object, TArgs, Task> func)
    {
        base.Add(func);

        foreach (var listener in this.listeners)
        {
            listener.Add(func);
        }

        return this;
    }

    ///<inheritdoc />
    public override IDelegateListener<TArgs> Remove(Action<object, TArgs> action)
    {
        base.Remove(action);

        foreach (var listener in this.listeners)
        {
            listener.Remove(action);
        }

        return this;
    }

    ///<inheritdoc />
    public override IDelegateListener<TArgs> Remove(Func<object, TArgs, Task> func)
    {
        base.Remove(func);

        foreach (var listener in this.listeners)
        {
            listener.Remove(func);
        }

        return this;
    }

    ///<inheritdoc />
    public override async Task InvokeAsync(object sender, TArgs args)
    {
        await base.InvokeAsync(sender, args);

        if (this.nextDelegateExecutor != null)
        {
            await this.nextDelegateExecutor.InvokeAsync(sender, args);
        }
    }
}
