namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common way to notifies clients whenever a message is published by underlying communication channel.
/// </summary>
public class ChannelEventArgs : EventArgs
{
    /// <summary>
    /// Gets or sets a tag which could be used to identify the underlying event raised.
    /// </summary>
    public string EventTag { get; set; }

    /// <summary>
    /// Get a set of properties custom set by publishers.
    /// </summary>
    public IDictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
}
