namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a delegate decorator which can register many delegates to execute.
/// </summary>
/// <typeparam name="TArgs"></typeparam>
public interface IDelegateHandler<TArgs> : IDelegateExecutor<TArgs>, IDelegateListener<TArgs>
{
    /// <summary>
    /// Verifies if the given action was registered previously.
    /// </summary>
    /// <param name="action">A custom action | delegate to verify</param>
    /// <returns></returns>
    bool Contains(Action<object, TArgs> action);

    /// <summary>
    /// Verifies if the given function was registered previously
    /// </summary>
    /// <param name="func">A custom function | delegate to verify</param>
    /// <returns></returns>
    bool Contains(Func<object, TArgs, Task> func);

    /// <summary>
    /// Gets the count of delegates for this instance.
    /// </summary>
    /// <returns></returns>
    int Count();

    /// <summary>
    /// Removes all delegates for this instance.
    /// </summary>
    void Clear();
}
