namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common way to notify clients whenever an exception was thrown during the message forwarding.
/// </summary>
/// <typeparam name="TData"></typeparam>
public class ForwardExceptionEventArg<TData> : EventArgs
{
    /// <summary>
    /// Creates a new instance of <see cref="ForwardExceptionEventArg{TData}"/>.
    /// </summary>
    /// <param name="eventArgs"></param>
    public ForwardExceptionEventArg(MessageDeliverEventArgs<TData> eventArgs)
    {
        this.MessageDeliverEventArgs = eventArgs;
    }

    /// <summary>
    /// Gets the original message event.
    /// <para>
    /// Getting the reference of message delivery event it's possible to change acknowledge and requeue values.
    /// </para>
    /// </summary>
    public MessageDeliverEventArgs<TData> MessageDeliverEventArgs { get; } 

    /// <summary>
    /// Gets or sets the exception caught 
    /// </summary>
    public Exception Exception { get; set; }
}
