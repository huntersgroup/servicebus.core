namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common way to indicate there's a exception due to max number of retries for resending the same message.
/// </summary>
public class RetryExceptionEventArgs<TData> : EventArgs
{
    /// <summary>
    /// Get or sets the message related to this event.
    /// </summary>
    public IMessage<TData> Message { get; set; }

    /// <summary>
    /// Gets or sets the number of retry reached by subscribers.
    /// </summary>
    public short RetryCounter { get; set; }

    /// <summary>
    /// Gets or sets the exception caught 
    /// </summary>
    public Exception Exception { get; set; }
}
