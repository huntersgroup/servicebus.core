namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common delegate listener which registers or removes delegates.
/// </summary>
/// <typeparam name="TArgs"></typeparam>
public interface IDelegateListener<out TArgs>
{
    /// <summary>
    /// Adds a new delegate.
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    IDelegateListener<TArgs> Add(Action<object, TArgs> action);

    /// <summary>
    /// Adds a new delegate.
    /// </summary>
    /// <param name="func"></param>
    /// <returns></returns>
    IDelegateListener<TArgs> Add(Func<object, TArgs, Task> func);

    /// <summary>
    /// Removes a new delegate.
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    IDelegateListener<TArgs> Remove(Action<object, TArgs> action);

    /// <summary>
    /// Removes a new delegate.
    /// </summary>
    /// <param name="func"></param>
    /// <returns></returns>
    IDelegateListener<TArgs> Remove(Func<object, TArgs, Task> func);
}
