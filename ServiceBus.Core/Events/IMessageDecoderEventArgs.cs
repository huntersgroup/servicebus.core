namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a contract which can be used as a middleware between original message and deserialization strategy.
/// </summary>
public interface IMessageDecoderEventArgs : IMessageDeliverEventArgs<object>
{
    /// <summary>
    /// Decodes the original message, using the given <see cref="TData"/> type.
    /// <para>
    /// Whenever this method is called, this set the result into the <see cref="IMessageDeliverEventArgs{TData}.Message"/> property.
    /// </para>
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <returns></returns>
    IMessage<TData> Decode<TData>();
}
