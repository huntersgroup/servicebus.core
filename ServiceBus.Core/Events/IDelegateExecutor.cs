namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a delegate to execute.
/// </summary>
/// <typeparam name="TArgs"></typeparam>
public interface IDelegateExecutor<in TArgs>
{
    /// <summary>
    /// Execute the underlying delegate with the given parameters.
    /// </summary>
    /// <param name="sender">The reference of the real object that invokes this method.</param>
    /// <param name="args">The argument for the underlying delegate.</param>
    /// <returns></returns>
    Task InvokeAsync(object sender, TArgs args);
}
