namespace ServiceBus.Core.Events;

/// <summary>
/// Represents a common way to notifies clients whenever a message is received by underlying communication channel.
/// </summary>
/// <typeparam name="TData"></typeparam>
public class MessageDeliverEventArgs<TData>(ReadOnlyMemory<byte> rawBytes) : EventArgs, IMessageDeliverEventArgs<TData>
{
    ///<inheritdoc />
    public ReadOnlyMemory<byte> RawBytes { get => rawBytes; }

    ///<inheritdoc />
    public IMessage<TData> Message { get; set; }

    ///<inheritdoc />
    public bool Acknowledged { get; set; } = true;

    ///<inheritdoc />
    public bool Requeue { get; set; } = false;
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IMessageDeliverEventArgs<out TData>
{
    /// <summary>
    /// 
    /// </summary>
    ReadOnlyMemory<byte> RawBytes { get; }

    /// <summary>
    /// Get or sets the message related to this event.
    /// </summary>
    IMessage<TData> Message { get; }

    /// <summary>
    /// Gets or sets the way to notify underlying communication channel that its holden message was managed correctly.
    /// </summary>
    bool Acknowledged { get; set; }

    /// <summary>
    /// Gets or sets a boolean value indicating if the current message will be re-queued if <see cref="Acknowledged"/> is false.
    /// </summary>
    public bool Requeue { get; set; }
}
