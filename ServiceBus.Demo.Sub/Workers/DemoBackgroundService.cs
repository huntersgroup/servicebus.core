using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServiceBus.Core.Clients.Subscribers;

namespace ServiceBus.Demo.Sub.Workers;

public class DemoBackgroundService : BackgroundService
{
    private readonly ILogger logger;
    private readonly string runnableTarget;
    private readonly IAsyncRunnable runnable;
    private bool disposed;

    public DemoBackgroundService(ILogger<DemoBackgroundService> logger, IAsyncRunnable runnable)
    {
        this.logger = logger;
        this.runnableTarget = $"{nameof(DemoBackgroundService)} app, id: {Guid.NewGuid():D}";
        this.runnable = runnable;
    }

    public override void Dispose()
    {
        this.DisposeAsync().AsTask().GetAwaiter().GetResult();
    }

    public ValueTask DisposeAsync()
    {
        this.disposed = true;

        base.Dispose();
        this.logger.LogInformation($"<<< {this.runnableTarget} was disposed !!! >>>");

        return ValueTask.CompletedTask;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        this.logger.LogInformation($"<<< {this.runnableTarget} has started !!! >>>");

        await this.runnable.Start();

        await ValueTask.CompletedTask;
    }
}
