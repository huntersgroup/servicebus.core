using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using ServiceBus.Demo.Sub;
using ServiceBus.Demo.Sub.Config;
using ServiceBus.Demo.Sub.Workers;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Hosting;
using ServiceResolver.Ioc.Options;
using SimpleInjector;
using Host = Microsoft.Extensions.Hosting.Host;

var env = Environment.GetEnvironmentVariable("APP_ENV") ?? "dev";

var hostBuilder =
        Host.CreateDefaultBuilder(args)
            .UseServiceResolver(context => new SimpleInjectorOptions
            {
                Lifestyle = Lifestyle.Singleton,
                HostSettings = new HostSettings
                {
                    UseScope = true
                },
                BeforeRegistering = _ =>
                {
                },
                OnRegistering = serviceRegister =>
                {
                    serviceRegister.Cache.TryAdd(context.Configuration.Get<AppSettings>());
                    serviceRegister.Cache.TryAdd(ExecParamsInit.GetInstance());

                    serviceRegister.RegisterModulesFrom(Assembly.GetExecutingAssembly());

                    serviceRegister
                        .RegisterMany<IHostedService>(new[] { typeof(DemoBackgroundService) },
                            LifetimeScope.Scoped);
                },
                AfterBuildingProvider = provider =>
                {
                    provider.Verify();
                }
            })
            .UseEnvironment(env)
    ;

var host = hostBuilder
    .Build();

await host.RunAsync();
