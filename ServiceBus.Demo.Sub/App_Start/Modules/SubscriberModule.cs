using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using ServiceBus.Core.Clients;
using ServiceBus.Core.Clients.Subscribers;
using ServiceBus.Core.Events;
using ServiceBus.Core.Extensions;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Formatters.Json;
using ServiceBus.Demo.Common.Config;
using ServiceBus.Demo.Model;
using ServiceBus.Demo.Sub.Config;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceBus.Demo.Sub.Modules;

internal class SubscriberModule : IServiceModule
{
    private static readonly char CharLabel = '\t';

    public void Initialize(IServiceRegister serviceRegister)
    {
        var exeParams = serviceRegister.Cache.Get<ExeParams>();
        var appSettings = serviceRegister.Cache.Get<AppSettings>();

        serviceRegister.Register<IConnectionFactory>(() => new ConnectionFactory
        {
            Uri = appSettings.EsbAddress,
            UserName = appSettings.EsbCredentials.Username,
            Password = appSettings.EsbCredentials.Password,
            DispatchConsumersAsync = true
        });

        if (exeParams.SubscriberType == SubscriberType.Bound)
        {
            serviceRegister.Register<IAsyncRunnable, SubscriberBound<ComplexCls>>()
                .Register(() => appSettings.Descriptors.SubscriberBound01)
                .Register<IDataDecoder>(provider => provider.GetService<IDataFormatter>())
                .Register(provider => provider.GetService<IConnectionFactory>().CreateConnection())
                .Register(provider => provider.GetService<IConnection>().CreateChannel());
        }
        else if (exeParams.SubscriberType == SubscriberType.BoundNoGeneric)
        {
            serviceRegister.Register<IAsyncRunnable, SubscriberBound>()
                .Register(() => appSettings.Descriptors.SubscriberBound01)
                .Register<IDataDecoder>(provider => provider.GetService<IDataFormatter>())
                .Register(provider => provider.GetService<IConnectionFactory>().CreateConnection())
                .Register(provider => provider.GetService<IConnection>().CreateChannel());
        }
        else
        {
            serviceRegister.Register<IAsyncRunnable, RoutingBridgeBound<ComplexCls>>()
                .Register(() => appSettings.Descriptors.RoutingBridgeBound)
                .Register<IChannelFactory, ChannelFactory>();
        }

        serviceRegister.RegisterInitializer<IAsyncSubscriber<ComplexCls>>((provider, subscriber) =>
        {
            var serializer = provider.GetService<IJsonSerializer>();

            subscriber.Received.Add((_, args) => BoundOnReceived1Async(args, serializer));
            subscriber.OnException.Add(BoundOnOnException);
        });

        serviceRegister.RegisterInitializer<IAsyncSubscriber>((provider, subscriber) =>
        {
            var serializer = provider.GetService<IJsonSerializer>();

            subscriber.Received.Add((_, args) => BoundOnReceived1Async(args, serializer));
            subscriber.OnException.Add(BoundOnOnException);
        });
    }

    private static async Task BoundOnReceived1Async(MessageDeliverEventArgs<ComplexCls> args, IJsonSerializer serializer)
    {
        Console.WriteLine($"*** message started at: {CharLabel}{DateTime.UtcNow:u}");
        Console.WriteLine($"*** message created at: {CharLabel}{args.Message.Created:u}");
        Console.WriteLine($"*** message received: {CharLabel}{CharLabel}{serializer.Serialize(args.Message.Data)}");
        await Task.Delay(TimeSpan.FromMilliseconds(50));
        Console.WriteLine($"*** message consumed at: {CharLabel}{DateTime.UtcNow:u}, task is over ***");
        Console.WriteLine();
    }

    private static async Task BoundOnReceived1Async(IMessageDecoderEventArgs args, IJsonSerializer serializer)
    {
        var msg = args.Decode<ComplexCls>();

        Console.WriteLine($"*** message started at: {CharLabel}{DateTime.UtcNow:u}");
        Console.WriteLine($"*** message created at: {CharLabel}{args.Message.Created:u}");
        Console.WriteLine($"*** message received: {CharLabel}{CharLabel}{serializer.Serialize(args.Message.Data)}");
        await Task.Delay(TimeSpan.FromMilliseconds(50));
        Console.WriteLine($"*** message consumed at: {CharLabel}{DateTime.UtcNow:u}, task is over ***");
        Console.WriteLine();
    }

    internal static void BoundOnOnException(object sender, MessageExceptionEventArgs e)
    {
        Console.WriteLine($"An exception was caught, payloadType: {e.PayloadType}, exception: [message: {e.Exception.Message}, stackTrace: {e.Exception.StackTrace}]");
    }
}
