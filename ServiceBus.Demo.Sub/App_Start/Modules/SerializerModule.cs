using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ServiceBus.Core.Formatters;
using ServiceBus.Core.Formatters.Extensions;
using ServiceBus.Core.Formatters.Json;
using ServiceBus.Demo.Common.Config;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace ServiceBus.Demo.Sub.Modules;

/// <summary>
/// 
/// </summary>
internal class SerializerModule : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        var exeParams = serviceRegister.Cache.Get<ExeParams>();

        if (exeParams.SerializerType == SerializerType.Json)
        {
            this.InitJsonNetFormatter(serviceRegister);
        }
        else
        {
            this.InitMsJsonFormatter(serviceRegister);
        }
    }

    private void InitJsonNetFormatter(IServiceRegister serviceRegister)
    {
        serviceRegister.Register<IDataFormatter, RwJsonDataFormatter>()
            .Register(() => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                FloatParseHandling = FloatParseHandling.Decimal,
                NullValueHandling = NullValueHandling.Ignore
            })
            .Register(provider => JsonSerializer.CreateDefault(provider.GetService<JsonSerializerSettings>()))
            .Register(() => Encoding.UTF8);

        serviceRegister.Register<IJsonSerializer, JsonNetSerializer>();
    }

    private void InitMsJsonFormatter(IServiceRegister serviceRegister)
    {
        serviceRegister.Register<IDataFormatter, MsJsonDataFormatter>()
            .Register(_ =>
            {
                var options = new JsonSerializerOptions
                {
                    DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                    PropertyNameCaseInsensitive = true,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    ReferenceHandler = ReferenceHandler.IgnoreCycles,
                    DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
                };

                options.Converters.Add(new JsonStringEnumConverter());

                options.ApplyRealNumberWithFractionalPortion();

                return options;
            });

        serviceRegister.Register<IJsonSerializer, MsJsonSerializer>();
    }
}
