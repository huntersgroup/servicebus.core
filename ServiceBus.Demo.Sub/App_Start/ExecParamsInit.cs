using ServiceBus.Demo.Common;
using ServiceBus.Demo.Common.Config;
using ServiceBus.Demo.Sub.Config;

namespace ServiceBus.Demo.Sub;

internal static class ExecParamsInit
{
    internal static ExeParams GetInstance()
    {
        ConsoleExt.WriteLine("### Choose the subscriber type to execute: ###", ConsoleColor.Blue);
        Console.WriteLine($"### Press 1 for {nameof(AppSettings.Descriptors.SubscriberBound01)}");
        Console.WriteLine($"### Press 2 for SubscriberBound - No Generic");
        Console.WriteLine($"### Press any key for {nameof(AppSettings.Descriptors.RoutingBridgeBound)}");

        var subType = ConsoleExt.ReadInput("*** Write the option: ", s => s.Equals("1") ? SubscriberType.Bound : s.Equals("2") ? SubscriberType.BoundNoGeneric : SubscriberType.Bridge);
        Console.WriteLine();

        ConsoleExt.WriteLine("### Choose the Serializer to use: ###", ConsoleColor.Blue);
        Console.WriteLine("### Press 1 for Json.Net");
        Console.WriteLine("### Press any key for Microsoft Text json");

        var formatType = ConsoleExt.ReadInput("*** Write the option: ", s => s.Equals("1") ? SerializerType.Json : SerializerType.Microsoft);
        Console.WriteLine();

        return new ExeParams
        {
            SerializerType = formatType,
            SubscriberType = subType
        };
    }
}
