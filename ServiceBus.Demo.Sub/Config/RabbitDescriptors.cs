using ServiceBus.Core.Clients.Subscribers.Descriptors;
using ServiceBus.Core.Descriptors;

namespace ServiceBus.Demo.Sub.Config;

public class RabbitDescriptors
{
    public RoutingBridgeDescriptor RoutingBridgeBound { get; set; }

    public SubscriberBoundDescriptor SubscriberBound01 { get; set; }
}
