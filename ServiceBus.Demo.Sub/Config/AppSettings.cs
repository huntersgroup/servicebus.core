using ServiceBus.Demo.Common.Config;

namespace ServiceBus.Demo.Sub.Config;

internal class AppSettings : AppSettingsBase
{
    public RabbitDescriptors Descriptors { get; set; }
}
