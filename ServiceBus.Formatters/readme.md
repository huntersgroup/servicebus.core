**ServiceBus.Formatters.Ms** is easy library which exposes some contracts used to serialize and deserialize messages incoming from ESB brokers.

for details, see the [documentation](https://servicebuscore.readthedocs.io/en/latest/).
