namespace ServiceBus.Core.Formatters;

/// <summary>
/// Represents an unique way to encode data in order to forward into underlying channel.
/// </summary>
public interface IDataEncoder
{
    /// <summary>
    /// Encodes the given data.
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    byte[] Encode(object data);
}
