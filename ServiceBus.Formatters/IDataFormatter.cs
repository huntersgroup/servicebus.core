using System.Text;

namespace ServiceBus.Core.Formatters;

/// <summary>
/// Represents a common contract used to encode and decode data.
/// </summary>
public interface IDataFormatter : IDataDecoder, IDataEncoder
{
    /// <summary>
    /// Gets the encoder related to this instance.
    /// </summary>
    Encoding Encoder { get; }
}
