namespace ServiceBus.Core.Formatters.Json;

/// <summary>
/// Represents a common serializer for json literal.
/// </summary>
public interface IJsonSerializer
{
    /// <summary>
    /// Serialize the given instance into json representation.
    /// </summary>
    /// <param name="instance"></param>
    /// <returns></returns>
    string Serialize(object instance);

    /// <summary>
    /// Deserialize the given json string into <see cref="TData"/> instance.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="json"></param>
    /// <returns></returns>
    TData Deserialize<TData>(string json);
}
