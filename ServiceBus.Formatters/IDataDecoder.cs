namespace ServiceBus.Core.Formatters;

/// <summary>
/// Represents a common contract for decoding messages from underlying channels.
/// </summary>
public interface IDataDecoder
{
    /// <summary>
    /// Decode the given bytes, deserializing into the given TData type.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="data"></param>
    /// <returns></returns>
    TData Decode<TData>(byte[] data);

    /// <summary>
    /// Decode the given bytes, deserializing into the given TData type.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="data"></param>
    /// <returns></returns>
    TData Decode<TData>(ReadOnlyMemory<byte> data);

    /// <summary>
    /// Decode the given bytes, deserializing into the given type.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    object Decode(byte[] data, Type type);

    /// <summary>
    /// Decode the given bytes, deserializing into the given type.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    object Decode(ReadOnlyMemory<byte> data, Type type);
}
